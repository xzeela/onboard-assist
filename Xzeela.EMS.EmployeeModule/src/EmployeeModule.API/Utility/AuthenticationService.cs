namespace EmployeeModule.API.Utility
{
    public interface IAuthenticationService
    {
        Task<string?> CreateUserAsync(string name, string email, string userId, int tenantId, int requestedUserId);
    }

    public class AuthenticationService : IAuthenticationService
    {
        private readonly HttpClient _httpClient;
        private readonly IConfiguration _configuration;
        private readonly string? _identityUrl;

        public AuthenticationService(HttpClient httpClient, IConfiguration configuration)
        {
            _httpClient = httpClient ?? throw new ArgumentNullException(nameof(httpClient));
            _configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
            _identityUrl = Environment.GetEnvironmentVariable("IDENTITY_URL");

        }

        public async Task<string?> CreateUserAsync(string name, string email, string userId, int tenantId, int requestedUserId)
        {
            try
            {
                var userEndpointUrl = _configuration["ApiSettings:UserEndpointUrl"];

                if (!string.IsNullOrEmpty(_identityUrl))
                {
                    userEndpointUrl = _identityUrl;
                }
                
                var requestContent = new StringContent(
                    $@"{{
                        ""name"": ""{name}"",
                        ""email"": ""{email}"",
                        ""userId"": ""{userId}"",
                        ""tenantId"": {tenantId},
                        ""requestedUserId"": {requestedUserId}
                    }}",
                    System.Text.Encoding.UTF8,
                    "application/json"
                );

                var response = await _httpClient.PostAsync(userEndpointUrl, requestContent);

                // await using var responseStream = await response.Content.ReadAsStreamAsync();
                // var successResponse = await JsonSerializer.DeserializeAsync<BaseResponse>(responseStream);
                
                return !response.IsSuccessStatusCode ? null : "success";
            }
            catch (Exception ex)
            {
                throw new Exception($"User creation failed: {ex.Message}", ex);
            }
        }
    }
}
