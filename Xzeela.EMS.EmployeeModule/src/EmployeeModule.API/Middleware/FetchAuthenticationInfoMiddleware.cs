using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace EmployeeModule.API.Middleware
{
    public class FetchAuthenticationInfoMiddleware
    {
        private readonly RequestDelegate _next;

        public FetchAuthenticationInfoMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            if (context.User.Identity is { IsAuthenticated: true })
            {
                // Extract and store JWT claims in context for later use
                var privilegeType = context.User.FindFirst("PrivilegeType")?.Value;
                var requestedUserId = context.User.FindFirst("Id")?.Value;
                var tenantId = context.User.FindFirst("TenantId")?.Value;
                
                context.Items["RequestedUserId"] = requestedUserId;
                context.Items["RequestedUserPrivilege"] = privilegeType;
                context.Items["TenantId"] = tenantId;
            }
            await _next(context);
        }
    }
}