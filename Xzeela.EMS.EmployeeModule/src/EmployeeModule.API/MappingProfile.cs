using AutoMapper;
using EmployeeModule.API.Models.Requests;
using EmployeeModule.API.Models.Responses;
using EmployeeModule.Domain.Entities;

namespace EmployeeModule.API;

public class MappingProfile : Profile
{
    public MappingProfile()
    {
        CreateMap<CreateEmployeeRequest, Employee>().ReverseMap();
        CreateMap<EmployeeResponse, Employee>().ReverseMap();

    }
}

