using System.Reflection;
using System.Text.Json.Serialization;
using EmployeeModule.API.Middleware;
using EmployeeModule.API.Utility;
using EmployeeModule.Infrastructure;
using EmployeeModule.Infrastructure.Interfaces;
using EmployeeModule.Infrastructure.Repositories;
using Microsoft.EntityFrameworkCore;
using NSwag;
using NSwag.Generation.Processors.Security;
using Prometheus;
using System.Text;
using EmployeeModule.API.Settings;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Identity;
using Microsoft.IdentityModel.Tokens;
using Xzeela.EMS.Shared.Services;
using IRequestTrackingService = Xzeela.EMS.Shared.Services.IRequestTrackingService;
using RequestTrackingService = Xzeela.EMS.Shared.Services.RequestTrackingService;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

var jwtSettings = builder.Configuration.GetSection("Jwt").Get<JwtSettings>();

builder.Services.AddAuthentication(opt =>
{
    opt.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
}).AddJwtBearer(opt =>
{
    var key = Encoding.UTF8.GetBytes(jwtSettings.Secret);
    opt.IncludeErrorDetails = true;
    opt.SaveToken = true;
    opt.TokenValidationParameters = new TokenValidationParameters
    {
        ValidateIssuer = true,
        ValidateLifetime = true,
        ValidateAudience = true,
        ValidateIssuerSigningKey = true,
        ValidIssuer = jwtSettings.Issuer,
        ValidAudience = jwtSettings.Audience,
        IssuerSigningKey = new SymmetricSecurityKey(key)
    };
});

builder.Services.Configure<DataProtectionTokenProviderOptions>(opt =>
    opt.TokenLifespan = TimeSpan.FromHours(2));

var connectionString = Environment.GetEnvironmentVariable("ConnectionString");

if (connectionString == null)
{
    connectionString = builder.Configuration["ConnectionString"];
}

builder.Services.AddDbContext<ApplicationDataContext>(options =>
     options.UseNpgsql(connectionString));

builder.Services.AddMediatR(cfg => cfg.RegisterServicesFromAssembly(Assembly.GetExecutingAssembly()));
builder.Services.AddAutoMapper(Assembly.GetExecutingAssembly());
builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

builder.Services.AddControllers();

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();

builder.Services.AddSwaggerDocument(config =>
{
    config.PostProcess = document =>
    {
        document.Info.Title = "Xzeela Employee Module API";
        document.Info.Description = "<li> Emplo    yee Module Information </li>";
        document.Info.Contact = new NSwag.OpenApiContact
        {
            Name = "Infaz Rumy",
            Url = "https://linktr.ee/infazrumy"
        };
    };
    
    config.AddSecurity("JWT", Enumerable.Empty<string>(), new OpenApiSecurityScheme
    {
        Type = OpenApiSecuritySchemeType.ApiKey,
        Name = "Authorization",
        In = OpenApiSecurityApiKeyLocation.Header,
        Description = "Type into the textbox: Bearer {your JWT token}."
    });
    
    config.OperationProcessors.Add(
        new AspNetCoreOperationSecurityScopeProcessor("JWT"));
});

builder.Services.AddApiVersioning(options =>
{
    options.AssumeDefaultVersionWhenUnspecified = true;
    options.DefaultApiVersion = new Microsoft.AspNetCore.Mvc.ApiVersion(1, 0);
    options.ReportApiVersions = true;
});

builder.Services.AddVersionedApiExplorer(
    options =>
    {
        options.GroupNameFormat = "'v'VVV";
        options.SubstituteApiVersionInUrl = true;
    });

builder.Services.AddEndpointsApiExplorer();

builder.Services.AddControllers().AddJsonOptions(x =>
{
    // serialize enums as strings in api responses (e.g. Gender)
    x.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
});

builder.Services.AddControllers();

// Register IHttpContextAccessor
builder.Services.AddHttpContextAccessor();

// Register HttpClient
builder.Services.AddHttpClient();

// Load API settings from appsettings.json
//services.Configure<ApiSettings>(Configuration.GetSection("ApiSettings"));

builder.Services.AddTransient<ExceptionHandlingMiddleware>();
builder.Services.AddScoped<IRequestTrackingService, RequestTrackingService>();
builder.Services.AddScoped<IEmployeeRepository, EmployeeRepository>();
builder.Services.AddScoped<IAuthenticationService, AuthenticationService>();
builder.Services.AddScoped<ILoggerService, LoggerService>();

//services cors
builder.Services.AddCors(p => p.AddPolicy("corsapp", builder =>
{
    builder.WithOrigins("*").AllowAnyMethod().AllowAnyHeader();
}));

var app = builder.Build();

app.UseMetricServer();//Starting the metrics exporter, will expose "/metrics"

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseOpenApi();
    app.UseSwaggerUi3();
}
else
{
    var swaggerEnabled = Environment.GetEnvironmentVariable("SWAGGER_ENABLED");

    var success = bool.TryParse(swaggerEnabled, out var output);
    if (success)
    {
        if (output)
        {
            app.UseOpenApi();
            app.UseSwaggerUi3();
        }
    }
}


//running migration at the start of application if ex
using (var scope = app.Services.CreateScope())
{
    var services = scope.ServiceProvider;
    var context = services.GetRequiredService<ApplicationDataContext>();

    if (context.Database.GetPendingMigrations().Any())
    {
        try
        {
            context.Database.Migrate();
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
        }
    }
}

app.UseRouting();
app.UseCors("corsapp");

app.UseAuthentication();
app.UseAuthorization();

app.UseHttpMetrics();

app.UseMiddleware<RequestTrackingMiddleware>();

app.UseMiddleware<FetchAuthenticationInfoMiddleware>();

app.UseMiddleware<ExceptionHandlingMiddleware>();

app.UseHttpsRedirection();

app.MapControllers();

//adding metrics related to HTTP
app.UseHttpMetrics(options =>
{
    options.AddCustomLabel("host", context => context.Request.Host.Host);
});

app.Run();
