using AutoMapper;
using EmployeeModule.API.Models.Requests;
using EmployeeModule.API.Utility;
using EmployeeModule.Domain.Entities;
using EmployeeModule.Domain.Exceptions;
using EmployeeModule.Infrastructure.Interfaces;
using MediatR;
using Xzeela.EMS.Shared.Services;

namespace EmployeeModule.API.Applications.Commands;

public class CreateEmployeeCommand : IRequestHandler<CreateEmployeeRequest, string>
{
    private readonly IEmployeeRepository _employeeRepository;
    private readonly IMapper _mapper;
    private readonly IAuthenticationService _authenticationService;
    private readonly IRequestTrackingService _trackingService;

    public CreateEmployeeCommand(
        IEmployeeRepository  employeeRepository,
        IMapper mapper,
        IAuthenticationService authenticationService,
        IRequestTrackingService requestTrackingService)
    {
        _employeeRepository = employeeRepository;
        _mapper = mapper;
        _authenticationService = authenticationService;
        _trackingService = requestTrackingService;
    }
    public async Task<string> Handle(CreateEmployeeRequest request, CancellationToken cancellationToken)
    {
        if (await _employeeRepository.IsEmailAvailable(request.Email, request.TenantId))
            throw new EmployeeDomainException("Email is already taken by the Tenant");
        
        if (await _employeeRepository.IsNicAvailable(request.NIC, request.TenantId))
            throw new EmployeeDomainException("NIC is already taken by the Tenant");

        var requestedUserId = _trackingService.GetRequestedUserId();
        if(string.IsNullOrEmpty(requestedUserId))
            throw new UnauthorizedAccessException("Unable to find requested user");

        var employee = _mapper.Map<Employee>(request);
        employee.CreatedBy = int.Parse(requestedUserId);
        employee.CreatedOn = DateTime.UtcNow;
        
        var appUserId = await _authenticationService.CreateUserAsync($"{request.FirstName} {request.LastName}",
            request.Email, request.EmployeeId, request.TenantId, int.Parse(requestedUserId));
        
        if(string.IsNullOrEmpty(appUserId))
            throw new EmployeeDomainException("Unable to create the user credentials");

        _ = await _employeeRepository.AddAsync(employee);
        return employee.Id.ToString();
    }
}