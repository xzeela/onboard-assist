using AutoMapper;
using EmployeeModule.API.Models.Requests;
using EmployeeModule.API.Models.Responses;
using EmployeeModule.Domain.Entities;
using EmployeeModule.Domain.Exceptions;
using EmployeeModule.Infrastructure.Interfaces;
using MediatR;
using Xzeela.EMS.Shared.Services;
using EmployeeModule.API.Utility;

namespace EmployeeModule.API.Applications.Queries;


public class GetAllEmployeesCommand : IRequestHandler<GetAllEmployeeRequest, List<EmployeeResponse>>
{
    private readonly IEmployeeRepository _employeeRepository;
    private readonly IMapper _mapper;
    private readonly IAuthenticationService _authenticationService;
    private readonly IRequestTrackingService _trackingService;

    public GetAllEmployeesCommand(
        IEmployeeRepository  employeeRepository,
        IMapper mapper,
        IAuthenticationService authenticationService,
        IRequestTrackingService requestTrackingService)
    {
        _employeeRepository = employeeRepository;
        _mapper = mapper;
        _authenticationService = authenticationService;
        _trackingService = requestTrackingService;
    }
    public async Task<List<EmployeeResponse>> Handle(GetAllEmployeeRequest request, CancellationToken cancellationToken)
    {
        var requestedUserTenant =  _trackingService.GetRequestUserTenantId();
        if(string.IsNullOrEmpty(requestedUserTenant))
            throw new UnauthorizedAccessException("Unable to find requested user");

        var employees = await _employeeRepository.GetEmployeesByTenantId(int.Parse(requestedUserTenant));
        return _mapper.Map<List<EmployeeResponse>>(employees);
    }
}