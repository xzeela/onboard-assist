using System.ComponentModel.DataAnnotations;
using System.Reflection;
using EmployeeModule.Domain.Enums;
using MediatR;

namespace EmployeeModule.API.Models.Requests;

public class CreateEmployeeRequest : IRequest<string>
{
    /// <summary>
    /// Employee ID of the Employee
    /// </summary>
    [Required(ErrorMessage = "Employee ID is required.")]
    [MaxLength(200, ErrorMessage = "Employee ID cannot exceed 200 characters.")]
    public string EmployeeId { get; set; } = null!;
    
    /// <summary>
    /// First Name of the Employee
    /// </summary>
    [Required(ErrorMessage = "First Name is required.")]
    [MaxLength(100, ErrorMessage = "First Name cannot exceed 100 characters.")]
    [RegularExpression(@"^(?![0-9]*$)[a-zA-Z0-9\s]+$", ErrorMessage = "First Name cannot contain only numbers.")]
    public string FirstName { get; set; } = null!;

    /// <summary>
    /// Last Name of the Employee
    /// </summary>
    [Required(ErrorMessage = "Last Name is required.")]
    [MaxLength(100, ErrorMessage = "Last Name cannot exceed 100 characters.")]
    [RegularExpression(@"^(?![0-9]*$)[a-zA-Z0-9\s]+$", ErrorMessage = "Last Name cannot contain only numbers.")]
    public string LastName { get; set; } = null!;
    
    /// <summary>
    /// Email of the Employee
    /// </summary>
    [Required(ErrorMessage = "Email is required.")]
    [MaxLength(100, ErrorMessage = "Email cannot exceed 100 characters.")]
    public string Email { get; set; } = null!;
    
    /// <summary>
    /// NIC of the Employee
    /// </summary>
    [Required(ErrorMessage = "NIC is required.")]
    [MaxLength(100, ErrorMessage = "NIC cannot exceed 100 characters.")]
    [RegularExpression(@"^[a-zA-Z0-9]+$", ErrorMessage = "NIC can only contain letters and numbers.")]
    public string NIC { get; set; } = null!;
    
    /// <summary>
    /// Gender of the Employee
    /// </summary>
    [Required(ErrorMessage = "Gender is required.")]
    [RegularExpression(@"^[a-zA-Z0-9]+$", ErrorMessage = "NIC can only contain letters and numbers.")]
    public Gender Gender { get; set; }
    
    /// <summary>
    /// Tenant Id (Set by internally)
    /// </summary>
    public int TenantId { get; set; }
    
}