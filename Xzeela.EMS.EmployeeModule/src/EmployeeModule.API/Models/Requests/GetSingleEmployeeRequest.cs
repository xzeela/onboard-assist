using EmployeeModule.API.Models.Responses;
using MediatR;

namespace EmployeeModule.API.Models.Requests;

public class GetSingleEmployeeRequest : IRequest<EmployeeResponse?>
{ 
    public string Email { get; set; } = null!;
}