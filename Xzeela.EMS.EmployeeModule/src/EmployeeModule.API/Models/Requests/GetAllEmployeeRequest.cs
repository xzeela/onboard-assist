using EmployeeModule.API.Models.Responses;
using MediatR;

namespace EmployeeModule.API.Models.Requests;

public class GetAllEmployeeRequest : IRequest<List<EmployeeResponse>>
{
    
}