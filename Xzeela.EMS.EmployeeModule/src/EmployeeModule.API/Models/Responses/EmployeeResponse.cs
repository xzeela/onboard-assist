using EmployeeModule.Domain.Entities;
using EmployeeModule.Domain.Enums;

namespace EmployeeModule.API.Models.Responses;

public class EmployeeResponse
{
    public int TenantId { get; set; }
    public string FirstName { get; set; } = null!;
    public string LastName { get; set; } = null!;
    public string Email { get; set; } = null!;
    public string NIC { get; set; } = null!;
    public Gender Gender { get; set; }
    public Address? Address { get; set; }
}