﻿using EmployeeModule.API.Models.Requests;
using EmployeeModule.Domain.Enums;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Xzeela.EMS.Shared.API.Models;
using Xzeela.EMS.Shared.Services;

namespace EmployeeModule.API.Controllers;

[ApiController]
[Authorize(AuthenticationSchemes = "Bearer")]
[ApiVersion("1.0")]
[Route("api/v{version:apiVersion}/[controller]")]
public class EmployeeController : ControllerBase
{

    private readonly ILogger<EmployeeController> _logger;
    private readonly IRequestTrackingService _trackingService;
    private readonly IMediator _mediator;

    public EmployeeController(IRequestTrackingService trackingService, ILogger<EmployeeController> logger, IMediator mediator)
    {
        _trackingService = trackingService;
        _logger = logger;
        _mediator = mediator;
    }

    /// <summary>
    /// Create the employee
    /// </summary>
    /// <param name="createEmployeeRequest"></param>
    /// <returns></returns>
    [HttpPost]
    public async Task<ActionResult<BaseResponse>> Post(CreateEmployeeRequest createEmployeeRequest)
    {
        try
        {
            // Serialize the createEmployeeRequest object to JSON for logging.
            var requestBodyJson = JsonConvert.SerializeObject(createEmployeeRequest);
            _trackingService.TrackRequest($"uri: [POST]/employee/ - request: {requestBodyJson}");

            var tenantId = _trackingService.GetRequestUserTenantId();
            
            if (string.IsNullOrEmpty(tenantId))
            {
                return BadRequest(new BaseResponse(_trackingService.GetRequestId() ?? string.Empty,null!, ResponseType.Failed.ToString()));
            }

            createEmployeeRequest.TenantId = int.Parse(tenantId);
            
            var employeeId = await _mediator.Send(createEmployeeRequest);
            var response = new BaseResponse(_trackingService.GetRequestId() ?? string.Empty, employeeId,
                ResponseType.Success.ToString());
                
            var responseBodyJson = JsonConvert.SerializeObject(response);
            _trackingService.TrackRequest($"uri: [POST]/employee/ - response: {responseBodyJson}");
            return Ok(response);
        }
        catch (Exception ex)
        {
            _trackingService.TrackRequest($"uri: [POST]/employee/ - exception: {ex.Message}");
            return BadRequest(new BaseResponse(_trackingService.GetRequestId() ?? string.Empty,null!, ResponseType.Failed.ToString()));
        }
    }
    
    /// <summary>
    /// Retrieves a list of all employees.
    /// </summary>
    /// <returns>A response containing a list of employee information if successful.</returns>
    /// <response code="200">Returns a list of employee if the operation is successful.</response>
    [HttpGet("getallemployees")]
    [ProducesResponseType(typeof(BaseResponse), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(BaseResponse), StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(typeof(BaseResponse), StatusCodes.Status400BadRequest)]
    public async Task<ActionResult<BaseResponse>> GetAllEmployees()
    {
        try
        {
            _trackingService.TrackRequest($"uri: [GET]tenant/employee/getallemployees - requested by: {_trackingService.GetRequestedUserId()}");
                
            var employees = await _mediator.Send(new GetAllEmployeeRequest());
            var response = new BaseResponse(_trackingService.GetRequestId() ?? string.Empty, employees,
                ResponseType.Success.ToString());
                
            var responseBodyJson = JsonConvert.SerializeObject(response);
            _trackingService.TrackRequest($"uri: [GET]employee/getallemployees - response: {responseBodyJson}");
            return Ok(response);
        }
        catch (Exception ex)
        {
            _trackingService.TrackRequest($"uri: [GET]employee/getallemployees - exception: {ex.Message}");
            return BadRequest(new BaseResponse(_trackingService.GetRequestId() ?? string.Empty,null!, ResponseType.Failed.ToString()));
        }
    }
    
    /// <summary>
    /// Retrieves a single employee.
    /// </summary>
    /// <returns>A response containing a single employee information if successful.</returns>
    /// <response code="200">Returns a single employee if the operation is successful.</response>
    [HttpGet()]
    [ProducesResponseType(typeof(BaseResponse), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(BaseResponse), StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(typeof(BaseResponse), StatusCodes.Status400BadRequest)]
    public async Task<ActionResult<BaseResponse>> Get([FromQuery] GetSingleEmployeeRequest getSingleEmployeeRequest)
    {
        try
        {
            _trackingService.TrackRequest($"uri: [GET]tenant/employee/ - requested by: {_trackingService.GetRequestedUserId()}");
                
            var employee = await _mediator.Send(getSingleEmployeeRequest);
            var response = new BaseResponse(_trackingService.GetRequestId() ?? string.Empty, employee,
                ResponseType.Success.ToString());
                
            var responseBodyJson = JsonConvert.SerializeObject(response);
            _trackingService.TrackRequest($"uri: [GET]employee/ - response: {responseBodyJson}");
            return Ok(response);
        }
        catch (Exception ex)
        {
            _trackingService.TrackRequest($"uri: [GET]employee/ - exception: {ex.Message}");
            return BadRequest(new BaseResponse(_trackingService.GetRequestId() ?? string.Empty,null!, ResponseType.Failed.ToString()));
        }
    }
}

