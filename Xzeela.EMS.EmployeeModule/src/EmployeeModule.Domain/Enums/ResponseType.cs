namespace EmployeeModule.Domain.Enums;

public enum ResponseType
{
    Success,
    Failed,
    NotFound,
    UserDontHaveAccess
}