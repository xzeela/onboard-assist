using System.ComponentModel.DataAnnotations;

namespace EmployeeModule.Domain.Enums;

public enum Gender
{
    [Display(Name = "MALE")]
    male,
    [Display(Name = "FEMALE")]
    female,
}