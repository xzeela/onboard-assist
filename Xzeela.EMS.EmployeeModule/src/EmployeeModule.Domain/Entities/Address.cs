﻿using System;
using Xzeela.EMS.Shared.Entity;

namespace EmployeeModule.Domain.Entities
{
	public class Address : BaseEntity
	{
		public string StreetOne { get; set; } = null!;
		public string? StreetTwo { get; set; }
		public string City { get; set; } = null!;
		public string Province { get; set; } = null!;
		public string Country { get; set; } = null!;
	}
}

