﻿using System;
using EmployeeModule.Domain.Enums;
using Xzeela.EMS.Shared.Entity;

namespace EmployeeModule.Domain.Entities
{
	public class Employee : BaseEntity
	{
		public int TenantId { get; set; }
		public string FirstName { get; set; } = null!;
		public string LastName { get; set; } = null!;
		public string Email { get; set; } = null!;
		public string NIC { get; set; } = null!;
		public Gender Gender { get; set; }
		public Address? Address { get; set; }
	}
}

