﻿using System;
using EmployeeModule.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace EmployeeModule.Infrastructure
{
	public class ApplicationDataContext : DbContext
    {
        public ApplicationDataContext(
            DbContextOptions<ApplicationDataContext> options
        ) : base(options)
        {
        }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Address> Addresses { get; set; }


        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<Employee>()
                .HasKey(employee => new { employee.Id });
            
            builder.Entity<Address>()
                .HasKey(address => new { address.Id });
        }
    }
}

