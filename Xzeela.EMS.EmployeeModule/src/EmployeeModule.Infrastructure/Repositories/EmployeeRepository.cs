﻿using System;
using EmployeeModule.Domain.Entities;
using EmployeeModule.Infrastructure.Interfaces;
using Microsoft.EntityFrameworkCore;
using Xzeela.EMS.Shared.Persistence.Repositories;

namespace EmployeeModule.Infrastructure.Repositories
{
	public class EmployeeRepository : BaseRepository<Employee>, IEmployeeRepository
	{
		private readonly DbSet<Employee> _employees;
		public EmployeeRepository(ApplicationDataContext applicationDbContext) : base(applicationDbContext)
		{
			_applicationDataContext = applicationDbContext;
			_employees = _applicationDataContext.Set<Employee>();
		}

		public async Task<bool> IsEmailAvailable(string email, int tenantId)
		{
			var employee = await _employees.FirstOrDefaultAsync(x => x.Email.ToLower() == email.ToLower() && x.TenantId == tenantId);
			return employee != null;
		}

		public async Task<bool> IsNicAvailable(string nic, int tenantId)
		{
			var employee = await _employees.FirstOrDefaultAsync(x => x.NIC.ToLower() == nic.ToLower() && x.TenantId == tenantId);
			return employee != null;
		}


		public async Task<List<Employee>> GetEmployeesByTenantId(int tenantId)
		{
			return await _employees.Where(x => x.TenantId == tenantId).ToListAsync();
		}
		
		public async Task<Employee?> GetEmployeeByEmailAndTenantId(string email, int tenantId)
		{
			return await _employees.FirstOrDefaultAsync(x => x.Email.ToLower() == email.ToLower() && x.TenantId == tenantId);
		}
	}
}

