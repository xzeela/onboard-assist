using EmployeeModule.Domain.Entities;
using Xzeela.EMS.Shared.Persistence.Interfaces;

namespace EmployeeModule.Infrastructure.Interfaces;

public interface IEmployeeRepository : IAsyncRepository<Employee>
{
    Task<bool> IsEmailAvailable(string email, int tenantId);
    Task<bool> IsNicAvailable(string nic, int tenantId);
    Task<List<Employee>> GetEmployeesByTenantId(int tenantId);
    Task<Employee?> GetEmployeeByEmailAndTenantId(string email, int tenantId);
}