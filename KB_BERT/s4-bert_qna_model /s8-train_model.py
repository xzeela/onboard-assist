import os
import pickle

os.environ['TF_GPU_ALLOCATOR'] = 'cuda_malloc_async'

from datasets import load_dataset, Dataset, DatasetDict
import tensorflow as tf

physical_devices = tf.config.list_physical_devices('GPU')
try:
    tf.config.experimental.set_memory_growth(physical_devices[0], True)
except:
    # Invalid device or cannot modify virtual devices once initialized.
    pass

# from tensorflow.keras.mixed_precision import experimental as mixed_precision
# policy = mixed_precision.Policy('mixed_float16')
# mixed_precision.set_policy(policy)
tf.keras.mixed_precision.set_global_policy('mixed_float16')

print("Loading dataset...")
hf_data_path = "dataset"
# raw_datasets = Dataset.load_from_disk(hf_data_path)
raw_datasets = DatasetDict.load_from_disk(hf_data_path)
print("Dataset loaded.")

print(raw_datasets)

print("Context: ", raw_datasets["train"][0]["context"])
print("Question: ", raw_datasets["train"][0]["question"])
print("Answer: ", raw_datasets["train"][0]["answers"])

raw_datasets["train"].filter(lambda x: len(x["answers"]["text"]) != 1)

print(raw_datasets["validation"][0]["answers"])
print(raw_datasets["validation"][2]["answers"])

print(raw_datasets["validation"][2]["context"])
print(raw_datasets["validation"][2]["question"])

from transformers import AutoTokenizer

model_checkpoint = "bert-base-cased"
tokenizer = AutoTokenizer.from_pretrained(model_checkpoint)

print(tokenizer.is_fast)

context = raw_datasets["train"][0]["context"]
question = raw_datasets["train"][0]["question"]

inputs = tokenizer(question, context)
tokenizer.decode(inputs["input_ids"])

inputs = tokenizer(
    question,
    context,
    max_length=100,
    truncation="only_second",
    stride=50,
    return_overflowing_tokens=True,
)

for ids in inputs["input_ids"]:
    print(tokenizer.decode(ids))

inputs = tokenizer(
    question,
    context,
    max_length=100,
    truncation="only_second",
    stride=50,
    return_overflowing_tokens=True,
    return_offsets_mapping=True,
)
inputs.keys()

print(inputs["overflow_to_sample_mapping"])

inputs = tokenizer(
    raw_datasets["train"][2:6]["question"],
    raw_datasets["train"][2:6]["context"],
    max_length=100,
    truncation="only_second",
    stride=50,
    return_overflowing_tokens=True,
    return_offsets_mapping=True,
)

print(f"The 4 examples gave {len(inputs['input_ids'])} features.")
print(f"Here is where each comes from: {inputs['overflow_to_sample_mapping']}.")

answers = raw_datasets["train"][2:6]["answers"]
start_positions = []
end_positions = []

for i, offset in enumerate(inputs["offset_mapping"]):
    sample_idx = inputs["overflow_to_sample_mapping"][i]
    answer = answers[sample_idx]
    start_char = answer["answer_start"][0]
    end_char = answer["answer_start"][0] + len(answer["text"][0])
    sequence_ids = inputs.sequence_ids(i)

    # Find the start and end of the context
    idx = 0
    while sequence_ids[idx] != 1:
        idx += 1
    context_start = idx
    while sequence_ids[idx] == 1:
        idx += 1
    context_end = idx - 1

    # If the answer is not fully inside the context, label is (0, 0)
    if offset[context_start][0] > start_char or offset[context_end][1] < end_char:
        start_positions.append(0)
        end_positions.append(0)
    else:
        # Otherwise it's the start and end token positions
        idx = context_start
        while idx <= context_end and offset[idx][0] <= start_char:
            idx += 1
        start_positions.append(idx - 1)

        idx = context_end
        while idx >= context_start and offset[idx][1] >= end_char:
            idx -= 1
        end_positions.append(idx + 1)

print(start_positions, end_positions)

idx = 0
sample_idx = inputs["overflow_to_sample_mapping"][idx]
answer = answers[sample_idx]["text"][0]

start = start_positions[idx]
end = end_positions[idx]
labeled_answer = tokenizer.decode(inputs["input_ids"][idx][start: end + 1])

print(f"Theoretical answer: {answer}, labels give: {labeled_answer}")

print(inputs["overflow_to_sample_mapping"])
print(len(inputs["overflow_to_sample_mapping"]))

idx = 3
sample_idx = inputs["overflow_to_sample_mapping"][idx]
answer = answers[sample_idx]["text"][0]

decoded_example = tokenizer.decode(inputs["input_ids"][idx])
print(f"Theoretical answer: {answer}, decoded example: {decoded_example}")

max_length = 384
stride = 128


def preprocess_training_examples(examples):
    questions = [q.strip() for q in examples["question"]]
    inputs = tokenizer(
        questions,
        examples["context"],
        max_length=max_length,
        truncation="only_second",
        stride=stride,
        return_overflowing_tokens=True,
        return_offsets_mapping=True,
        padding="max_length",
    )

    offset_mapping = inputs.pop("offset_mapping")
    sample_map = inputs.pop("overflow_to_sample_mapping")
    answers = examples["answers"]
    start_positions = []
    end_positions = []

    for i, offset in enumerate(offset_mapping):
        sample_idx = sample_map[i]
        answer = answers[sample_idx]
        start_char = answer["answer_start"][0]
        end_char = answer["answer_start"][0] + len(answer["text"][0])
        sequence_ids = inputs.sequence_ids(i)

        # Find the start and end of the context
        idx = 0
        while sequence_ids[idx] != 1:
            idx += 1
        context_start = idx
        while sequence_ids[idx] == 1:
            idx += 1
        context_end = idx - 1

        # If the answer is not fully inside the context, label is (0, 0)
        if offset[context_start][0] > start_char or offset[context_end][1] < end_char:
            start_positions.append(0)
            end_positions.append(0)
        else:
            # Otherwise it's the start and end token positions
            idx = context_start
            while idx <= context_end and offset[idx][0] <= start_char:
                idx += 1
            start_positions.append(idx - 1)

            idx = context_end
            while idx >= context_start and offset[idx][1] >= end_char:
                idx -= 1
            end_positions.append(idx + 1)

    inputs["start_positions"] = start_positions
    inputs["end_positions"] = end_positions
    return inputs

print(len(raw_datasets["train"]))

train_dataset = raw_datasets["train"].map(
    preprocess_training_examples,
    batched=True,
    remove_columns=raw_datasets["train"].column_names,
)
len(raw_datasets["train"]), len(train_dataset)


def preprocess_validation_examples(examples):
    questions = [q.strip() for q in examples["question"]]
    inputs = tokenizer(
        questions,
        examples["context"],
        max_length=max_length,
        truncation="only_second",
        stride=stride,
        return_overflowing_tokens=True,
        return_offsets_mapping=True,
        padding="max_length",
    )

    sample_map = inputs.pop("overflow_to_sample_mapping")
    example_ids = []

    for i in range(len(inputs["input_ids"])):
        sample_idx = sample_map[i]
        example_ids.append(examples["id"][sample_idx])

        sequence_ids = inputs.sequence_ids(i)
        offset = inputs["offset_mapping"][i]
        inputs["offset_mapping"][i] = [
            o if sequence_ids[k] == 1 else None for k, o in enumerate(offset)
        ]

    inputs["example_id"] = example_ids
    return inputs


validation_dataset = raw_datasets["validation"].map(
    preprocess_validation_examples,
    batched=True,
    remove_columns=raw_datasets["validation"].column_names,
)
len(raw_datasets["validation"]), len(validation_dataset)

small_eval_set = raw_datasets["validation"].select(range(100))
trained_checkpoint = "distilbert-base-cased-distilled-squad"

tokenizer = AutoTokenizer.from_pretrained(trained_checkpoint)
eval_set = small_eval_set.map(
    preprocess_validation_examples,
    batched=True,
    remove_columns=raw_datasets["validation"].column_names,
)

tokenizer = AutoTokenizer.from_pretrained(model_checkpoint)

import tensorflow as tf
from transformers import TFAutoModelForQuestionAnswering

eval_set_for_model = eval_set.remove_columns(["example_id", "offset_mapping"])
eval_set_for_model.set_format("numpy")

batch = {k: eval_set_for_model[k] for k in eval_set_for_model.column_names}
trained_model = TFAutoModelForQuestionAnswering.from_pretrained(trained_checkpoint)

outputs = trained_model(**batch)

start_logits = outputs.start_logits.numpy()
end_logits = outputs.end_logits.numpy()

import collections

example_to_features = collections.defaultdict(list)
for idx, feature in enumerate(eval_set):
    example_to_features[feature["example_id"]].append(idx)

import numpy as np

n_best = 20
max_answer_length = 30
predicted_answers = []

for example in small_eval_set:
    example_id = example["id"]
    context = example["context"]
    answers = []

    for feature_index in example_to_features[example_id]:
        start_logit = start_logits[feature_index]
        end_logit = end_logits[feature_index]
        offsets = eval_set["offset_mapping"][feature_index]

        start_indexes = np.argsort(start_logit)[-1: -n_best - 1: -1].tolist()
        end_indexes = np.argsort(end_logit)[-1: -n_best - 1: -1].tolist()
        for start_index in start_indexes:
            for end_index in end_indexes:
                # Skip answers that are not fully in the context
                if offsets[start_index] is None or offsets[end_index] is None:
                    continue
                # Skip answers with a length that is either < 0 or > max_answer_length.
                if (
                        end_index < start_index
                        or end_index - start_index + 1 > max_answer_length
                ):
                    continue

                answers.append(
                    {
                        "text": context[offsets[start_index][0]: offsets[end_index][1]],
                        "logit_score": start_logit[start_index] + end_logit[end_index],
                    }
                )

    best_answer = max(answers, key=lambda x: x["logit_score"])
    predicted_answers.append({"id": example_id, "prediction_text": best_answer["text"]})

import evaluate

metric = evaluate.load("squad")

theoretical_answers = [
    {"id": ex["id"], "answers": ex["answers"]} for ex in small_eval_set
]

print(predicted_answers[0])
print(theoretical_answers[0])

# Format predictions
formatted_predictions = [{'id': str(pred['id']), 'prediction_text': pred['prediction_text']} for pred in
                         predicted_answers]

# Format references
formatted_references = [{'id': str(ref['id']), 'answers': ref['answers']} for ref in theoretical_answers]

# Compute metric with formatted predictions and references
results = metric.compute(predictions=formatted_predictions, references=formatted_references)

print(results)

from tqdm.auto import tqdm


def compute_metrics(start_logits, end_logits, features, examples):
    example_to_features = collections.defaultdict(list)
    for idx, feature in enumerate(features):
        example_to_features[feature["example_id"]].append(idx)

    predicted_answers = []
    for example in tqdm(examples):
        example_id = example["id"]
        context = example["context"]
        answers = []

        # Loop through all features associated with that example
        for feature_index in example_to_features[example_id]:
            start_logit = start_logits[feature_index]
            end_logit = end_logits[feature_index]
            offsets = features[feature_index]["offset_mapping"]

            start_indexes = np.argsort(start_logit)[-1: -n_best - 1: -1].tolist()
            end_indexes = np.argsort(end_logit)[-1: -n_best - 1: -1].tolist()
            for start_index in start_indexes:
                for end_index in end_indexes:
                    # Skip answers that are not fully in the context
                    if offsets[start_index] is None or offsets[end_index] is None:
                        continue
                    # Skip answers with a length that is either < 0 or > max_answer_length
                    if (
                            end_index < start_index
                            or end_index - start_index + 1 > max_answer_length
                    ):
                        continue

                    answer = {
                        "text": context[offsets[start_index][0]: offsets[end_index][1]],
                        "logit_score": start_logit[start_index] + end_logit[end_index],
                    }
                    answers.append(answer)

        # Select the answer with the best score
        if len(answers) > 0:
            best_answer = max(answers, key=lambda x: x["logit_score"])
            predicted_answers.append(
                {"id": example_id, "prediction_text": best_answer["text"]}
            )
        else:
            predicted_answers.append({"id": example_id, "prediction_text": ""})

    theoretical_answers = [{"id": ex["id"], "answers": ex["answers"]} for ex in examples]

    # Format predictions
    formatted_predictions = [{'id': str(pred['id']), 'prediction_text': pred['prediction_text']} for pred in
                             predicted_answers]

    # Format references
    formatted_references = [{'id': str(ref['id']), 'answers': ref['answers']} for ref in theoretical_answers]

    # Compute metric with formatted predictions and references
    results = metric.compute(predictions=formatted_predictions, references=formatted_references)
    return results


compute_metrics(start_logits, end_logits, eval_set, small_eval_set)

model = TFAutoModelForQuestionAnswering.from_pretrained(model_checkpoint)

from transformers import DefaultDataCollator

data_collator = DefaultDataCollator(return_tensors="tf")

tf_train_dataset = model.prepare_tf_dataset(
    train_dataset,
    collate_fn=data_collator,
    shuffle=True,
    batch_size=16,
)
tf_eval_dataset = model.prepare_tf_dataset(
    validation_dataset,
    collate_fn=data_collator,
    shuffle=False,
    batch_size=16,
)

from transformers import create_optimizer
from transformers.keras_callbacks import PushToHubCallback
import tensorflow as tf

# The number of training steps is the number of samples in the dataset, divided by the batch size then multiplied
# by the total number of epochs. Note that the tf_train_dataset here is a batched tf.data.Dataset,
# not the original Hugging Face Dataset, so its len() is already num_samples // batch_size.
num_train_epochs = 3
num_train_steps = len(tf_train_dataset) * num_train_epochs
optimizer, schedule = create_optimizer(
    init_lr=2e-5,
    num_warmup_steps=0,
    num_train_steps=num_train_steps,
    weight_decay_rate=0.01,
)
model.compile(optimizer=optimizer)

# Train in mixed-precision float16
tf.keras.mixed_precision.set_global_policy("mixed_float16")

# from transformers.keras_callbacks import PushToHubCallback

# callback = PushToHubCallback(output_dir="bert-finetuned-squad", tokenizer=tokenizer)

# We're going to do validation afterwards, so no validation mid-training
# model.fit(tf_train_dataset, callbacks=[callback], epochs=num_train_epochs)

from tensorflow.keras.callbacks import ModelCheckpoint, EarlyStopping

# Define the checkpoint callback
checkpoint_cb = ModelCheckpoint(
    "bert-finetuned-squad/ckpt_{epoch}", # Path where to save the model
    save_weights_only=True, # Set to False to save the entire model
    save_best_only=True, # Set to True to save only the best model according to the validation loss
    monitor='val_loss', # The metric to monitor
    verbose=1 # Log level
)

# Optionally, define an EarlyStopping callback
early_stopping_cb = EarlyStopping(
    monitor='val_loss', # The metric to monitor
    patience=3, # The number of epochs to wait before stopping
    verbose=1
)

# Fit the model
history = model.fit(
    tf_train_dataset,
    epochs=5,
    validation_data=tf_eval_dataset, # Make sure you have a validation dataset
    callbacks=[checkpoint_cb, early_stopping_cb] # Add callbacks here
)

# save the model
model.save_pretrained("bert-finetuned-squad")
tokenizer.save_pretrained("bert-finetuned-squad")

# model.fit(tf_train_dataset, epochs=num_train_epochs)
history_path = "bert-finetuned-squad/history.pkl"
with open(history_path, 'wb') as f:
    pickle.dump(history.history, f)



# /home/timnirmal/.virtualenvs/KB/bin/python /mnt/c/D/Projects/KB/KB_BERT/bert_qna_model _4/train_model.py
# 2024-02-11 21:16:19.378246: I tensorflow/core/util/port.cc:113] oneDNN custom operations are on. You may see slightly different numerical results due to floating-point round-off errors from different computation orders. To turn them off, set the environment variable `TF_ENABLE_ONEDNN_OPTS=0`.
# 2024-02-11 21:16:19.419167: E external/local_xla/xla/stream_executor/cuda/cuda_dnn.cc:9261] Unable to register cuDNN factory: Attempting to register factory for plugin cuDNN when one has already been registered
# 2024-02-11 21:16:19.419216: E external/local_xla/xla/stream_executor/cuda/cuda_fft.cc:607] Unable to register cuFFT factory: Attempting to register factory for plugin cuFFT when one has already been registered
# 2024-02-11 21:16:19.420026: E external/local_xla/xla/stream_executor/cuda/cuda_blas.cc:1515] Unable to register cuBLAS factory: Attempting to register factory for plugin cuBLAS when one has already been registered
# 2024-02-11 21:16:19.427956: I tensorflow/core/platform/cpu_feature_guard.cc:182] This TensorFlow binary is optimized to use available CPU instructions in performance-critical operations.
# To enable the following instructions: AVX2 AVX_VNNI FMA, in other operations, rebuild TensorFlow with the appropriate compiler flags.
# 2024-02-11 21:16:19.975147: W tensorflow/compiler/tf2tensorrt/utils/py_utils.cc:38] TF-TRT Warning: Could not find TensorRT
# 2024-02-11 21:16:21.408805: I external/local_xla/xla/stream_executor/cuda/cuda_executor.cc:887] could not open file to read NUMA node: /sys/bus/pci/devices/0000:01:00.0/numa_node
# Your kernel may have been built without NUMA support.
# 2024-02-11 21:16:21.452651: I external/local_xla/xla/stream_executor/cuda/cuda_executor.cc:887] could not open file to read NUMA node: /sys/bus/pci/devices/0000:01:00.0/numa_node
# Your kernel may have been built without NUMA support.
# 2024-02-11 21:16:21.452730: I external/local_xla/xla/stream_executor/cuda/cuda_executor.cc:887] could not open file to read NUMA node: /sys/bus/pci/devices/0000:01:00.0/numa_node
# Your kernel may have been built without NUMA support.
# 2024-02-11 21:16:21.459457: I external/local_xla/xla/stream_executor/cuda/cuda_executor.cc:887] could not open file to read NUMA node: /sys/bus/pci/devices/0000:01:00.0/numa_node
# Your kernel may have been built without NUMA support.
# Loading dataset...
# Dataset loaded.
# DatasetDict({
#     train: Dataset({
#         features: ['context', 'question', 'id', 'title', 'answers', '__index_level_0__'],
#         num_rows: 160201
#     })
#     validation: Dataset({
#         features: ['context', 'question', 'id', 'title', 'answers', '__index_level_0__'],
#         num_rows: 40051
#     })
# })
# Context:  User ID 109 is assigned to the project 'Spring XD' with the project key 'XD'. They have participated in the sprint 'Sprint 21', which is currently in the 'CLOSED' state.
# Question:  Which sprint has User ID 109 participated in for the project 'Spring XD'?
# Answer:  {'answer_start': [116], 'text': ['Sprint 21']}
# Filter: 100%|██████████| 160201/160201 [00:08<00:00, 19604.12 examples/s]
# {'answer_start': [466], 'text': ['2016-09-27 17:35:48']}
# {'answer_start': [19], 'text': ['known for their contributions']}
# User ID 11683.0 is known for their contributions to the project 'Atlassian Crowd' (ID 16). They have worked on issues including "a single Capital in a email address after a dot is not accepted". This involvement showcases the user's active role in the development and progression of the project.
# What has been the role of User ID 11683.0 in the development of project 'Atlassian Crowd'?
# /home/timnirmal/.virtualenvs/KB/lib/python3.10/site-packages/transformers/utils/generic.py:441: UserWarning: torch.utils._pytree._register_pytree_node is deprecated. Please use torch.utils._pytree.register_pytree_node instead.
#   _torch_pytree._register_pytree_node(
# True
# [CLS] Which sprint has User ID 109 participated in for the project'Spring XD '? [SEP] User ID 109 is assigned to the project'Spring XD'with the project key'XD '. They have participated in the sprint'Sprint 21 ', which is currently in the'CLOSED'state. [SEP]
# [0]
# The 4 examples gave 5 features.
# Here is where each comes from: [0, 1, 2, 2, 3].
# [20, 37, 70, 40, 19] [23, 40, 71, 41, 21]
# Theoretical answer: 1451.0, labels give: 1451. 0
# [0, 1, 2, 2, 3]
# 5
# Theoretical answer: 0 minutes, decoded example: [CLS] How much total effort in minutes has been spent on the issue with ID 3343? [SEP] status of'Complete '. With a story point of 5. 0, the time spent in progress is 0 minutes, and the total effort is 0 minutes. The issue has undergone changes, including'DESCRIPTION, STATUS, OTHER'modifications, reflecting its dynamic lifecycle. [SEP]
# 160201
# Map: 100%|██████████| 160201/160201 [00:35<00:00, 4482.67 examples/s]
# Map: 100%|██████████| 40051/40051 [00:13<00:00, 2922.15 examples/s]
# Map: 100%|██████████| 100/100 [00:00<00:00, 1431.49 examples/s]
# /home/timnirmal/.virtualenvs/KB/lib/python3.10/site-packages/transformers/utils/generic.py:309: UserWarning: torch.utils._pytree._register_pytree_node is deprecated. Please use torch.utils._pytree.register_pytree_node instead.
#   _torch_pytree._register_pytree_node(
# /home/timnirmal/.virtualenvs/KB/lib/python3.10/site-packages/transformers/utils/generic.py:309: UserWarning: torch.utils._pytree._register_pytree_node is deprecated. Please use torch.utils._pytree.register_pytree_node instead.
#   _torch_pytree._register_pytree_node(
# 2024-02-11 21:18:08.464566: I external/local_xla/xla/stream_executor/cuda/cuda_executor.cc:887] could not open file to read NUMA node: /sys/bus/pci/devices/0000:01:00.0/numa_node
# Your kernel may have been built without NUMA support.
# 2024-02-11 21:18:08.464749: I external/local_xla/xla/stream_executor/cuda/cuda_executor.cc:887] could not open file to read NUMA node: /sys/bus/pci/devices/0000:01:00.0/numa_node
# Your kernel may have been built without NUMA support.
# 2024-02-11 21:18:08.464784: I external/local_xla/xla/stream_executor/cuda/cuda_executor.cc:887] could not open file to read NUMA node: /sys/bus/pci/devices/0000:01:00.0/numa_node
# Your kernel may have been built without NUMA support.
# 2024-02-11 21:18:08.902834: I external/local_xla/xla/stream_executor/cuda/cuda_executor.cc:887] could not open file to read NUMA node: /sys/bus/pci/devices/0000:01:00.0/numa_node
# Your kernel may have been built without NUMA support.
# 2024-02-11 21:18:08.903316: I external/local_xla/xla/stream_executor/cuda/cuda_executor.cc:887] could not open file to read NUMA node: /sys/bus/pci/devices/0000:01:00.0/numa_node
# Your kernel may have been built without NUMA support.
# 2024-02-11 21:18:08.903357: I tensorflow/core/common_runtime/gpu/gpu_device.cc:2022] Could not identify NUMA node of platform GPU id 0, defaulting to 0.  Your kernel may not have been built with NUMA support.
# 2024-02-11 21:18:08.903400: I tensorflow/core/common_runtime/gpu/gpu_process_state.cc:236] Using CUDA malloc Async allocator for GPU: 0
# 2024-02-11 21:18:08.903993: I external/local_xla/xla/stream_executor/cuda/cuda_executor.cc:887] could not open file to read NUMA node: /sys/bus/pci/devices/0000:01:00.0/numa_node
# Your kernel may have been built without NUMA support.
# 2024-02-11 21:18:08.904671: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1929] Created device /job:localhost/replica:0/task:0/device:GPU:0 with 9511 MB memory:  -> device: 0, name: NVIDIA GeForce RTX 4080 Laptop GPU, pci bus id: 0000:01:00.0, compute capability: 8.9
# 2024-02-11 21:18:09.768776: I external/local_tsl/tsl/platform/default/subprocess.cc:304] Start cannot spawn child process: No such file or directory
# All PyTorch model weights were used when initializing TFDistilBertForQuestionAnswering.
#
# All the weights of TFDistilBertForQuestionAnswering were initialized from the PyTorch model.
# If your task is similar to the task the model of the checkpoint was trained on, you can already use TFDistilBertForQuestionAnswering for predictions without further training.
# {'id': 30613, 'prediction_text': '2016-09-27 17:35:48'}
# {'id': 30613, 'answers': {'answer_start': [466], 'text': ['2016-09-27 17:35:48']}}
# {'exact_match': 57.0, 'f1': 67.75504423113117}
# 100%|██████████| 100/100 [00:00<00:00, 1342.79it/s]
# All PyTorch model weights were used when initializing TFBertForQuestionAnswering.
#
# Some weights or buffers of the TF 2.0 model TFBertForQuestionAnswering were not initialized from the PyTorch model and are newly initialized: ['qa_outputs.weight', 'qa_outputs.bias']
# You should probably TRAIN this model on a down-stream task to be able to use it for predictions and inference.
# Epoch 1/5
# 10548/10548 [==============================] - ETA: 0s - loss: 0.0351
# Epoch 1: val_loss improved from inf to 0.00000, saving model to bert-finetuned-squad/ckpt_1
# 10548/10548 [==============================] - 2980s 281ms/step - loss: 0.0351 - val_loss: 0.0000e+00
# Epoch 2/5
# 10548/10548 [==============================] - ETA: 0s - loss: 0.0043
# Epoch 2: val_loss did not improve from 0.00000
# 10548/10548 [==============================] - 2986s 283ms/step - loss: 0.0043 - val_loss: 0.0000e+00
# Epoch 3/5
# 10548/10548 [==============================] - ETA: 0s - loss: 0.0017
# Epoch 3: val_loss did not improve from 0.00000
# 10548/10548 [==============================] - 2733s 259ms/step - loss: 0.0017 - val_loss: 0.0000e+00
# Epoch 4/5
# 10548/10548 [==============================] - ETA: 0s - loss: 9.4067e-04
# Epoch 4: val_loss did not improve from 0.00000
# 10548/10548 [==============================] - 2718s 258ms/step - loss: 9.4067e-04 - val_loss: 0.0000e+00
# Epoch 4: early stopping
# Exception ignored in: <function AtomicFunction.__del__ at 0x7f1c7832a7a0>
# Traceback (most recent call last):
#   File "/home/timnirmal/.virtualenvs/KB/lib/python3.10/site-packages/tensorflow/python/eager/polymorphic_function/atomic_function.py", line 291, in __del__
# TypeError: 'NoneType' object is not subscriptable
# Exception ignored in: <function AtomicFunction.__del__ at 0x7f1c7832a7a0>
# Traceback (most recent call last):
#   File "/home/timnirmal/.virtualenvs/KB/lib/python3.10/site-packages/tensorflow/python/eager/polymorphic_function/atomic_function.py", line 291, in __del__
# TypeError: 'NoneType' object is not subscriptable
# Exception ignored in: <function AtomicFunction.__del__ at 0x7f1c7832a7a0>
# Traceback (most recent call last):
#   File "/home/timnirmal/.virtualenvs/KB/lib/python3.10/site-packages/tensorflow/python/eager/polymorphic_function/atomic_function.py", line 291, in __del__
# TypeError: 'NoneType' object is not subscriptable
# Exception ignored in: <function AtomicFunction.__del__ at 0x7f1c7832a7a0>
# Traceback (most recent call last):
#   File "/home/timnirmal/.virtualenvs/KB/lib/python3.10/site-packages/tensorflow/python/eager/polymorphic_function/atomic_function.py", line 291, in __del__
# TypeError: 'NoneType' object is not subscriptable
# Exception ignored in: <function AtomicFunction.__del__ at 0x7f1c7832a7a0>
# Traceback (most recent call last):
#   File "/home/timnirmal/.virtualenvs/KB/lib/python3.10/site-packages/tensorflow/python/eager/polymorphic_function/atomic_function.py", line 291, in __del__
# TypeError: 'NoneType' object is not subscriptable
# Exception ignored in: <function AtomicFunction.__del__ at 0x7f1c7832a7a0>
# Traceback (most recent call last):
#   File "/home/timnirmal/.virtualenvs/KB/lib/python3.10/site-packages/tensorflow/python/eager/polymorphic_function/atomic_function.py", line 291, in __del__
# TypeError: 'NoneType' object is not subscriptable
#
# Process finished with exit code 0