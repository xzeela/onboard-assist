from transformers import TFAutoModelForQuestionAnswering
import tensorflow as tf

model_directory = "bert-finetuned-squad"  # The path where you saved your model
model = TFAutoModelForQuestionAnswering.from_pretrained(model_directory)

from transformers import AutoTokenizer

tokenizer_directory = "bert-finetuned-squad"  # The path where you saved your tokenizer
tokenizer = AutoTokenizer.from_pretrained(tokenizer_directory)

# context = "The sprint, starting on 2014-07-21 and ending on 2014-07-29, was completed on 2014-07-29. It included issues with an estimated completion date of 2014-04-07 and an actual resolution date of 2014-07-25. The sprint comprised issues with varying story points, reflecting the sprint's planning and execution complexity."
# question = "When did the sprint including the issue with estimated completion date of 2014-04-07 start?"

context = "The version '1.3 GA' associated with the project 'Spring XD' was released on 2015-11-19 00:00:00. It is currently not archived and is described as '1.3.0 GA Release'. The release status is 'officially released'."
question = "On what date was the version '1.3 GA' released for project 'Spring XD'?"

# Tokenize the input (question and context)
inputs = tokenizer(question, context, return_tensors="tf", max_length=512, truncation=True)

# Generate answer predictions
output = model(inputs)
answer_start_scores, answer_end_scores = output.start_logits, output.end_logits

# Find the tokens with the highest `start` and `end` scores
answer_start = tf.argmax(answer_start_scores, axis=1).numpy()[0]
answer_end = (tf.argmax(answer_end_scores, axis=1) + 1).numpy()[0]  # Add 1 to get the actual end position

# Convert tokens to answer string
answer = tokenizer.convert_tokens_to_string(
    tokenizer.convert_ids_to_tokens(inputs["input_ids"][0][answer_start:answer_end]))

print("Answer:", answer)
