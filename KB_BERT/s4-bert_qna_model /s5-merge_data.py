import pandas as pd
from tqdm import tqdm

# List of file paths
context_paths = [
    "../create_qna_dataset/cqa_dataset/1_context.csv",
    "../create_qna_dataset/cqa_dataset/2_context.csv",
    "../create_qna_dataset/cqa_dataset/3_context.csv",
    "../create_qna_dataset/cqa_dataset/4_context.csv",
    # "../create_qna_dataset/cqa_dataset/5_context.csv",
    "../create_qna_dataset/cqa_dataset/6_context_part_1.csv",
    "../create_qna_dataset/cqa_dataset/7_context.csv",
    "../create_qna_dataset/cqa_dataset/8_context.csv",
    "../create_qna_dataset/cqa_dataset/9_context.csv",
    "../create_qna_dataset/cqa_dataset/10_context_part_1.csv",
    "../create_qna_dataset/cqa_dataset/11_context.csv"
]

qa_paths = [
    "../create_qna_dataset/cqa_dataset/1_question_answer.csv",
    "../create_qna_dataset/cqa_dataset/2_question_answer.csv",
    "../create_qna_dataset/cqa_dataset/3_question_answer.csv",
    "../create_qna_dataset/cqa_dataset/4_question_answer.csv",
    # "../create_qna_dataset/cqa_dataset/5_question_answer.csv",
    "../create_qna_dataset/cqa_dataset/6_question_answer_part_1.csv",
    "../create_qna_dataset/cqa_dataset/7_question_answer.csv",
    "../create_qna_dataset/cqa_dataset/8_question_answer.csv",
    "../create_qna_dataset/cqa_dataset/9_question_answer.csv",
    "../create_qna_dataset/cqa_dataset/10_question_answer_part_1.csv",
    "../create_qna_dataset/cqa_dataset/11_question_answer.csv"
]

# Initialize an empty DataFrame for concatenation
concatenated_df = pd.DataFrame()

# Loop through the file paths and concatenate
for file_path in tqdm(context_paths, desc="Reading and concatenating files"):
    temp_df = pd.read_csv(file_path).iloc[:10000]
    print("running")
    print("temp_df length: ", len(temp_df))
    temp_qa_df = pd.read_csv(qa_paths[context_paths.index(file_path)])
    # keep only rows with matching Context_ID
    temp_qa_df = temp_qa_df[temp_qa_df['Context_ID'].isin(temp_df['Context_ID'])]
    # merge the two dataframes
    temp_df = pd.merge(temp_df, temp_qa_df, on='Context_ID', how='left')
    # remove Context ID column
    temp_df = temp_df.drop(columns=['Context_ID'])
    print("temp_df length after merge: ", len(temp_df))

    concatenated_df = pd.concat([concatenated_df, temp_df], ignore_index=True)
    print("concatenated_df length: ", len(concatenated_df))

# Write the final DataFrame to a new CSV file
concatenated_df.to_csv("project_df_final_10000_2.csv", index=False)
print("project_df_final_10000.csv saved")

print("Done")