import pandas as pd

df = pd.read_csv('project_df_final_10000_2.csv')

original_len = len(df)
print(len(df))

# check for null values
print(df.isnull().sum())

# remove null values, reset index, save to disk
df = df.dropna()
df = df.reset_index(drop=True)

print(len(df))

print(original_len-4353)


df.to_csv('project_df_final_10000_2.csv', index=False)



# # save sample dataset
# df.iloc[:1000].to_csv('project_df_final_10000_sample.csv', index=False)