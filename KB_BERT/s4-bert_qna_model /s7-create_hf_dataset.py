# import os
# import glob
# import re
# from tqdm.auto import tqdm
#
# # Set the path to the directory containing the CSV files
# data_dir = '../create_qna_dataset/cqa_dataset'
#
# # List all files in the directory that match the context and question_answer pattern
# context_files = glob.glob(os.path.join(data_dir, '*_context.csv')) + glob.glob(
#     os.path.join(data_dir, '*_context_part_*.csv'))
# qa_files = glob.glob(os.path.join(data_dir, '*_question_answer.csv')) + glob.glob(
#     os.path.join(data_dir, '*_question_answer_part_*.csv'))
#
#
# # # now pair items to a dictionary
# # qa_context_files = dict(zip(context_files, qa_files))
# # for key, value in qa_context_files.items():
# #     print(f"{key} : {value}")
#
#
# # Function to extract the dataset number from the file name
# def extract_dataset_number(file_name):
#     # This will match the first occurrence of a number followed by an underscore and 'context' or 'question_answer'
#     match = re.search(r'(\d+)_context.*\.csv', file_name) or re.search(r'(\d+)_question_answer.*\.csv', file_name)
#     if match:
#         return int(match.group(1))
#     return None
#
#
# # Group the context and question_answer files by their dataset number
# grouped_files = {}
# for file in context_files + qa_files:
#     dataset_number = extract_dataset_number(file)
#     if dataset_number not in grouped_files:
#         grouped_files[dataset_number] = {'context': [], 'question_answer': []}
#
#     if 'context' in file:
#         grouped_files[dataset_number]['context'].append(file)
#     elif 'question_answer' in file:
#         grouped_files[dataset_number]['question_answer'].append(file)
#
# # Sort the files within each group
# for dataset_number in grouped_files:
#     grouped_files[dataset_number]['context'].sort()
#     grouped_files[dataset_number]['question_answer'].sort()
#
# # sort by dataset number
# grouped_files = dict(sorted(grouped_files.items()))
#
# for key, value in grouped_files.items():
#     print(f"Dataset {key}: {value}")
#
# import pandas as pd
#
# max_unique_contexts = 100000
#
# # create dataframe to hold all the data
# final_df = pd.DataFrame()
#
# # for each dataset
# for dataset_number, files in grouped_files.items():
#     print(f"Processing dataset {dataset_number}")
#     print(files)
#
#     # for length of context files
#     for i in tqdm(range(len(files['context']))):
#         print(f"Processing context file {files['context'][i]}")
#         context_df = pd.read_csv(files['context'][i], nrows=max_unique_contexts)
#
#         # read corresponding question_answer file
#         qa_file = files['question_answer'][i]
#         print(f"Processing question_answer file {qa_file}")
#         qa_df = pd.read_csv(qa_file)
#
#         # in qa_df keep only context_id in context_df (both have Context_ID)
#         qa_df = qa_df[qa_df['Context_ID'].isin(context_df['Context_ID'])]
#
#         # merge the two dataframes
#         merged_df = pd.merge(context_df, qa_df, on='Context_ID', how='left')
#
#         # append to final_df
#         final_df = pd.concat([final_df, merged_df], ignore_index=True)
#
#         print(f"Processed {len(merged_df)} rows")
#
#     print(f"Processed dataset {dataset_number}")
#
# # if there are null values in the final_df, remove them
# final_df = final_df.dropna()
#
# # save to csv
# final_df.to_csv("final_df.csv", index=False)
#


print("Hello World")

import pandas as pd
from datasets import Dataset, DatasetDict
from sklearn.model_selection import train_test_split


def csv_to_dataset_dict_simple(csv_file, test_size=0.2):
    # Load the CSV file into a DataFrame
    df = pd.read_csv(csv_file)

    # Prepare data by renaming columns to match the required format
    df = df.rename(columns={
        'Context': 'context',
        'Question': 'question',
        'Answer': 'answer_text',
        'Answer_Start_Index': 'answer_start'
    })

    # Adding 'id' and 'title' columns
    df['id'] = df.index
    df['title'] = "Your Title Here"  # Placeholder title if needed

    # Formatting 'answers' column to match the expected structure
    df['answers'] = df.apply(lambda row: {'text': [row['answer_text']], 'answer_start': [row['answer_start']]}, axis=1)

    # Drop the now redundant 'answer_text' and 'answer_start' columns
    df = df.drop(columns=['answer_text', 'answer_start'])

    # Split the data into train and validation sets
    train_df, val_df = train_test_split(df, test_size=test_size)

    # Convert to Hugging Face Datasets
    train_dataset = Dataset.from_pandas(train_df)
    val_dataset = Dataset.from_pandas(val_df)

    # Create a DatasetDict
    dataset_dict = DatasetDict({
        'train': train_dataset,
        'validation': val_dataset
    })

    return dataset_dict


# Usage
dataset_dict = csv_to_dataset_dict_simple("project_df_final_10000_2.csv")

# Save to disk
dataset_dict.save_to_disk("dataset")
