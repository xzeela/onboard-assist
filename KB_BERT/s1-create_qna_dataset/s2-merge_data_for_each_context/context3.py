import pandas as pd

issue_df = pd.read_csv('../extract_github_data/updated_issues.csv')
print("issue_df loaded")
# ['ID', 'Jira_ID', 'Issue_Key', 'URL', 'Title', 'Description', 'Description_Text', 'Description_Code', 'Type', 'Priority', 'Status', 'Resolution', 'Creation_Date', 'Estimation_Date', 'Resolution_Date', 'Last_Updated', 'Story_Point', 'Timespent', 'In_Progress_Minutes', 'Total_Effort_Minutes', 'Resolution_Time_Minutes', 'Title_Changed_After_Estimation', 'Description_Changed_After_Estimation', 'Story_Point_Changed_After_Estimation', 'Pull_Request_URL', 'Creator_ID', 'Reporter_ID', 'Assignee_ID', 'Project_ID', 'Sprint_ID', 'Owner', 'Repo', 'Pull_Number', 'PR_ID', 'PR_Title', 'PR_State', 'Is_Mergeable', 'Mergeable_State', 'Base_Branch', 'Head_Branch', 'Content', 'Diff']
issue_component_df = pd.read_csv('../../database/issue_component.csv')
print("issue_component_df loaded")
# ['Issue_ID', 'Component_ID']
issue_link_df = pd.read_csv('../../database/issue_link.csv')
print("issue_link_df loaded")
# ['ID', 'Issue_ID', 'Name', 'Description', 'Direction', 'Target_Issue_ID']
project_df = pd.read_csv('../../database/project.csv')
print("project_df loaded")
# ['ID', 'Project_Key', 'Name', 'URL', 'Description', 'Start_Date', 'Last_Update_Date', 'SP_Field_ID', 'Repository_ID']
sprint_df = pd.read_csv('../../database/sprint.csv')
print("sprint_df loaded")
# ['ID', 'JiraID', 'Name', 'State', 'Start_Date', 'End_Date', 'Activated_Date', 'Complete_Date', 'Project_ID']

print("Dataframes loaded")

# Context 3: Sprint Overview with Associated Issues
# Tables and Attributes Used: Sprint, Issue, Project
context_3 = {
    'template': "In the sprint '[Sprint_Name]' (ID [Sprint_ID]) for project '[Project_Name]', which is currently '[State]', the following issues are addressed: [Issue_Titles]. The sprint started on [Start_Date], is planned to end on [End_Date], and is scheduled for completion on [Complete_Date]."
}

# Context 3 Q&A Templates
qa_templates_context_3 = [
    {
        'question_template': "What is the current state of the sprint '[Sprint_Name]'?",
        'answer_template': "[State]",
        'answer_start_placeholder': "[State_Start_Index]"
    },
    {
        'question_template': "When is the sprint '[Sprint_Name]' scheduled to be completed?",
        'answer_template': "[Complete_Date]",
        'answer_start_placeholder': "[Complete_Date_Start_Index]"
    },
    {
        'question_template': "Which issues are being addressed in the sprint '[Sprint_Name]'?",
        'answer_template': "[Issue_Titles]",
        'answer_start_placeholder': "[Issue_Titles_Start_Index]"
    }
]

# Prepare the individual dataframes
sprint_df = sprint_df.rename(columns={'ID': 'Sprint_ID', 'Name': 'Sprint_Name'})
issue_df = issue_df.rename(columns={'ID': 'Issue_ID', 'Title': 'Issue_Title'})
project_df = project_df.rename(columns={'ID': 'Project_ID', 'Name': 'Project_Name'})

# Merge Sprint with Project
merged_df = pd.merge(sprint_df, project_df, on='Project_ID', how='left')

# Merge with Issue
merged_df = pd.merge(merged_df, issue_df, on='Sprint_ID', how='left')

# Aggregate Issue Titles
merged_df['Issue_Titles'] = merged_df.groupby('Sprint_ID')['Issue_Title'].transform(
    lambda x: ', '.join(x.dropna().unique()))
merged_df.drop(columns=['Issue_Title', 'Issue_ID'], inplace=True)

print(merged_df.columns)

# Dropping duplicates and selecting necessary columns
final_columns = ['Sprint_Name', 'Sprint_ID', 'Project_Name', 'State', 'Start_Date_x', 'End_Date', 'Complete_Date',
                 'Issue_Titles']
final_df = merged_df[final_columns].drop_duplicates('Sprint_ID')

# Save to CSV
final_df.to_csv('../data_for_each_context/project_df_context_3.csv', index=False)
print("project_df_context_3 saved to 'project_df_context_3.csv'")
