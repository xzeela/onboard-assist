import pandas as pd

change_log_df = pd.read_csv('../../database/change_log.csv')
print("change_log_df loaded")
# ['ID', 'Field', 'From_Value', 'To_Value', 'From_String', 'To_String', 'Change_Type', 'Creation_Date', 'Author_ID', 'Issue_ID']
issue_df = pd.read_csv('../extract_github_data/updated_issues.csv')
print("issue_df loaded")
# ['ID', 'Jira_ID', 'Issue_Key', 'URL', 'Title', 'Description', 'Description_Text', 'Description_Code', 'Type', 'Priority', 'Status', 'Resolution', 'Creation_Date', 'Estimation_Date', 'Resolution_Date', 'Last_Updated', 'Story_Point', 'Timespent', 'In_Progress_Minutes', 'Total_Effort_Minutes', 'Resolution_Time_Minutes', 'Title_Changed_After_Estimation', 'Description_Changed_After_Estimation', 'Story_Point_Changed_After_Estimation', 'Pull_Request_URL', 'Creator_ID', 'Reporter_ID', 'Assignee_ID', 'Project_ID', 'Sprint_ID', 'Owner', 'Repo', 'Pull_Number', 'PR_ID', 'PR_Title', 'PR_State', 'Is_Mergeable', 'Mergeable_State', 'Base_Branch', 'Head_Branch', 'Content', 'Diff']


print("Dataframes loaded")

# Context 5: Change History for an Issue
# Tables and Attributes Used: Change_Log, Issue
context_5 = {
    'template': "For issue '[Issue_Title]' (ID [Issue_ID]), the change history records an entry (ID [Change_ID]) indicating that the '[Field]' was updated from '[From_Value]' to '[To_Value]' by author ID [Author_ID] on [Creation_Date]. This modification is part of the issue's evolution through the resolution process."
}

# Context 5 Q&A Templates
qa_templates_context_5 = [
    {
        'question_template': "Which field was updated in the change history of issue ID [Issue_ID]?",
        'answer_template': "[Field]",
        'answer_start_placeholder': "[Field_Start_Index]"
    },
    {
        'question_template': "After the update to issue ID [Issue_ID], what is the new value of '[Field]'?",
        'answer_template': "[To_Value]",
        'answer_start_placeholder': "[To_Value_Start_Index]"
    },
    {
        'question_template': "When was the change made to the field '[Field]' of issue ID [Issue_ID]?",
        'answer_template': "[Creation_Date]",
        'answer_start_placeholder': "[Creation_Date_Start_Index]"
    },
]

print("Context 5: Change History for an Issue")
# Prepare the individual dataframes
change_log_df = change_log_df.rename(
    columns={'ID': 'Change_ID', 'Field': 'Field', 'From_Value': 'From_Value', 'To_Value': 'To_Value',
             'Creation_Date': 'Creation_Date', 'Author_ID': 'Author_ID', 'Issue_ID': 'Issue_ID'})
issue_df = issue_df.rename(columns={'ID': 'Issue_ID', 'Title': 'Issue_Title'})

print("Dataframes prepared")
n = 3  # Number of parts to divide the dataframe into
total_rows = change_log_df.shape[0]
chunk_size = total_rows // n

for i in range(n):
    start = i * chunk_size
    # Ensure the last chunk contains all remaining rows
    end = (i + 1) * chunk_size if i < n - 1 else total_rows
    chunk = change_log_df.iloc[start:end]
    merged_chunk = pd.merge(chunk, issue_df, on='Issue_ID', how='left')
    file_name = f'../data_for_each_context/project_df_context_5_part_{i + 1}.csv'
    merged_chunk.to_csv(file_name, index=False)
    print(f"{file_name} saved")
