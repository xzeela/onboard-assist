import pandas as pd

affected_version_df = pd.read_csv('../../database/affected_version.csv')
print("affected_version_df loaded")
# ['Issue_ID', 'Affected_Version_ID']
change_log_df = pd.read_csv('../../database/change_log.csv')
print("change_log_df loaded")
# ['ID', 'Field', 'From_Value', 'To_Value', 'From_String', 'To_String', 'Change_Type', 'Creation_Date', 'Author_ID', 'Issue_ID']
comment_df = pd.read_csv('../../database/comment.csv')
print("comment_df loaded")
# ['ID', 'Comment', 'Comment_Text', 'Comment_Code', 'Creation_Date', 'Author_ID', 'Issue_ID']
component_df = pd.read_csv('../../database/component.csv')
print("component_df loaded")
# ['ID', 'Jira_ID', 'Name', 'Description', 'Project_ID']
fix_version_df = pd.read_csv('../../database/fix_version.csv')
print("fix_version_df loaded")
# ['Issue_ID', 'Fix_Version_ID']
issue_df = pd.read_csv('../extract_github_data/updated_issues.csv')
print("issue_df loaded")
# ['ID', 'Jira_ID', 'Issue_Key', 'URL', 'Title', 'Description', 'Description_Text', 'Description_Code', 'Type', 'Priority', 'Status', 'Resolution', 'Creation_Date', 'Estimation_Date', 'Resolution_Date', 'Last_Updated', 'Story_Point', 'Timespent', 'In_Progress_Minutes', 'Total_Effort_Minutes', 'Resolution_Time_Minutes', 'Title_Changed_After_Estimation', 'Description_Changed_After_Estimation', 'Story_Point_Changed_After_Estimation', 'Pull_Request_URL', 'Creator_ID', 'Reporter_ID', 'Assignee_ID', 'Project_ID', 'Sprint_ID', 'Owner', 'Repo', 'Pull_Number', 'PR_ID', 'PR_Title', 'PR_State', 'Is_Mergeable', 'Mergeable_State', 'Base_Branch', 'Head_Branch', 'Content', 'Diff']
issue_component_df = pd.read_csv('../../database/issue_component.csv')
print("issue_component_df loaded")
# ['Issue_ID', 'Component_ID']
issue_link_df = pd.read_csv('../../database/issue_link.csv')
print("issue_link_df loaded")
# ['ID', 'Issue_ID', 'Name', 'Description', 'Direction', 'Target_Issue_ID']
project_df = pd.read_csv('../../database/project.csv')
print("project_df loaded")
# ['ID', 'Project_Key', 'Name', 'URL', 'Description', 'Start_Date', 'Last_Update_Date', 'SP_Field_ID', 'Repository_ID']
repository_df = pd.read_csv('../../database/repository.csv')
print("repository_df loaded")
# ['ID', 'Name', 'Description', 'URL']
sprint_df = pd.read_csv('../../database/sprint.csv')
print("sprint_df loaded")
# ['ID', 'JiraID', 'Name', 'State', 'Start_Date', 'End_Date', 'Activated_Date', 'Complete_Date', 'Project_ID']
user_df = pd.read_csv('../../database/user.csv')
print("user_df loaded")
# ['ID', 'Project_ID']
version_df = pd.read_csv('../../database/version.csv')
print("version_df loaded")
# ['ID', 'Jira_ID', 'Name', 'Description', 'Archived', 'Released', 'Release_Date', 'Project_ID']

print("Dataframes loaded")



# Context 11: Sprint Planning and Execution Details
# Tables and Attributes Used: Sprint, Issue
context_11 = {
    'template': "The sprint, starting on [Start_Date] and ending on [End_Date], was completed on [Complete_Date]. It included issues with an estimated completion date of [Estimation_Date] and an actual resolution date of [Resolution_Date]. The sprint comprised issues with varying story points, reflecting the sprint's planning and execution complexity."
}

# Context 11 Q&A Templates
qa_templates_context_11 = [
    {
        'question_template': "When did the sprint including the issue with estimated completion date of [Estimation_Date] start?",
        'answer_template': "[Start_Date]",
        'answer_start_placeholder': "[Start_Date_Start_Index]"
    },
    {
        'question_template': "When was the sprint '[Sprint_Name]' completed?",
        'answer_template': "[Complete_Date]",
        'answer_start_placeholder': "[Complete_Date_Start_Index]"
    },
    {
        'question_template': "What are the story points of the issues included in the sprint '[Sprint_Name]'?",
        'answer_template': "issues with varying story points",
        'answer_start_placeholder': "story points_Start_Index"
    },
]

# Prepare the individual dataframes
sprint_df = sprint_df.rename(columns={'ID': 'Sprint_ID', 'Name': 'Sprint_Name', 'Start_Date': 'Start_Date', 'End_Date': 'End_Date', 'Complete_Date': 'Complete_Date'})
issue_df = issue_df.rename(columns={'Sprint_ID': 'Sprint_ID', 'Estimation_Date': 'Estimation_Date', 'Resolution_Date': 'Resolution_Date', 'Story_Point': 'Story_Point'})

# Merge Sprint with Issue
merged_df = pd.merge(sprint_df, issue_df, on='Sprint_ID', how='left')

# Aggregating Issue-Related Data
# Aggregating Story Points
merged_df['Story_Points'] = merged_df.groupby('Sprint_ID')['Story_Point'].transform(lambda x: ', '.join(map(str, x.dropna().unique())))
merged_df.drop(columns=['Story_Point'], inplace=True)

# Selecting necessary columns
final_columns = ['Sprint_Name', 'Start_Date', 'End_Date', 'Complete_Date', 'Estimation_Date', 'Resolution_Date', 'Story_Points']
final_df = merged_df[final_columns]

# Save to CSV
final_df.to_csv('../data_for_each_context/project_df_context_11.csv', index=False)
print("project_df_context_11 saved to 'project_df_context_11.csv'")