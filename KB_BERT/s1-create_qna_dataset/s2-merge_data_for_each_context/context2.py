import pandas as pd

affected_version_df = pd.read_csv('../../database/affected_version.csv')
print("affected_version_df loaded")
# ['Issue_ID', 'Affected_Version_ID']
change_log_df = pd.read_csv('../../database/change_log.csv')
print("change_log_df loaded")
# ['ID', 'Field', 'From_Value', 'To_Value', 'From_String', 'To_String', 'Change_Type', 'Creation_Date', 'Author_ID', 'Issue_ID']
comment_df = pd.read_csv('../../database/comment.csv')
print("comment_df loaded")
# ['ID', 'Comment', 'Comment_Text', 'Comment_Code', 'Creation_Date', 'Author_ID', 'Issue_ID']
component_df = pd.read_csv('../../database/component.csv')
print("component_df loaded")
# ['ID', 'Jira_ID', 'Name', 'Description', 'Project_ID']
fix_version_df = pd.read_csv('../../database/fix_version.csv')
print("fix_version_df loaded")
# ['Issue_ID', 'Fix_Version_ID']
issue_df = pd.read_csv('../extract_github_data/updated_issues.csv')
print("issue_df loaded")
# ['ID', 'Jira_ID', 'Issue_Key', 'URL', 'Title', 'Description', 'Description_Text', 'Description_Code', 'Type', 'Priority', 'Status', 'Resolution', 'Creation_Date', 'Estimation_Date', 'Resolution_Date', 'Last_Updated', 'Story_Point', 'Timespent', 'In_Progress_Minutes', 'Total_Effort_Minutes', 'Resolution_Time_Minutes', 'Title_Changed_After_Estimation', 'Description_Changed_After_Estimation', 'Story_Point_Changed_After_Estimation', 'Pull_Request_URL', 'Creator_ID', 'Reporter_ID', 'Assignee_ID', 'Project_ID', 'Sprint_ID', 'Owner', 'Repo', 'Pull_Number', 'PR_ID', 'PR_Title', 'PR_State', 'Is_Mergeable', 'Mergeable_State', 'Base_Branch', 'Head_Branch', 'Content', 'Diff']
issue_component_df = pd.read_csv('../../database/issue_component.csv')
print("issue_component_df loaded")
# ['Issue_ID', 'Component_ID']
issue_link_df = pd.read_csv('../../database/issue_link.csv')
print("issue_link_df loaded")
# ['ID', 'Issue_ID', 'Name', 'Description', 'Direction', 'Target_Issue_ID']
project_df = pd.read_csv('../../database/project.csv')
print("project_df loaded")
# ['ID', 'Project_Key', 'Name', 'URL', 'Description', 'Start_Date', 'Last_Update_Date', 'SP_Field_ID', 'Repository_ID']
repository_df = pd.read_csv('../../database/repository.csv')
print("repository_df loaded")
# ['ID', 'Name', 'Description', 'URL']
sprint_df = pd.read_csv('../../database/sprint.csv')
print("sprint_df loaded")
# ['ID', 'JiraID', 'Name', 'State', 'Start_Date', 'End_Date', 'Activated_Date', 'Complete_Date', 'Project_ID']
user_df = pd.read_csv('../../database/user.csv')
print("user_df loaded")
# ['ID', 'Project_ID']
version_df = pd.read_csv('../../database/version.csv')
print("version_df loaded")
# ['ID', 'Jira_ID', 'Name', 'Description', 'Archived', 'Released', 'Release_Date', 'Project_ID']

print("Dataframes loaded")

# Context 2: Project Overview with Repository Details
# Tables and Attributes Used: Project, Repository
context_2 = {
    'template': "The project '[Project_Name]' (ID [Project_ID]) with the key '[Project_Key]' is described as '[Project_Description]' and is hosted at '[Project_URL]'. The repository '[Repository_Name]' is part of this project and is described as '[Repository_Description]'."
}

# Context 2 Q&A Templates
qa_templates_context_2 = [
    {
        'question_template': "Where is the project '[Project_Name]' hosted?",
        'answer_template': "[Project_URL]",
        'answer_start_placeholder': "[Project_URL_Start_Index]"
    },
    {
        'question_template': "What is the description of the repository for project '[Project_Name]'?",
        'answer_template': "[Repository_Description]",
        'answer_start_placeholder': "[Repository_Description_Start_Index]"
    },
]

# Prepare the individual dataframes
project_df = project_df.rename(
    columns={'ID': 'Project_ID', 'Name': 'Project_Name', 'URL': 'Project_URL', 'Description': 'Project_Description'})
repository_df = repository_df.rename(
    columns={'ID': 'Repository_ID', 'Name': 'Repository_Name', 'Description': 'Repository_Description',
             'URL': 'Repository_URL'})

# Merge Project with Repository
merged_df = pd.merge(project_df, repository_df, on='Repository_ID', how='left')

# Selecting necessary columns
final_columns = ['Project_Name', 'Project_ID', 'Project_Key', 'Project_Description', 'Project_URL', 'Repository_Name',
                 'Repository_Description']
final_df = merged_df[final_columns].drop_duplicates('Project_ID')

# Save to CSV
final_df.to_csv('../data_for_each_context/project_df_context_2.csv', index=False)
print("project_df_context_2 saved to 'project_df_context_2.csv'")
