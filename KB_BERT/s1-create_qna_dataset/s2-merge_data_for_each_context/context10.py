import pandas as pd

change_log_df = pd.read_csv('../../database/change_log.csv')
print("change_log_df loaded")
# ['ID', 'Field', 'From_Value', 'To_Value', 'From_String', 'To_String', 'Change_Type', 'Creation_Date', 'Author_ID', 'Issue_ID']
issue_df = pd.read_csv('../extract_github_data/updated_issues.csv')
print("issue_df loaded")
# ['ID', 'Jira_ID', 'Issue_Key', 'URL', 'Title', 'Description', 'Description_Text', 'Description_Code', 'Type', 'Priority', 'Status', 'Resolution', 'Creation_Date', 'Estimation_Date', 'Resolution_Date', 'Last_Updated', 'Story_Point', 'Timespent', 'In_Progress_Minutes', 'Total_Effort_Minutes', 'Resolution_Time_Minutes', 'Title_Changed_After_Estimation', 'Description_Changed_After_Estimation', 'Story_Point_Changed_After_Estimation', 'Pull_Request_URL', 'Creator_ID', 'Reporter_ID', 'Assignee_ID', 'Project_ID', 'Sprint_ID', 'Owner', 'Repo', 'Pull_Number', 'PR_ID', 'PR_Title', 'PR_State', 'Is_Mergeable', 'Mergeable_State', 'Base_Branch', 'Head_Branch', 'Content', 'Diff']

print("Dataframes loaded")



# Context 10: Comprehensive Issue Lifecycle
# Tables and Attributes Used: Issue, Change_Log
context_10 = {
    'template': "Issue created on [Creation_Date] and last updated on [Last_Updated] has a resolution status of '[Resolution]'. With a story point of [Story_Point], the time spent in progress is [In_Progress_Minutes] minutes, and the total effort is [Total_Effort_Minutes] minutes. The issue has undergone changes, including '[Change_Type]' modifications, reflecting its dynamic lifecycle."
}

# Context 10 Q&A Templates
qa_templates_context_10 = [
    {
        'question_template': "How much total effort in minutes has been spent on the issue with ID [Issue_ID]?",
        'answer_template': "[Total_Effort_Minutes] minutes",
        'answer_start_placeholder': "[Total_Effort_Minutes_Start_Index]"
    },
    {
        'question_template': "What is the resolution status of the issue with ID [Issue_ID]?",
        'answer_template': "[Resolution]",
        'answer_start_placeholder': "[Resolution_Start_Index]"
    },
    {
        'question_template': "What are the story points allocated to the issue with ID [Issue_ID]?",
        'answer_template': "[Story_Point]",
        'answer_start_placeholder': "[Story_Point_Start_Index]"
    },
    {
        'question_template': "When was the issue with ID [Issue_ID] last updated?",
        'answer_template': "[Last_Updated]",
        'answer_start_placeholder': "[Last_Updated_Start_Index]"
    },
]

# Prepare the individual dataframes
issue_df = issue_df.rename(columns={'ID': 'Issue_ID', 'Creation_Date': 'Creation_Date', 'Last_Updated': 'Last_Updated', 'Resolution': 'Resolution', 'Story_Point': 'Story_Point', 'In_Progress_Minutes': 'In_Progress_Minutes', 'Total_Effort_Minutes': 'Total_Effort_Minutes'})
change_log_df = change_log_df.rename(columns={'Issue_ID': 'Issue_ID', 'Change_Type': 'Change_Type'})

print("Dataframes renamed")
# # Merge Issue with Change_Log
# merged_df = pd.merge(issue_df, change_log_df, on='Issue_ID', how='left')
#
# print("Dataframes merged")
# # Aggregate Change Types
# merged_df['Change_Types'] = merged_df.groupby('Issue_ID')['Change_Type'].transform(lambda x: ', '.join(x.dropna().unique()))
# merged_df.drop(columns=['Change_Type'], inplace=True)
#
# print("Change Types aggregated")
# # Selecting necessary columns
# final_columns = ['Issue_ID', 'Creation_Date', 'Last_Updated', 'Resolution', 'Story_Point', 'In_Progress_Minutes', 'Total_Effort_Minutes', 'Change_Types']
# final_df = merged_df[final_columns].drop_duplicates('Issue_ID')
#
# # Save to CSV
# final_df.to_csv('project_df_context_10.csv', index=False)
# print("project_df_context_10 saved to 'project_df_context_10.csv'")

# Assuming 'change_log_df' is the larger dataframe
n_chunks = 4  # Adjust based on your system's memory capabilities
total_rows = change_log_df.shape[0]
chunk_size = total_rows // n_chunks

for i in range(n_chunks):
    start = i * chunk_size
    end = total_rows if i == n_chunks - 1 else start + chunk_size
    chunk = change_log_df.iloc[start:end]
    merged_chunk = pd.merge(issue_df, chunk, on='Issue_ID', how='left')

    print(f"Chunk {i + 1} merged")
    # Aggregate Change Types
    merged_chunk['Change_Types'] = merged_chunk.groupby('Issue_ID')['Change_Type'].transform(lambda x: ', '.join(x.dropna().unique()))
    merged_chunk.drop(columns=['Change_Type'], inplace=True )

    print(f"Change Types aggregated for chunk {i + 1}")
    # Selecting necessary columns and dropping duplicates
    final_columns = ['Issue_ID', 'Creation_Date_x', 'Last_Updated', 'Resolution', 'Story_Point', 'In_Progress_Minutes', 'Total_Effort_Minutes', 'Change_Types']
    final_chunk = merged_chunk[final_columns].drop_duplicates('Issue_ID')

    # Save each chunk to a separate CSV file
    file_name = f'../data_for_each_context/project_df_context_10_part_{i + 1}.csv'
    final_chunk.to_csv(file_name, index=False)
    print(f"{file_name} saved")
