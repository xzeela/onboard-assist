import pandas as pd

affected_version_df = pd.read_csv('../../database/affected_version.csv')
print("affected_version_df loaded")
# ['Issue_ID', 'Affected_Version_ID']
change_log_df = pd.read_csv('../../database/change_log.csv')
print("change_log_df loaded")
# ['ID', 'Field', 'From_Value', 'To_Value', 'From_String', 'To_String', 'Change_Type', 'Creation_Date', 'Author_ID', 'Issue_ID']
comment_df = pd.read_csv('../../database/comment.csv')
print("comment_df loaded")
# ['ID', 'Comment', 'Comment_Text', 'Comment_Code', 'Creation_Date', 'Author_ID', 'Issue_ID']
component_df = pd.read_csv('../../database/component.csv')
print("component_df loaded")
# ['ID', 'Jira_ID', 'Name', 'Description', 'Project_ID']
fix_version_df = pd.read_csv('../../database/fix_version.csv')
print("fix_version_df loaded")
# ['Issue_ID', 'Fix_Version_ID']
issue_df = pd.read_csv('../extract_github_data/updated_issues.csv')
print("issue_df loaded")
# ['ID', 'Jira_ID', 'Issue_Key', 'URL', 'Title', 'Description', 'Description_Text', 'Description_Code', 'Type', 'Priority', 'Status', 'Resolution', 'Creation_Date', 'Estimation_Date', 'Resolution_Date', 'Last_Updated', 'Story_Point', 'Timespent', 'In_Progress_Minutes', 'Total_Effort_Minutes', 'Resolution_Time_Minutes', 'Title_Changed_After_Estimation', 'Description_Changed_After_Estimation', 'Story_Point_Changed_After_Estimation', 'Pull_Request_URL', 'Creator_ID', 'Reporter_ID', 'Assignee_ID', 'Project_ID', 'Sprint_ID', 'Owner', 'Repo', 'Pull_Number', 'PR_ID', 'PR_Title', 'PR_State', 'Is_Mergeable', 'Mergeable_State', 'Base_Branch', 'Head_Branch', 'Content', 'Diff']
issue_component_df = pd.read_csv('../../database/issue_component.csv')
print("issue_component_df loaded")
# ['Issue_ID', 'Component_ID']
issue_link_df = pd.read_csv('../../database/issue_link.csv')
print("issue_link_df loaded")
# ['ID', 'Issue_ID', 'Name', 'Description', 'Direction', 'Target_Issue_ID']
project_df = pd.read_csv('../../database/project.csv')
print("project_df loaded")
# ['ID', 'Project_Key', 'Name', 'URL', 'Description', 'Start_Date', 'Last_Update_Date', 'SP_Field_ID', 'Repository_ID']
repository_df = pd.read_csv('../../database/repository.csv')
print("repository_df loaded")
# ['ID', 'Name', 'Description', 'URL']
sprint_df = pd.read_csv('../../database/sprint.csv')
print("sprint_df loaded")
# ['ID', 'JiraID', 'Name', 'State', 'Start_Date', 'End_Date', 'Activated_Date', 'Complete_Date', 'Project_ID']
user_df = pd.read_csv('../../database/user.csv')
print("user_df loaded")
# ['ID', 'Project_ID']
version_df = pd.read_csv('../../database/version.csv')
print("version_df loaded")
# ['ID', 'Jira_ID', 'Name', 'Description', 'Archived', 'Released', 'Release_Date', 'Project_ID']

print("Dataframes loaded")



# Context 1: Detailed Issue Information in a Project
# Tables and Attributes Used: Project, Issue, Issue_Components, Component, Affected_Version, Fix_Version, Version
context_1 = {
    'template': "In project '[Project_Name]' with key '[Project_Key]', the issue titled '[Issue_Title]' (ID [Issue_ID]) has been reported. It is of type '[Type]' with priority '[Priority]' and current status '[Status]'. This issue includes components [Component_Names] and is affected in version [Affected_Version_Names] but fixed in version [Fix_Version_Names]. The detailed description states: '[Description]'."
}

# Context 1 Q&A Templates
qa_templates_context_1 = [
    {
        'question_template': "What is the status of issue ID [Issue_ID]?",
        'answer_template': "[Status]",
        'answer_start_placeholder': "[Status_Start_Index]"
    },
    {
        'question_template': "What components are involved in issue ID [Issue_ID]?",
        'answer_template': "[Component_Names]",
        'answer_start_placeholder': "[Components_Start_Index]"
    },
    {
        'question_template': "In which version was the issue titled '[Issue_Title]' fixed?",
        'answer_template': "[Fix_Version_Names]",
        'answer_start_placeholder': "[Fix_Version_Start_Index]"
    },
]

# Project, Issue, Issue_Components, Component, Affected_Version, Fix_Version, Version
# Prepare the individual dataframes
project_df = project_df.rename(columns={'ID': 'Project_ID', 'Name': 'Project_Name'})
issue_df = issue_df.rename(columns={'ID': 'Issue_ID', 'Title': 'Issue_Title', 'Type': 'Type', 'Priority': 'Priority', 'Status': 'Status', 'Description': 'Description', 'Project_ID': 'Project_ID'})
component_df = component_df.rename(columns={'ID': 'Component_ID', 'Name': 'Component_Name'})
version_df = version_df.rename(columns={'ID': 'Version_ID', 'Name': 'Version_Name'})


# read from csv
merged_df = pd.read_csv('context_1_merged_df.csv')

# Merging and Aggregating Affected Versions
merged_df = pd.merge(merged_df, affected_version_df, on='Issue_ID', how='left')
merged_df = pd.merge(merged_df, version_df[['Version_ID', 'Version_Name']], left_on='Affected_Version_ID', right_on='Version_ID', how='left')
merged_df['Affected_Version_Names'] = merged_df.groupby('Issue_ID')['Version_Name'].transform(lambda x: ', '.join(x.dropna().unique()))
merged_df.drop(columns=['Affected_Version_ID', 'Version_ID'], inplace=True)

# Merging and Aggregating Fix Versions
merged_df = pd.merge(merged_df, fix_version_df, on='Issue_ID', how='left')
merged_df = pd.merge(merged_df, version_df[['Version_ID', 'Version_Name']], left_on='Fix_Version_ID', right_on='Version_ID', how='left')

# save to csv
merged_df.to_csv('context_1_merged_df_2.csv', index=False)

# read
merged_df = pd.read_csv('context_1_merged_df_2.csv')

merged_df['Fix_Version_Names'] = merged_df.groupby('Issue_ID')['Version_Name_y'].transform(lambda x: ', '.join(x.dropna().unique()))
merged_df.drop(columns=['Fix_Version_ID', 'Version_ID'], inplace=True)
print(merged_df)

# Dropping duplicates and selecting necessary columns
final_columns = ['Project_Name', 'Project_Key', 'Issue_Title', 'Issue_ID', 'Type', 'Priority', 'Status', 'Description', 'Component_Names', 'Affected_Version_Names', 'Fix_Version_Names']
final_df = merged_df[final_columns].drop_duplicates('Issue_ID')

# Save to CSV
final_df.to_csv('../data_for_each_context/project_df_context_1.csv', index=False)
print("Context 1 data saved to 'project_df_context_1.csv'")
