import pandas as pd

affected_version_df = pd.read_csv('../../database/affected_version.csv')
print("affected_version_df loaded")
# ['Issue_ID', 'Affected_Version_ID']
change_log_df = pd.read_csv('../../database/change_log.csv')
print("change_log_df loaded")
# ['ID', 'Field', 'From_Value', 'To_Value', 'From_String', 'To_String', 'Change_Type', 'Creation_Date', 'Author_ID', 'Issue_ID']
comment_df = pd.read_csv('../../database/comment.csv')
print("comment_df loaded")
# ['ID', 'Comment', 'Comment_Text', 'Comment_Code', 'Creation_Date', 'Author_ID', 'Issue_ID']
component_df = pd.read_csv('../../database/component.csv')
print("component_df loaded")
# ['ID', 'Jira_ID', 'Name', 'Description', 'Project_ID']
fix_version_df = pd.read_csv('../../database/fix_version.csv')
print("fix_version_df loaded")
# ['Issue_ID', 'Fix_Version_ID']
issue_df = pd.read_csv('../extract_github_data/updated_issues.csv')
print("issue_df loaded")
# ['ID', 'Jira_ID', 'Issue_Key', 'URL', 'Title', 'Description', 'Description_Text', 'Description_Code', 'Type', 'Priority', 'Status', 'Resolution', 'Creation_Date', 'Estimation_Date', 'Resolution_Date', 'Last_Updated', 'Story_Point', 'Timespent', 'In_Progress_Minutes', 'Total_Effort_Minutes', 'Resolution_Time_Minutes', 'Title_Changed_After_Estimation', 'Description_Changed_After_Estimation', 'Story_Point_Changed_After_Estimation', 'Pull_Request_URL', 'Creator_ID', 'Reporter_ID', 'Assignee_ID', 'Project_ID', 'Sprint_ID', 'Owner', 'Repo', 'Pull_Number', 'PR_ID', 'PR_Title', 'PR_State', 'Is_Mergeable', 'Mergeable_State', 'Base_Branch', 'Head_Branch', 'Content', 'Diff']
issue_component_df = pd.read_csv('../../database/issue_component.csv')
print("issue_component_df loaded")
# ['Issue_ID', 'Component_ID']
issue_link_df = pd.read_csv('../../database/issue_link.csv')
print("issue_link_df loaded")
# ['ID', 'Issue_ID', 'Name', 'Description', 'Direction', 'Target_Issue_ID']
project_df = pd.read_csv('../../database/project.csv')
print("project_df loaded")
# ['ID', 'Project_Key', 'Name', 'URL', 'Description', 'Start_Date', 'Last_Update_Date', 'SP_Field_ID', 'Repository_ID']
repository_df = pd.read_csv('../../database/repository.csv')
print("repository_df loaded")
# ['ID', 'Name', 'Description', 'URL']
sprint_df = pd.read_csv('../../database/sprint.csv')
print("sprint_df loaded")
# ['ID', 'JiraID', 'Name', 'State', 'Start_Date', 'End_Date', 'Activated_Date', 'Complete_Date', 'Project_ID']
user_df = pd.read_csv('../../database/user.csv')
print("user_df loaded")
# ['ID', 'Project_ID']
version_df = pd.read_csv('../../database/version.csv')
print("version_df loaded")
# ['ID', 'Jira_ID', 'Name', 'Description', 'Archived', 'Released', 'Release_Date', 'Project_ID']

print("Dataframes loaded")



# Context 9: Issue Reporting and Handling Details
# Tables and Attributes Used: Issue, User
context_9 = {
    'template': "Issue reported by user ID [Reporter_ID] has been assigned to user ID [Assignee_ID]. The initial estimation date was set to [Estimation_Date], with a resolution expected by [Resolution_Date]. The handling of this issue reflects the responsiveness and planning of the project team."
}

# Context 9 Q&A Templates
qa_templates_context_9 = [
    {
        'question_template': "Who reported the issue assigned to user ID [Assignee_ID]?",
        'answer_template': "[Reporter_ID]",
        'answer_start_placeholder': "[Reporter_ID_Start_Index]"
    },
    {
        'question_template': "Who is assigned to handle the issue reported by user ID [Reporter_ID]?",
        'answer_template': "[Assignee_ID]",
        'answer_start_placeholder': "[Assignee_ID_Start_Index]"
    },
    {
        'question_template': "When is the estimated resolution date for the issue handled by user ID [Assignee_ID]?",
        'answer_template': "[Resolution_Date]",
        'answer_start_placeholder': "[Resolution_Date_Start_Index]"
    },
]

# Prepare the individual dataframes
issue_df = issue_df.rename(columns={'ID': 'Issue_ID', 'Reporter_ID': 'Reporter_ID', 'Assignee_ID': 'Assignee_ID', 'Estimation_Date': 'Estimation_Date', 'Resolution_Date': 'Resolution_Date'})
user_df = user_df.rename(columns={'ID': 'User_ID'})

# Merge Issue with User for Reporter
merged_df = pd.merge(issue_df, user_df, left_on='Reporter_ID', right_on='User_ID', how='left')
merged_df = merged_df.rename(columns={'User_ID': 'Reporter_User_ID'})

# Merge Issue with User for Assignee
merged_df = pd.merge(merged_df, user_df, left_on='Assignee_ID', right_on='User_ID', how='left')
merged_df = merged_df.rename(columns={'User_ID': 'Assignee_User_ID'})

# Dropping unnecessary user columns
merged_df.drop(columns=['Reporter_User_ID', 'Assignee_User_ID'], inplace=True)

# Selecting necessary columns
final_columns = ['Issue_ID', 'Reporter_ID', 'Assignee_ID', 'Estimation_Date', 'Resolution_Date']
final_df = merged_df[final_columns].drop_duplicates('Issue_ID')

# Save to CSV
final_df.to_csv('../data_for_each_context/project_df_context_9.csv', index=False)
print("project_df_context_9 saved to 'project_df_context_9.csv'")
