import pandas as pd

project_df = pd.read_csv('../../database/project.csv')
print("project_df loaded")
# ['ID', 'Project_Key', 'Name', 'URL', 'Description', 'Start_Date', 'Last_Update_Date', 'SP_Field_ID', 'Repository_ID']
sprint_df = pd.read_csv('../../database/sprint.csv')
print("sprint_df loaded")
# ['ID', 'JiraID', 'Name', 'State', 'Start_Date', 'End_Date', 'Activated_Date', 'Complete_Date', 'Project_ID']
user_df = pd.read_csv('../../database/user.csv')
print("user_df loaded")
# ['ID', 'Project_ID']

print("Dataframes loaded")

# Context 6: User's Project Role and Sprint Participation
# Tables and Attributes Used: User, Project, Sprint
context_6 = {
    'template': "User ID [User_ID] is assigned to the project '[Project_Name]' with the project key '[Project_Key]'. They have participated in the sprint '[Sprint_Name]', which is currently in the '[State]' state."
}

# Context 6 Q&A Templates
qa_templates_context_6 = [
    {
        'question_template': "Which sprint has User ID [User_ID] participated in for the project '[Project_Name]'?",
        'answer_template': "[Sprint_Name]",
        'answer_start_placeholder': "[Sprint_Name_Start_Index]"
    },
    {
        'question_template': "What is the state of the sprint '[Sprint_Name]' in which User ID [User_ID] is participating?",
        'answer_template': "[State]",
        'answer_start_placeholder': "[State_Start_Index]"
    },
]

# Prepare the individual dataframes
user_df = user_df.rename(columns={'ID': 'User_ID', 'Project_ID': 'Project_ID'})
project_df = project_df.rename(columns={'ID': 'Project_ID', 'Name': 'Project_Name', 'Project_Key': 'Project_Key'})
sprint_df = sprint_df.rename(
    columns={'ID': 'Sprint_ID', 'Name': 'Sprint_Name', 'State': 'State', 'Project_ID': 'Project_ID'})

print("Dataframes renamed")
# Merge User with Project
merged_df = pd.merge(user_df, project_df, on='Project_ID', how='left')
# # keep only the necessary columns
# merged_df = merged_df[['User_ID', 'Project_Name', 'Project_Key', 'Project_ID']].drop_duplicates(['User_ID', 'Project_ID'])

print("User merged with Project")

# save merged_df
merged_df.to_csv('merged_df_6.csv', index=False)

# Determine the number of rows for each chunk
n_chunks = 3  # Number of chunks to divide 'merged_df' into
total_rows = merged_df.shape[0]
chunk_size = total_rows // n_chunks

# Iterate through 'merged_df' in chunks, merge with 'sprint_df', and save to CSV
for i in range(n_chunks):
    start = i * chunk_size
    end = total_rows if i == n_chunks - 1 else start + chunk_size
    chunk = merged_df.iloc[start:end]
    merged_chunk = pd.merge(chunk, sprint_df, on='Project_ID', how='left')

    # Selecting necessary columns and dropping duplicates
    final_columns = ['User_ID', 'Project_Name', 'Project_Key', 'Sprint_Name', 'State']
    final_chunk = merged_chunk[final_columns]

    file_name = f'../data_for_each_context/project_df_context_6_part_{i + 1}.csv'
    final_chunk.to_csv(file_name, index=False)
    print(f"{file_name} saved")
