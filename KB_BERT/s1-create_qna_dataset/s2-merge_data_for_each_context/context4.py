import pandas as pd

affected_version_df = pd.read_csv('../../database/affected_version.csv')
print("affected_version_df loaded")
# ['Issue_ID', 'Affected_Version_ID']
change_log_df = pd.read_csv('../../database/change_log.csv')
print("change_log_df loaded")
# ['ID', 'Field', 'From_Value', 'To_Value', 'From_String', 'To_String', 'Change_Type', 'Creation_Date', 'Author_ID', 'Issue_ID']
comment_df = pd.read_csv('../../database/comment.csv')
print("comment_df loaded")
# ['ID', 'Comment', 'Comment_Text', 'Comment_Code', 'Creation_Date', 'Author_ID', 'Issue_ID']
component_df = pd.read_csv('../../database/component.csv')
print("component_df loaded")
# ['ID', 'Jira_ID', 'Name', 'Description', 'Project_ID']
fix_version_df = pd.read_csv('../../database/fix_version.csv')
print("fix_version_df loaded")
# ['Issue_ID', 'Fix_Version_ID']
issue_df = pd.read_csv('../extract_github_data/updated_issues.csv')
print("issue_df loaded")
# ['ID', 'Jira_ID', 'Issue_Key', 'URL', 'Title', 'Description', 'Description_Text', 'Description_Code', 'Type', 'Priority', 'Status', 'Resolution', 'Creation_Date', 'Estimation_Date', 'Resolution_Date', 'Last_Updated', 'Story_Point', 'Timespent', 'In_Progress_Minutes', 'Total_Effort_Minutes', 'Resolution_Time_Minutes', 'Title_Changed_After_Estimation', 'Description_Changed_After_Estimation', 'Story_Point_Changed_After_Estimation', 'Pull_Request_URL', 'Creator_ID', 'Reporter_ID', 'Assignee_ID', 'Project_ID', 'Sprint_ID', 'Owner', 'Repo', 'Pull_Number', 'PR_ID', 'PR_Title', 'PR_State', 'Is_Mergeable', 'Mergeable_State', 'Base_Branch', 'Head_Branch', 'Content', 'Diff']
issue_component_df = pd.read_csv('../../database/issue_component.csv')
print("issue_component_df loaded")
# ['Issue_ID', 'Component_ID']
issue_link_df = pd.read_csv('../../database/issue_link.csv')
print("issue_link_df loaded")
# ['ID', 'Issue_ID', 'Name', 'Description', 'Direction', 'Target_Issue_ID']
project_df = pd.read_csv('../../database/project.csv')
print("project_df loaded")
# ['ID', 'Project_Key', 'Name', 'URL', 'Description', 'Start_Date', 'Last_Update_Date', 'SP_Field_ID', 'Repository_ID']
repository_df = pd.read_csv('../../database/repository.csv')
print("repository_df loaded")
# ['ID', 'Name', 'Description', 'URL']
sprint_df = pd.read_csv('../../database/sprint.csv')
print("sprint_df loaded")
# ['ID', 'JiraID', 'Name', 'State', 'Start_Date', 'End_Date', 'Activated_Date', 'Complete_Date', 'Project_ID']
user_df = pd.read_csv('../../database/user.csv')
print("user_df loaded")
# ['ID', 'Project_ID']
version_df = pd.read_csv('../../database/version.csv')
print("version_df loaded")
# ['ID', 'Jira_ID', 'Name', 'Description', 'Archived', 'Released', 'Release_Date', 'Project_ID']

print("Dataframes loaded")



# Context 4: User Contributions Across Projects
# Tables and Attributes Used: User, Issue, Project
context_4 = {
    'template': "User ID [User_ID] is known for their contributions to the project '[Project_Name]' (ID [Project_ID]). They have worked on issues including [Issue_Titles]. This involvement showcases the user's active role in the development and progression of the project."
}

# Context 4 Q&A Templates
qa_templates_context_4 = [
    {
        'question_template': "Which issues has User ID [User_ID] worked on in the project '[Project_Name]'?",
        'answer_template': "[Issue_Titles]",
        'answer_start_placeholder': "[Issue_Titles_Start_Index]"
    },
    {
        'question_template': "What has been the role of User ID [User_ID] in the development of project '[Project_Name]'?",
        'answer_template': "known for their contributions",
        'answer_start_placeholder': "known for their contributions_Start_Index"
    },
]

# Prepare the individual dataframes
user_df = user_df.rename(columns={'ID': 'User_ID'})
issue_df = issue_df.rename(columns={'ID': 'Issue_ID', 'Title': 'Issue_Title', 'Creator_ID': 'User_ID', 'Project_ID': 'Project_ID'})
project_df = project_df.rename(columns={'ID': 'Project_ID', 'Name': 'Project_Name'})

# Merge Issue with User
merged_df = pd.merge(issue_df, user_df, on='User_ID', how='left')
# rename Project_ID to Project_ID_x
merged_df = merged_df.rename(columns={'Project_ID_x': 'Project_ID'})
print(merged_df.columns)

# Merge with Project
merged_df = pd.merge(merged_df, project_df, on='Project_ID', how='left')

# Aggregate Issue Titles
merged_df['Issue_Titles'] = merged_df.groupby(['User_ID', 'Project_ID'])['Issue_Title'].transform(lambda x: ', '.join(x.dropna().unique()))
merged_df.drop(columns=['Issue_Title', 'Issue_ID'], inplace=True)

# Dropping duplicates and selecting necessary columns
final_columns = ['User_ID', 'Project_Name', 'Project_ID', 'Issue_Titles']
final_df = merged_df[final_columns].drop_duplicates(['User_ID', 'Project_ID'])

# Save to CSV
final_df.to_csv('../data_for_each_context/project_df_context_4.csv', index=False)
print("project_df_context_4 saved to 'project_df_context_4.csv'")



