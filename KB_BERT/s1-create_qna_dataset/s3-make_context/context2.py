import pandas as pd
from tqdm import tqdm

# Load the CSV file
df = pd.read_csv('../data_for_each_context/project_df_context_2.csv')

# Prepare lists to hold the results for both dataframes
contexts = []
qa_pairs = []

# A variable to keep track of unique context IDs
context_id = 0

# Iterate over each row to populate the templates
for index, row in tqdm(df.iterrows(), total=df.shape[0]):
    # Increment the context ID for each row
    context_id += 1

    # Convert fields to string and handle NaN values
    project_name = str(row['Project_Name'])
    project_id = str(row['Project_ID'])
    project_key = str(row['Project_Key'])
    project_description = str(row['Project_Description']) if pd.notna(row['Project_Description']) else "Not specified"
    project_url = str(row['Project_URL']) if pd.notna(row['Project_URL']) else "Not specified"
    repository_name = str(row['Repository_Name']) if pd.notna(row['Repository_Name']) else "Not specified"
    repository_description = str(row['Repository_Description']) if pd.notna(row['Repository_Description']) else "Not specified"

    # Construct the context
    context = f"The project '{project_name}' (ID {project_id}) with the key '{project_key}' is described as '{project_description}' and is hosted at '{project_url}'. The repository '{repository_name}' is part of this project and is described as '{repository_description}'."

    # Add context and its ID to the contexts list
    contexts.append({'Context_ID': context_id, 'Context': context})

    # Questions and Answers preparation
    questions_answers = [
        (f"Where is the project '{project_name}' hosted?", project_url),
        (f"What is the description of the repository for project '{project_name}'?", repository_description),
    ]

    # Add question-answer pairs with context ID and answer start indices
    for q, a in questions_answers:
        a_start = context.find(a)
        qa_pairs.append({
            'Context_ID': context_id,
            'Question': q,
            'Answer': a,
            'Answer_Start_Index': a_start
        })

# Convert lists to DataFrames
contexts_df = pd.DataFrame(contexts)
qa_df = pd.DataFrame(qa_pairs)

# Save to CSV files
contexts_df.to_csv('../cqa_dataset/2_context.csv', index=False)
qa_df.to_csv('../cqa_dataset/2_question_answer.csv', index=False)

print("2_context.csv and 2_question_answer.csv saved")
