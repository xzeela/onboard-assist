import os
import pandas as pd
import gc
from tqdm import tqdm

def process_row(row, context_id):
    issue_title = str(row['Issue_Title'])
    issue_id = str(row['Issue_ID'])
    change_id = str(row['Change_ID'])
    field = str(row['Field'])
    from_value = str(row['From_Value']) if pd.notna(row['From_Value']) else "Not specified"
    to_value = str(row['To_Value']) if pd.notna(row['To_Value']) else "Not specified"
    author_id = str(row['Author_ID'])
    creation_date = str(row['Creation_Date_x']) if pd.notna(row['Creation_Date_x']) else "Not specified"

    context = f"For issue '{issue_title}' (ID {issue_id}), the change history records an entry (ID {change_id}) indicating that the '{field}' was updated from '{from_value}' to '{to_value}' by author ID {author_id} on {creation_date}. This modification is part of the issue's evolution through the resolution process."

    qa_pairs = [
        {
            'Context_ID': context_id,
            'Question': f"Which field was updated in the change history of issue ID {issue_id}?",
            'Answer': field,
            'Answer_Start_Index': context.find(field)
        },
        {
            'Context_ID': context_id,
            'Question': f"After the update to issue ID {issue_id}, what is the new value of '{field}'?",
            'Answer': to_value,
            'Answer_Start_Index': context.find(to_value)
        },
        {
            'Context_ID': context_id,
            'Question': f"When was the change made to the field '{field}' of issue ID {issue_id}?",
            'Answer': creation_date,
            'Answer_Start_Index': context.find(creation_date)
        }
    ]

    return {'Context_ID': context_id, 'Context': context}, qa_pairs

def process_chunk(df, start_context_id):
    contexts = []
    qa_pairs = []

    for index, row in df.iterrows():
        context_id = start_context_id + index
        context, row_qa_pairs = process_row(row, context_id)
        contexts.append(context)
        qa_pairs.extend(row_qa_pairs)

    return contexts, qa_pairs

n_chunks = 3  # Adjust as per your dataset
chunksize = 100000  # Adjust based on memory constraints

context_id_counter = 0

for i in range(n_chunks):
    file_name = f'../data_for_each_context/project_df_context_5_part_{i + 1}.csv'
    context_file = f'../cqa_dataset/5_context_part_{i + 1}.csv'
    qa_file = f'../cqa_dataset/5_question_answer_part_{i + 1}.csv'

    try:
        chunk_iter = pd.read_csv(file_name, chunksize=chunksize)

        for df in tqdm(chunk_iter, desc=f"Processing File {i + 1}"):
            contexts, qa_pairs = process_chunk(df, context_id_counter)

            # Update context_id_counter for the next chunk
            context_id_counter += len(df)

            # Determine whether to include headers based on file existence
            context_header = not os.path.exists(context_file)
            qa_header = not os.path.exists(qa_file)

            # Save contexts to CSV
            contexts_df = pd.DataFrame(contexts)
            contexts_df.to_csv(context_file, mode='a', index=False, header=context_header)

            # Save QA pairs to CSV
            qa_df = pd.DataFrame(qa_pairs)
            qa_df.to_csv(qa_file, mode='a', index=False, header=qa_header)

            # Clear memory
            del contexts, qa_pairs, contexts_df, qa_df
            gc.collect()

    except Exception as e:
        print(f"An error occurred while processing {file_name}: {e}")

print("Script completed.")
