import pandas as pd
from tqdm import tqdm

n_chunks = 4  # Adjust based on the number of files

# Initialize context ID counter
context_id_counter = 0

for i in range(n_chunks):
    file_name = f'../data_for_each_context/project_df_context_10_part_{i + 1}.csv'
    df = pd.read_csv(file_name)

    # Lists to hold the contexts and question-answer pairs
    contexts = []
    qa_pairs = []

    # Iterate over each row and populate the templates
    for index, row in tqdm(df.iterrows(), total=df.shape[0]):
        context_id_counter += 1  # Increment context ID for each new row

        # Convert all fields to string and handle NaN values
        creation_date = str(row['Creation_Date_x'])
        last_updated = str(row['Last_Updated'])
        resolution = str(row['Resolution']) if pd.notna(row['Resolution']) else "Not specified"
        story_point = str(row['Story_Point'])
        in_progress_minutes = str(row['In_Progress_Minutes'])
        total_effort_minutes = str(row['Total_Effort_Minutes'])
        change_types = str(row['Change_Types'])

        # Construct the context
        context = f"Issue created on {creation_date} and last updated on {last_updated} has a resolution status of '{resolution}'. With a story point of {story_point}, the time spent in progress is {in_progress_minutes} minutes, and the total effort is {total_effort_minutes} minutes. The issue has undergone changes, including '{change_types}' modifications, reflecting its dynamic lifecycle."
        contexts.append({'Context_ID': context_id_counter, 'Context': context})

        # Construct the question-answer pairs
        questions = [
            f"How much total effort in minutes has been spent on the issue with ID {row['Issue_ID']}?",
            f"What is the resolution status of the issue with ID {row['Issue_ID']}?",
            f"What are the story points allocated to the issue with ID {row['Issue_ID']}?",
            f"When was the issue with ID {row['Issue_ID']} last updated?"
        ]
        answers = [f"{total_effort_minutes} minutes", resolution, story_point, last_updated]
        answer_starts = [context.find(a) for a in answers]

        for q, a, a_start in zip(questions, answers, answer_starts):
            qa_pairs.append({
                'Context_ID': context_id_counter,
                'Question': q,
                'Answer': a,
                'Answer_Start_Index': a_start
            })

    # Convert lists to DataFrames
    contexts_df = pd.DataFrame(contexts)
    qa_df = pd.DataFrame(qa_pairs)

    # Save the contexts and QA pairs to separate CSV files
    contexts_df.to_csv(f'../cqa_dataset/10_context_part_{i + 1}.csv', index=False)
    qa_df.to_csv(f'../cqa_dataset/10_question_answer_part_{i + 1}.csv', index=False)

    print(f"Part {i + 1} saved for contexts and question-answer pairs.")

# Indicate script completion
print("Script completed.")
