import pandas as pd
from tqdm import tqdm

# Load the CSV file
df = pd.read_csv('../data_for_each_context/project_df_context_9.csv')

# Lists to hold the contexts and question-answer pairs
contexts = []
qa_pairs = []

# A variable to keep track of unique context IDs
context_id = 0

# Iterate over each row and populate the templates
for index, row in tqdm(df.iterrows(), total=df.shape[0]):
    # Increment the context ID for each row
    context_id += 1

    # Convert all fields to string and handle NaN values
    reporter_id = str(row['Reporter_ID']) if pd.notna(row['Reporter_ID']) else "Not specified"
    assignee_id = str(row['Assignee_ID']) if pd.notna(row['Assignee_ID']) else "Not specified"
    estimation_date = str(row['Estimation_Date']) if pd.notna(row['Estimation_Date']) else "Not specified"
    resolution_date = str(row['Resolution_Date']) if pd.notna(row['Resolution_Date']) else "Not specified"

    # Construct the context
    context = f"Issue reported by user ID {reporter_id} has been assigned to user ID {assignee_id}. The initial estimation date was set to {estimation_date}, with a resolution expected by {resolution_date}. The handling of this issue reflects the responsiveness and planning of the project team."
    contexts.append({'Context_ID': context_id, 'Context': context})

    # Questions and Answers preparation
    questions_answers = [
        (f"Who reported the issue assigned to user ID {assignee_id}?", reporter_id),
        (f"Who is assigned to handle the issue reported by user ID {reporter_id}?", assignee_id),
        (f"When is the estimated resolution date for the issue handled by user ID {assignee_id}?", resolution_date),
    ]

    # Add question-answer pairs with context ID and answer start indices
    for q, a in questions_answers:
        a_start = context.find(a)
        qa_pairs.append({
            'Context_ID': context_id,
            'Question': q,
            'Answer': a,
            'Answer_Start_Index': a_start
        })

# Convert lists to DataFrames
contexts_df = pd.DataFrame(contexts)
qa_df = pd.DataFrame(qa_pairs)

# Save to CSV files
contexts_df.to_csv('../cqa_dataset/9_context.csv', index=False)
qa_df.to_csv('../cqa_dataset/9_question_answer.csv', index=False)

print("9_context.csv and 9_question_answer.csv saved")
