import pandas as pd
from tqdm import tqdm

# Load the CSV file
df = pd.read_csv('../data_for_each_context/project_df_context_1.csv')

# Prepare lists to hold the results for both dataframes
contexts = []
qa_pairs = []

# A variable to keep track of unique context IDs
context_id = 0

# Iterate over each row to populate the templates
for index, row in tqdm(df.iterrows(), total=df.shape[0]):
    # Increment the context ID for each row
    context_id += 1

    # Extract and handle values, managing NaN values as before
    project_name = str(row['Project_Name'])
    project_key = str(row['Project_Key'])
    issue_title = str(row['Issue_Title'])
    issue_id = str(row['Issue_ID'])
    type_ = str(row['Type']) if pd.notna(row['Type']) else "Not specified"
    priority = str(row['Priority']) if pd.notna(row['Priority']) else "Not specified"
    status = str(row['Status']) if pd.notna(row['Status']) else "Not specified"
    description = str(row['Description']) if pd.notna(row['Description']) else "Not specified"
    component_names = str(row['Component_Names']) if pd.notna(row['Component_Names']) else "Not specified"
    affected_version_names = str(row['Affected_Version_Names']) if pd.notna(
        row['Affected_Version_Names']) else "Not specified"
    fix_version_names = str(row['Fix_Version_Names']) if pd.notna(row['Fix_Version_Names']) else "Not specified"

    # Construct the context
    context = f"In project '{project_name}' with key '{project_key}', the issue titled '{issue_title}' (ID {issue_id}) has been reported. It is of type '{type_}' with priority '{priority}' and current status '{status}'. This issue includes components {component_names} and is affected in version {affected_version_names} but fixed in version {fix_version_names}. The detailed description states: '{description}'."

    # Add context and its ID to the contexts list
    contexts.append({'Context_ID': context_id, 'Context': context})

    # Questions and Answers preparation
    questions_answers = [
        (f"What is the status of issue ID {issue_id}?", status),
        (f"What components are involved in issue ID {issue_id}?", component_names),
        (f"In which version was the issue titled '{issue_title}' fixed?", fix_version_names),
    ]

    # Add question-answer pairs with context ID and answer start indices
    for q, a in questions_answers:
        a_start = context.find(a)
        qa_pairs.append({
            'Context_ID': context_id,
            'Question': q,
            'Answer': a,
            'Answer_Start_Index': a_start
        })

# Convert lists to DataFrames
contexts_df = pd.DataFrame(contexts)
qa_df = pd.DataFrame(qa_pairs)

# Save to CSV files
contexts_df.to_csv('../cqa_dataset/1_context.csv', index=False)
qa_df.to_csv('../cqa_dataset/1_question_answer.csv', index=False)

print("1_context.csv and 1_question_answer.csv saved")
