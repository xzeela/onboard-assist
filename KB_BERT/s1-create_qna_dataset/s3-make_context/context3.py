import pandas as pd
from tqdm import tqdm

# Load the CSV file
df = pd.read_csv('../data_for_each_context/project_df_context_3.csv')

# Prepare lists to hold the results for both dataframes
contexts = []
qa_pairs = []

# A variable to keep track of unique context IDs
context_id = 0

# Iterate over each row and populate the templates
for index, row in tqdm(df.iterrows(), total=df.shape[0]):
    # Increment the context ID for each row
    context_id += 1

    # Convert fields to string and handle NaN values
    sprint_name = str(row['Sprint_Name'])
    sprint_id = str(row['Sprint_ID'])
    project_name = str(row['Project_Name'])
    state = str(row['State']) if pd.notna(row['State']) else "Not specified"
    start_date = str(row['Start_Date_x']) if pd.notna(row['Start_Date_x']) else "Not specified"
    end_date = str(row['End_Date']) if pd.notna(row['End_Date']) else "Not specified"
    complete_date = str(row['Complete_Date']) if pd.notna(row['Complete_Date']) else "Not specified"
    issue_titles = str(row['Issue_Titles']) if pd.notna(row['Issue_Titles']) else "Not specified"

    # Construct the context
    context = f"In the sprint '{sprint_name}' (ID {sprint_id}) for project '{project_name}', which is currently '{state}', the following issues are addressed: {issue_titles}. The sprint started on {start_date}, is planned to end on {end_date}, and is scheduled for completion on {complete_date}."

    # Add context and its ID to the contexts list
    contexts.append({'Context_ID': context_id, 'Context': context})

    # Questions and Answers preparation
    questions_answers = [
        (f"What is the current state of the sprint '{sprint_name}'?", state),
        (f"When is the sprint '{sprint_name}' scheduled to be completed?", complete_date),
        (f"Which issues are being addressed in the sprint '{sprint_name}'?", issue_titles),
    ]

    # Add question-answer pairs with context ID and answer start indices
    for q, a in questions_answers:
        a_start = context.find(a)
        qa_pairs.append({
            'Context_ID': context_id,
            'Question': q,
            'Answer': a,
            'Answer_Start_Index': a_start
        })

# Convert lists to DataFrames
contexts_df = pd.DataFrame(contexts)
qa_df = pd.DataFrame(qa_pairs)

# Save to CSV files
contexts_df.to_csv('../cqa_dataset/3_context.csv', index=False)
qa_df.to_csv('../cqa_dataset/3_question_answer.csv', index=False)

print("3_context.csv and 3_question_answer.csv saved")
