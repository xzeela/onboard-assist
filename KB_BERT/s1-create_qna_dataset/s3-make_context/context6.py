import os

import pandas as pd
import gc
from tqdm import tqdm


def process_row(row, context_id):
    # Process each row to create context and QA pair, using context_id to link them
    user_id = str(row['User_ID'])
    project_name = str(row['Project_Name'])
    project_key = str(row['Project_Key'])
    sprint_name = str(row['Sprint_Name']) if pd.notna(row['Sprint_Name']) else "Not specified"
    state = str(row['State']) if pd.notna(row['State']) else "Not specified"

    context = f"User ID {user_id} is assigned to the project '{project_name}' with the project key '{project_key}'. They have participated in the sprint '{sprint_name}', which is currently in the '{state}' state."

    # Questions and Answers
    qa_pairs = [
        {
            'Context_ID': context_id,
            'Question': f"Which sprint has User ID {user_id} participated in for the project '{project_name}'?",
            'Answer': sprint_name,
            'Answer_Start_Index': context.find(sprint_name)
        },
        {
            'Context_ID': context_id,
            'Question': f"What is the state of the sprint '{sprint_name}' in which User ID {user_id} is participating?",
            'Answer': state,
            'Answer_Start_Index': context.find(state)
        }
    ]

    return {'Context_ID': context_id, 'Context': context}, qa_pairs


def process_chunk(df, start_context_id):
    contexts = []
    qa_pairs = []

    for index, row in df.iterrows():
        context_id = start_context_id + index
        context, row_qa_pairs = process_row(row, context_id)
        contexts.append(context)
        qa_pairs.extend(row_qa_pairs)

    return contexts, qa_pairs


n_chunks = 3  # Adjust based on the number of files you have
chunksize = 10000  # Adjust based on your memory constraints
context_id_counter = 0  # Initialize context ID counter

for i in range(n_chunks):
    file_name = f'../data_for_each_context/project_df_context_6_part_{i + 1}.csv'
    context_file = f'../cqa_dataset/6_context_part_{i + 1}.csv'
    qa_file = f'../cqa_dataset/6_question_answer_part_{i + 1}.csv'

    try:
        chunk_iter = pd.read_csv(file_name, chunksize=chunksize)

        for df in tqdm(chunk_iter, desc=f"Processing File {i + 1}"):
            contexts, qa_pairs = process_chunk(df, context_id_counter)

            # Update the context ID counter for the next chunk
            context_id_counter += len(df)

            # Determine whether to include headers based on file existence
            context_header = not os.path.exists(context_file)
            qa_header = not os.path.exists(qa_file)

            # Save contexts to CSV
            contexts_df = pd.DataFrame(contexts)
            contexts_df.to_csv(context_file, mode='a', index=False, header=context_header)

            # Save QA pairs to CSV
            qa_df = pd.DataFrame(qa_pairs)
            qa_df.to_csv(qa_file, mode='a', index=False, header=qa_header)

            # Clear memory
            del contexts, qa_pairs, contexts_df, qa_df
            gc.collect()

    except Exception as e:
        print(f"An error occurred while processing {file_name}: {e}")

print("Script completed.")