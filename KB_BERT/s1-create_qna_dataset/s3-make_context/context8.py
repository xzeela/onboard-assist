import pandas as pd
from tqdm import tqdm

# Load the CSV file
df = pd.read_csv('../data_for_each_context/project_df_context_8.csv')

# Lists to hold the contexts and question-answer pairs
contexts = []
qa_pairs = []

# A variable to keep track of unique context IDs
context_id = 0

# Iterate over each row and populate the templates
for index, row in tqdm(df.iterrows(), total=df.shape[0]):
    # Increment the context ID for each row
    context_id += 1

    # Convert all fields to string and handle NaN values
    repository_id = str(row['Repository_ID'])
    url = str(row['URL'])
    project_name = str(row['Project_Name'])
    project_description = str(row['Project_Description']) if pd.notna(row['Project_Description']) else "Not specified"
    last_update_date = str(row['Last_Update_Date']) if pd.notna(row['Last_Update_Date']) else "Not specified"

    # Construct the context
    context = f"The repository ID {repository_id}, accessible at '{url}', is associated with the project '{project_name}', which aims to '{project_description}'. The project was last updated on {last_update_date}."
    contexts.append({'Context_ID': context_id, 'Context': context})

    # Questions and Answers preparation
    q1 = f"What is the aim of the project '{project_name}' associated with repository ID {repository_id}?"
    a1 = project_description
    a1_start = context.find(a1)

    # Add question-answer pair with context ID and answer start index
    qa_pairs.append({
        'Context_ID': context_id,
        'Question': q1,
        'Answer': a1,
        'Answer_Start_Index': a1_start
    })

# Convert lists to DataFrames
contexts_df = pd.DataFrame(contexts)
qa_df = pd.DataFrame(qa_pairs)

# Save to CSV files
contexts_df.to_csv('../cqa_dataset/8_context.csv', index=False)
qa_df.to_csv('../cqa_dataset/8_question_answer.csv', index=False)

print("8_context.csv and 8_question_answer.csv saved")
