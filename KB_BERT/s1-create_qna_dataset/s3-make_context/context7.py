import pandas as pd
from tqdm import tqdm

# Load the CSV file
df = pd.read_csv('../data_for_each_context/project_df_context_7.csv')

# Lists to hold the contexts and question-answer pairs
contexts = []
qa_pairs = []

# A variable to keep track of unique context IDs
context_id = 0

# Iterate over each row and populate the templates
for index, row in tqdm(df.iterrows(), total=df.shape[0]):
    # Increment the context ID for each row
    context_id += 1

    # Convert fields to string and handle NaN values
    version_name = str(row['Version_Name'])
    project_name = str(row['Project_Name'])
    release_date = str(row['Release_Date']) if pd.notna(row['Release_Date']) else "Not specified"
    archived_status = str(row['Archived_Status'])
    version_description = str(row['Version_Description']) if pd.notna(row['Version_Description']) else "Not specified"
    release_status = str(row['Release_Status'])

    # Construct the context
    context = f"The version '{version_name}' associated with the project '{project_name}' was released on {release_date}. It is currently {archived_status} and is described as '{version_description}'. The release status is '{release_status}'."
    contexts.append({'Context_ID': context_id, 'Context': context})

    # Questions and Answers preparation
    questions_answers = [
        (f"On what date was the version '{version_name}' released for project '{project_name}'?", release_date),
        (f"Is the version '{version_name}' currently archived?", archived_status),
        (f"What is the description of the version '{version_name}'?", version_description),
    ]

    # Add question-answer pairs with context ID and answer start indices
    for q, a in questions_answers:
        a_start = context.find(a)
        qa_pairs.append({
            'Context_ID': context_id,
            'Question': q,
            'Answer': a,
            'Answer_Start_Index': a_start
        })

# Convert lists to DataFrames
contexts_df = pd.DataFrame(contexts)
qa_df = pd.DataFrame(qa_pairs)

# Save to CSV files
contexts_df.to_csv('../cqa_dataset/7_context.csv', index=False)
qa_df.to_csv('../cqa_dataset/7_question_answer.csv', index=False)

print("7_context.csv and 7_question_answer.csv saved")
