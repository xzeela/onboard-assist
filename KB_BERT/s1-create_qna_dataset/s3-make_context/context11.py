import pandas as pd
from tqdm import tqdm

# Load the CSV file
df = pd.read_csv('../data_for_each_context/project_df_context_11.csv')

# Specify the date format
date_format = "%Y-%m-%d %H:%M:%S"

# Convert datetime columns to date-only format
df['Start_Date'] = pd.to_datetime(df['Start_Date'], format=date_format, errors='coerce').dt.date
df['End_Date'] = pd.to_datetime(df['End_Date'], format=date_format, errors='coerce').dt.date
df['Complete_Date'] = pd.to_datetime(df['Complete_Date'], format=date_format, errors='coerce').dt.date
df['Estimation_Date'] = pd.to_datetime(df['Estimation_Date'], format=date_format, errors='coerce').dt.date
df['Resolution_Date'] = pd.to_datetime(df['Resolution_Date'], format=date_format, errors='coerce').dt.date

# Lists for contexts and question-answer pairs
contexts = []
qa_pairs = []

# A variable for unique context IDs
context_id = 0

# Iterate over each row to populate the templates
for index, row in tqdm(df.iterrows(), total=df.shape[0]):
    # Increment the context ID
    context_id += 1

    # Preparing context with converted dates to string
    context = f"The sprint, starting on {str(row['Start_Date'])} and ending on {str(row['End_Date'])}, was completed on {str(row['Complete_Date'])}. It included issues with an estimated completion date of {str(row['Estimation_Date'])} and an actual resolution date of {str(row['Resolution_Date'])}. The sprint comprised issues with varying story points, reflecting the sprint's planning and execution complexity."
    contexts.append({'Context_ID': context_id, 'Context': context})

    # Preparing question-answer pairs
    qa_pairs.extend([
        {
            'Context_ID': context_id,
            'Question': f"When did the sprint including the issue with estimated completion date of {str(row['Estimation_Date'])} start?",
            'Answer': str(row['Start_Date']),
            'Answer_Start_Index': context.find(str(row['Start_Date']))
        },
        {
            'Context_ID': context_id,
            'Question': f"When was the sprint '{row['Sprint_Name']}' completed?",
            'Answer': str(row['Complete_Date']),
            'Answer_Start_Index': context.find(str(row['Complete_Date']))
        },
        {
            'Context_ID': context_id,
            'Question': "What are the story points of the issues included in the sprint?",
            'Answer': "Issues with varying story points",
            'Answer_Start_Index': context.find("story points")
        }
    ])

# Convert lists to DataFrames
contexts_df = pd.DataFrame(contexts)
qa_df = pd.DataFrame(qa_pairs)

# Save to CSV files
contexts_df.to_csv('../cqa_dataset/11_context.csv', index=False)
qa_df.to_csv('../cqa_dataset/11_question_answer.csv', index=False)

print("11_context.csv and 11_question_answer.csv saved")
