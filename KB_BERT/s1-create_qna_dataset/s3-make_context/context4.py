import pandas as pd
from tqdm import tqdm

# Load the CSV file
df = pd.read_csv('../data_for_each_context/project_df_context_4.csv')

# Prepare lists to hold the results for both dataframes
contexts = []
qa_pairs = []

# A variable to keep track of unique context IDs
context_id = 0

# Iterate over each row and populate the templates
for index, row in tqdm(df.iterrows(), total=df.shape[0]):
    # Increment the context ID for each row
    context_id += 1

    # Convert fields to string and handle NaN values
    user_id = str(row['User_ID'])
    project_name = str(row['Project_Name'])
    project_id = str(row['Project_ID'])
    issue_titles = str(row['Issue_Titles']) if pd.notna(row['Issue_Titles']) else "Not specified"

    # Construct the context
    context = f"User ID {user_id} is known for their contributions to the project '{project_name}' (ID {project_id}). They have worked on issues including {issue_titles}. This involvement showcases the user's active role in the development and progression of the project."

    # Add context and its ID to the contexts list
    contexts.append({'Context_ID': context_id, 'Context': context})

    # Questions and Answers preparation
    questions_answers = [
        (f"Which issues has User ID {user_id} worked on in the project '{project_name}'?", issue_titles),
        (f"What has been the role of User ID {user_id} in the development of project '{project_name}'?", "known for their contributions"),
    ]

    # Add question-answer pairs with context ID and answer start indices
    for q, a in questions_answers:
        a_start = context.find(a)
        qa_pairs.append({
            'Context_ID': context_id,
            'Question': q,
            'Answer': a,
            'Answer_Start_Index': a_start
        })

# Convert lists to DataFrames
contexts_df = pd.DataFrame(contexts)
qa_df = pd.DataFrame(qa_pairs)

# Save to CSV files
contexts_df.to_csv('../cqa_dataset/4_context.csv', index=False)
qa_df.to_csv('../cqa_dataset/4_question_answer.csv', index=False)

print("4_context.csv and 4_question_answer.csv saved")
