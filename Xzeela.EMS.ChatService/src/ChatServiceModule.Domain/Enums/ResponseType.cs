namespace ChatServiceModule.Domain.Enums;

public enum ResponseType
{
    Success,
    Failed,
    NotFound,
    UserDontHaveAccess
}