﻿using Xzeela.EMS.Shared.Entity;

namespace ChatServiceModule.Domain.Entities
{
    public class Response : BaseEntity
    {
        public long Message { get; set; }
        public string Content { get; set; } = null!;
    }
}
