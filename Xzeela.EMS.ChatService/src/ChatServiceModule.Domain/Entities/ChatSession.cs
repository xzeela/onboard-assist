﻿using Xzeela.EMS.Shared.Entity;

namespace ChatServiceModule.Domain.Entities
{
    public class ChatSession : BaseEntity
    {
        public string Title { get; set; } = null!;
    }
}
