﻿using Xzeela.EMS.Shared.Entity;

namespace ChatServiceModule.Domain.Entities
{
    public class Message : BaseEntity
    {
        public long ChatSession { get; set; }
        public string Content { get; set; } = null!;
    }
}
