﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatServiceModule.Domain.Exceptions
{
    public class ChatDomainException : Exception
    {
        public ChatDomainException()
        {
        }

        public ChatDomainException(string message) : base(message)
        {
        }

        public ChatDomainException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
