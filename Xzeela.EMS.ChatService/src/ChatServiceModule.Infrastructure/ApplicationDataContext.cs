﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using ChatServiceModule.Domain.Entities;
using Microsoft.EntityFrameworkCore;


namespace ChatServiceModule.Infrastructure
{
    public class ApplicationDataContext : DbContext
    {
        public ApplicationDataContext(
            DbContextOptions<ApplicationDataContext> options
        ) : base(options)
        {
        }

        public DbSet<ChatSession> ChatSessions { get; set; }
        public DbSet<Message> Messages { get; set; }
        public DbSet<Response> Responses { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<ChatSession>()
                .HasKey(chatSession => new { chatSession.Id });

            builder.Entity<Message>()
                .HasKey(messages => new { messages.Id });
            
            builder.Entity<Response>()
                .HasKey(response => new { response.Id });
        }
    }

}
