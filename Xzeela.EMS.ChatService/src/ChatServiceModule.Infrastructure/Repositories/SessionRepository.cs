using ChatServiceModule.Domain.Entities;
using ChatServiceModule.Infrastructure.Interfaces;
using Microsoft.EntityFrameworkCore;
using Xzeela.EMS.Shared.Persistence.Repositories;

namespace ChatServiceModule.Infrastructure.Repositories;

public class SessionRepository : BaseRepository<ChatSession>, ISessionRepository
{
    private readonly DbSet<ChatSession> _sessions;
    public SessionRepository(ApplicationDataContext applicationDbContext) : base(applicationDbContext)
    {
        _applicationDataContext = applicationDbContext;
        _sessions = _applicationDataContext.Set<ChatSession>();
    }

    public async Task<ChatSession?> FindChatSessionByUserId(int id)
    {
        return await _sessions.FirstOrDefaultAsync(x => x.CreatedBy == id);
    }

    public async Task<List<ChatSession>> GetChatSessionsForUser(int id)
    {
        return await _sessions.Where(x => x.CreatedBy == id).ToListAsync();
    }

    public async Task<ChatSession?> GetChatSessionForUser(int userId, int sessionId)
    {
        return await _sessions.FirstOrDefaultAsync(x => x.CreatedBy == userId && x.Id == sessionId);
    }
}