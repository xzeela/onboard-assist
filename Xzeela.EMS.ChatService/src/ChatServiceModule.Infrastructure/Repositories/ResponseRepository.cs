using ChatServiceModule.Domain.Entities;
using ChatServiceModule.Infrastructure.Interfaces;
using Microsoft.EntityFrameworkCore;
using Xzeela.EMS.Shared.Persistence.Repositories;

namespace ChatServiceModule.Infrastructure.Repositories;

public class ResponseRepository : BaseRepository<Response>, IResponseRepository
{
    private readonly DbSet<Response> _responses;
    
    public ResponseRepository(ApplicationDataContext applicationDbContext) : base(applicationDbContext)
    {
        _applicationDataContext = applicationDbContext;
        _responses = _applicationDataContext.Set<Response>();
    }

    public async Task<List<Response>?> GetResponsesForMessages(List<long> messageIds)
    {
        var responses = await _responses
            .Where(r => messageIds.Contains(r.Message))
            .ToListAsync();

        return responses;
    }
}