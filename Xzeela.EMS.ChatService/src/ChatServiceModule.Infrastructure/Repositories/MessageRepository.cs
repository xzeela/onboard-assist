using ChatServiceModule.Domain.Entities;
using ChatServiceModule.Infrastructure.Interfaces;
using Microsoft.EntityFrameworkCore;
using Xzeela.EMS.Shared.Persistence.Repositories;

namespace ChatServiceModule.Infrastructure.Repositories;

public class MessageRepository : BaseRepository<Message>, IMessageRepository
{
    private readonly DbSet<Message> _messages;
    public MessageRepository(ApplicationDataContext applicationDbContext) : base(applicationDbContext)
    {
        _applicationDataContext = applicationDbContext;
        _messages = _applicationDataContext.Set<Message>();
    }

    public async Task<PaginatedList<Message>> GetMessagesForSession(int sessionId, int pageIndex, int pageSize)
    {
        var messages = await _messages
            .Where(x => x.ChatSession == sessionId)
            .OrderByDescending(b => b.Id)
            .Skip((pageIndex - 1) * pageSize)
            .Take(pageSize)
            .ToListAsync();

        var count = await _messages.CountAsync();
        var totalPages = (int)Math.Ceiling(count / (double)pageSize);

        return new PaginatedList<Message>(messages, pageIndex, totalPages);
    }
}