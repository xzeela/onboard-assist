using ChatServiceModule.Domain.Entities;
using Xzeela.EMS.Shared.Persistence.Interfaces;

namespace ChatServiceModule.Infrastructure.Interfaces;

public interface IMessageRepository : IAsyncRepository<Message>
{
    Task<PaginatedList<Message>> GetMessagesForSession(int sessionId, int pageIndex, int pageSize);
}