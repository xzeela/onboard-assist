using ChatServiceModule.Domain.Entities;
using Xzeela.EMS.Shared.Persistence.Interfaces;

namespace ChatServiceModule.Infrastructure.Interfaces;

public interface ISessionRepository : IAsyncRepository<ChatSession>
{
    Task<List<ChatSession>> GetChatSessionsForUser(int id);
    Task<ChatSession?> GetChatSessionForUser(int userId, int sessionId);
}