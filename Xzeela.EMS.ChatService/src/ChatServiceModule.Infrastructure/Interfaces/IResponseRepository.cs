using ChatServiceModule.Domain.Entities;
using Xzeela.EMS.Shared.Persistence.Interfaces;

namespace ChatServiceModule.Infrastructure.Interfaces;

public interface IResponseRepository : IAsyncRepository<Response>
{
    Task<List<Response>?> GetResponsesForMessages(List<long> messageIds);
}