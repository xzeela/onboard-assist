using ChatServiceModule.API.Models.Requests;

namespace ChatServiceModule.API.Services;


public interface IOnBoardAssistService
{
    Task<string> SendMessage(MessageRequest messageRequest);
}

public class OnBoardAssistService : IOnBoardAssistService
{
    public async Task<string> SendMessage(MessageRequest messageRequest)
    {
        await Task.Delay(300);
        return "Test message received from onboard assist";
    }
}