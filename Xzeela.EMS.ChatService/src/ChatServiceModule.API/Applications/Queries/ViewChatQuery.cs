using ChatServiceModule.API.Models.Requests;
using ChatServiceModule.API.Models.Responses;
using ChatServiceModule.Domain.Entities;
using ChatServiceModule.Infrastructure.Interfaces;
using MediatR;
using Xzeela.EMS.Shared.Services;
using ChatServiceModule.Domain.Enums;

namespace ChatServiceModule.API.Applications.Queries;

public class ViewChatQuery : IRequestHandler<ViewChatRequest, ChatResponse>
{
    private readonly IRequestTrackingService _trackingService;
    private readonly IMessageRepository _messageRepository;
    private readonly ISessionRepository _sessionRepository;
    private readonly IResponseRepository _responseRepository;

    public ViewChatQuery(
        IRequestTrackingService trackingService,
        IMessageRepository messageRepository,
        ISessionRepository sessionRepository,
        IResponseRepository responseRepository
    )
    {
        _trackingService = trackingService;
        _sessionRepository = sessionRepository;
        _messageRepository = messageRepository;
        _responseRepository = responseRepository;
    }
    public async Task<ChatResponse> Handle(ViewChatRequest request, CancellationToken cancellationToken)
    {
        var requestedUserId =  _trackingService.GetRequestedUserId();
        var chat = new List<MessageResponse>();
        ChatSession selectedSession;

        if (requestedUserId == null)
            throw new UnauthorizedAccessException(ResponseType.UserDontHaveAccess.ToString());


        if (request.SessionId == null)
        {
            var sessionList = await _sessionRepository.GetChatSessionsForUser(int.Parse(requestedUserId));
            if (!sessionList.Any())
            {
                return new ChatResponse(){};
            }
            selectedSession = sessionList.First();
        }
        else
        {
            var session = await _sessionRepository.GetChatSessionForUser(int.Parse(requestedUserId), (int) request.SessionId!);

            if (session == null)
            {
                var sessionList = await _sessionRepository.GetChatSessionsForUser(int.Parse(requestedUserId));
                if (!sessionList.Any())
                {
                    return new ChatResponse(){};
                }
                selectedSession = sessionList.First();
            }
            else
            {
                selectedSession = session;
            }
        }
        
   

        var messages = await _messageRepository.GetMessagesForSession((int) selectedSession.Id, request.PageIndex, request.PageSize);
        
        var messageIds = messages.Items.Select(m => m.Id).ToList();
        var responses = await _responseRepository.GetResponsesForMessages(messageIds);

        foreach (var message in messages.Items)
        {
            var messageResponse = new MessageResponse
            {
                SessionId = selectedSession.Id,
                MessageId = message.Id,
                Message = message.Content,
                ResponseId = 0, // Set default values if no response is found
                Response = null
            };

            // Check if there is a response for the current message
            var response = responses?.FirstOrDefault(r => r.Message == message.Id);
            if (response != null)
            {
                messageResponse.ResponseId = response.Id;
                messageResponse.Response = response.Content;
            }

            chat.Add(messageResponse);
        }

        var chatResponse = new ChatResponse();
        chatResponse.Chat.AddRange(chat);
        return chatResponse;
    }
}