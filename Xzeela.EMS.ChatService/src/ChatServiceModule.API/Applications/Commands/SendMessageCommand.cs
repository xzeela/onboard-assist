using ChatServiceModule.API.Models.Requests;
using ChatServiceModule.API.Models.Responses;
using ChatServiceModule.API.Services;
using ChatServiceModule.Domain.Entities;
using ChatServiceModule.Domain.Enums;
using ChatServiceModule.Domain.Exceptions;
using ChatServiceModule.Infrastructure.Interfaces;
using MediatR;
using Xzeela.EMS.Shared.Services;

namespace ChatServiceModule.API.Applications.Commands;

public class SendMessageCommand : IRequestHandler<MessageRequest, MessageResponse>
{
    private readonly IRequestTrackingService _trackingService;
    private readonly IMessageRepository _messageRepository;
    private readonly ISessionRepository _sessionRepository;
    private readonly IResponseRepository _responseRepository;
    private readonly IOnBoardAssistService _onBoardAssistService;

    public SendMessageCommand(
        IRequestTrackingService trackingService,
        IMessageRepository messageRepository,
        ISessionRepository sessionRepository,
        IResponseRepository responseRepository,
        IOnBoardAssistService onBoardAssistService
    )
    {
        _trackingService = trackingService;
        _sessionRepository = sessionRepository;
        _messageRepository = messageRepository;
        _responseRepository = responseRepository;
        _onBoardAssistService = onBoardAssistService;
    }

    public async Task<MessageResponse> Handle(MessageRequest request, CancellationToken cancellationToken)
    {
        var requestedUserId =  _trackingService.GetRequestedUserId();

        if (requestedUserId == null)
            throw new UnauthorizedAccessException(ResponseType.UserDontHaveAccess.ToString());

        ChatSession selectedSession;
        
        if (request.SessionId != null && request.SessionId != 0)
        {
            var existingSession =
                await _sessionRepository.GetChatSessionForUser(int.Parse(requestedUserId), (int) request.SessionId);

            selectedSession = existingSession ?? throw new UnauthorizedAccessException(ResponseType.UserDontHaveAccess.ToString());
        }
        else
        {
            var sessionList = await _sessionRepository.GetChatSessionsForUser(int.Parse(requestedUserId));

            if (!sessionList.Any())
            {
                var newSession = new ChatSession()
                {
                    CreatedBy = int.Parse(requestedUserId),
                    CreatedOn = DateTime.UtcNow,
                    Title = "Default Session"
                };
                
                newSession = await _sessionRepository.AddAsync(newSession);
                selectedSession = newSession ?? throw new ChatDomainException("Unable to start the chat");
            }
            else
            {
                selectedSession = sessionList.First();
            }
        }
        
        var session = await _sessionRepository.GetChatSessionsForUser(int.Parse(requestedUserId));

        if (!session.Any())
        {
            var newSession = new ChatSession()
            {
                CreatedBy = int.Parse(requestedUserId),
                CreatedOn = DateTime.UtcNow,
                Title = "Default Session"
            };

            newSession = await _sessionRepository.AddAsync(newSession);

            if (newSession == null) throw new ChatDomainException("Unable to start the chat");
        }

        var onboardAssistResponse = await _onBoardAssistService.SendMessage(request);
         
        if(string.IsNullOrEmpty(onboardAssistResponse))
            throw new ChatDomainException("Unable get the response from Onboard Assist");

        var saveMessageResponse = await _messageRepository.AddAsync(new Message()
        {
            CreatedBy = int.Parse(requestedUserId),
            CreatedOn = DateTime.UtcNow,
            Content = request.Content,
            ChatSession = selectedSession.Id
        });
        
        if(saveMessageResponse == null) throw new ChatDomainException("Unable to start the chat");

        var saveAnswerResponse = await _responseRepository.AddAsync(new Response()
        {
            Content = onboardAssistResponse,
            Message = saveMessageResponse.Id,
            CreatedBy = int.Parse(requestedUserId),
            CreatedOn = DateTime.UtcNow,
        });

        if (saveAnswerResponse == null)
        {
            _ = await _messageRepository.DeleteAsync(saveMessageResponse);
            throw new ChatDomainException("Unable to start the chat");
        }

        return new MessageResponse()
        {
            SessionId = selectedSession.Id,
            Message = saveMessageResponse.Content,
            Response = saveAnswerResponse.Content,
            MessageId = saveMessageResponse.Id,
            ResponseId = saveAnswerResponse.Id
        };
    }
}