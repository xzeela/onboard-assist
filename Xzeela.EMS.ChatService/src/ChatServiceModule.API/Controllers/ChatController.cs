﻿using ChatServiceModule.API.Models.Requests;
using ChatServiceModule.Domain.Enums;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Xzeela.EMS.Shared.API.Models;
using Xzeela.EMS.Shared.Services;

namespace ChatServiceModule.API.Controllers
{
    [ApiController]
    [Authorize(AuthenticationSchemes = "Bearer")]
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/xzeela/[controller]")]
    public class ChatController : ControllerBase
    {
        private readonly IRequestTrackingService _trackingService;
        private readonly IMediator _mediator;
        public ChatController(
           IRequestTrackingService trackingService,
           IMediator mediator)
        {
            _trackingService = trackingService;
            _mediator = mediator;
        }

        /// <summary>
        /// Send a message to Onboard Assist
        /// </summary>
        /// <param name="messageRequest">Message Details</param>
        /// <returns>Reply from Onboard Assist</returns>
        /// <response code="201">Reply from Onboard Assist</response>
        [HttpPost]
        [ProducesResponseType(typeof(BaseResponse), StatusCodes.Status201Created)]
        [ProducesResponseType(typeof(BaseResponse), StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(typeof(BaseResponse), StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<BaseResponse>> Post([FromBody] MessageRequest messageRequest)
        {
            try
            {
                var traceId = _trackingService.GetRequestId() ?? string.Empty;
                var response = await _mediator.Send(messageRequest);
                return Ok(new BaseResponse(traceId, response, ""));
            }
            catch (Exception ex)
            {
                _trackingService.TrackRequest($"uri: [POST]/chat/ - exception: {ex.Message}");
                return BadRequest(new BaseResponse(null!, ResponseType.Failed.ToString()));
            }
        }
        
        /// <summary>
        /// Get the chat details
        /// </summary>
        /// <param name="viewChatRequest">Chat Details</param>
        /// <returns>Chat content for the given session if passed and empty session then return the default session</returns>
        /// <response code="201">Chat Content</response>
        [HttpGet]
        [ProducesResponseType(typeof(BaseResponse), StatusCodes.Status201Created)]
        [ProducesResponseType(typeof(BaseResponse), StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(typeof(BaseResponse), StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<BaseResponse>> Get([FromQuery] ViewChatRequest viewChatRequest)
        {
            try
            {
                var traceId = _trackingService.GetRequestId() ?? string.Empty;
                var response = await _mediator.Send(viewChatRequest);
                return Ok(new BaseResponse(traceId, response, ""));
            }
            catch (Exception ex)
            {
                _trackingService.TrackRequest($"uri: [GET]/chat/ - exception: {ex.Message}");
                return BadRequest(new BaseResponse(null!, ResponseType.Failed.ToString()));
            }
        }
    }
}
