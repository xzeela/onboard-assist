﻿using System.ComponentModel.DataAnnotations;
using ChatServiceModule.API.Models.Responses;
using MediatR;

namespace ChatServiceModule.API.Models.Requests
{
    public class MessageRequest : IRequest<MessageResponse>
    {
        /// <summary>
        /// Message content
        /// </summary>
        [Required(ErrorMessage = "Message body cannot be empty")]
        public string Content { get; set; } = null!;
        
        /// <summary>
        /// UserId is automatically filled by after validating the JWT token
        /// </summary>
        public int UserId { get; set; }
        
        /// <summary>
        /// Session that belong
        /// </summary>
        public int? SessionId { get; set; }
    }
}
