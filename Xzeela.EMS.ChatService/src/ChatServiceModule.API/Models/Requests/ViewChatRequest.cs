using ChatServiceModule.API.Models.Responses;
using MediatR;

namespace ChatServiceModule.API.Models.Requests;

public class ViewChatRequest : IRequest<ChatResponse>
{ 
    public int? SessionId { get; set; }
    public int PageIndex { get; set; } = 1;
    public int PageSize { get; set; } = 10;
}