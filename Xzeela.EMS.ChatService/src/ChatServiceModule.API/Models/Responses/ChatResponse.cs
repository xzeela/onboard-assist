namespace ChatServiceModule.API.Models.Responses;

public class ChatResponse
{
    public List<MessageResponse> Chat { get; set; } = new();
}