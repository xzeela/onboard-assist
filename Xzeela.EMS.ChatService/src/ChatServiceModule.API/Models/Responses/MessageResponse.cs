namespace ChatServiceModule.API.Models.Responses;

public class MessageResponse
{   
    public long SessionId { get; set; }
    public long MessageId { get; set; }
    public string Message { get; set; } = null!;
    public long ResponseId { get; set; }
    public string? Response { get; set; }
}