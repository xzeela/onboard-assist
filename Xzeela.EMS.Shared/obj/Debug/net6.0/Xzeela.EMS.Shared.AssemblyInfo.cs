//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Reflection;

[assembly: System.Reflection.AssemblyCompanyAttribute("xzeela")]
[assembly: System.Reflection.AssemblyConfigurationAttribute("Debug")]
[assembly: System.Reflection.AssemblyDescriptionAttribute("common utilities that will used between xzeela.ems services")]
[assembly: System.Reflection.AssemblyFileVersionAttribute("1.0.5.0")]
[assembly: System.Reflection.AssemblyInformationalVersionAttribute("1.0.5")]
[assembly: System.Reflection.AssemblyProductAttribute("Xzeela.EMS.Shared")]
[assembly: System.Reflection.AssemblyTitleAttribute("Xzeela.EMS.Shared")]
[assembly: System.Reflection.AssemblyVersionAttribute("1.0.5.0")]

// Generated by the MSBuild WriteCodeFragment class.

