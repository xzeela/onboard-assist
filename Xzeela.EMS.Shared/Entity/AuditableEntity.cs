﻿using System;
namespace Xzeela.EMS.Shared.Entity
{
	public abstract class AuditableEntity
	{
		public long? CreatedBy { get; set; }
		public DateTimeOffset? CreatedOn { get; set; }
		public long? LastModifiedBy { get; set; }
        public DateTimeOffset? LastModifiedOn { get; set; }
		public bool IsDeleted { get; set; } = false;
	}
}

