﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Xzeela.EMS.Shared.Entity
{
	public class BaseEntity : AuditableEntity
	{
		[Key] public long Id { get; set; }
	}
}

