namespace Xzeela.EMS.Shared.API.Models;

public class BaseResponse
{
    public BaseResponse(dynamic result, string message)
    {
        Result = result;
        Message = message;
        TimeStamp = DateTime.UtcNow;
    }

    public BaseResponse(string traceId, dynamic result, string message)
    {
        Result = result;
        Message = message;
        TraceId = traceId;
        TimeStamp = DateTime.UtcNow;
    }

    public string TraceId { get; set; }
    public dynamic Result { get; set; }
    public string Message { get; set; }
    public DateTime TimeStamp { get; set; }
}