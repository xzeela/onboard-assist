﻿using Microsoft.AspNetCore.Http;

namespace Xzeela.EMS.Shared.Services
{
    public interface IRequestTrackingService
    {
        void TrackRequest(string message);
        string? GetRequestId();
        string? GetBaseURL();
        string? GetRequestedUserId();
        string? GetRequestedUserPrivilege();
        string? GetRequestUserTenantId();
    }

    public class RequestTrackingService : IRequestTrackingService
    {
        
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly ILoggerService _loggerService;

        public RequestTrackingService(
            IHttpContextAccessor httpContextAccessor,
            ILoggerService loggerService)
        {
            _loggerService = loggerService;
            _httpContextAccessor = httpContextAccessor;
        }


        public void TrackRequest(string message)
        {
            string? requestId = _httpContextAccessor?.HttpContext?.Items["RequestId"] as string;
            _loggerService.Logger.Information($"Request ID: {requestId}, {message}");
        }

        public string? GetRequestId()
        {
            return _httpContextAccessor.HttpContext?.Items["RequestId"] as string;
        }

        public string? GetBaseURL()
        {
            return $"{_httpContextAccessor.HttpContext?.Request?.Scheme}://{_httpContextAccessor.HttpContext?.Request?.Host}/";
        }

        public string? GetRequestedUserId()
        {
            return _httpContextAccessor.HttpContext?.Items["RequestedUserId"] as string;
        }

        public string? GetRequestedUserPrivilege()
        {
            return _httpContextAccessor.HttpContext?.Items["RequestedUserPrivilege"] as string;
        }
        
        public string? GetRequestUserTenantId()
        {
            var x = _httpContextAccessor.HttpContext?.Items;
            return _httpContextAccessor.HttpContext?.Items["TenantId"] as string;
        }
    }
}

