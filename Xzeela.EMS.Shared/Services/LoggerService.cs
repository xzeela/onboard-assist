﻿using Serilog;

namespace Xzeela.EMS.Shared.Services
{

    public interface ILoggerService
    {
        public ILogger Logger { get; }

    }

    public class LoggerService : ILoggerService
    {

        public ILogger Logger { get; }

        public LoggerService()
        {
            Logger = new LoggerConfiguration()
                .WriteTo.NLog()
                .MinimumLevel.Information()
                .CreateLogger();
        }
    }
}

