using System.Security.Claims;

namespace Identity.API.Middlewares;

public class FetchAuthenticationInfoMiddleware
{
    private readonly RequestDelegate _next;

    public FetchAuthenticationInfoMiddleware(RequestDelegate next)
    {
        _next = next;
    }

    public async Task Invoke(HttpContext context)
    {
        if (context.User.Identity is { IsAuthenticated: true })
        {
            // Extract and store JWT claims in context for later use
            var privilegeType = context.User.FindFirst("PrivilegeType")?.Value;
            var requestedUserId = context.User.FindFirst("Id")?.Value;

            context.Items["RequestedUserId"] = requestedUserId;
            context.Items["RequestedUserPrivilege"] = privilegeType;
        }
        await _next(context);
    }
}