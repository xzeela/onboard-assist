﻿using System;
using System.Net;
using Newtonsoft.Json;

namespace Identity.API.Middlewares
{
    public class ExceptionHandlingMiddleware : IMiddleware
    {
        public async Task InvokeAsync(HttpContext context, RequestDelegate next)
        {
            try
            {
                await next(context);
            }
            catch (Exception ex)
            {
                context.Response.ContentType = "application/json";
                var result = JsonConvert.SerializeObject(new { Message = ex.Message, Source = ex.Source, Type = ex.GetType().Name });

                if (ex is UnauthorizedAccessException)
                {
                    context.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                }
                else
                {
                    context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                }
                
                await context.Response.WriteAsync(result);
            }
        }
    }
}