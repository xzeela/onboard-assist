using System.Text;
using Identity.API.Models.Responses;
using Identity.API.Utility;
using Newtonsoft.Json;
using Xzeela.EMS.Shared.Services;
using JsonSerializer = System.Text.Json.JsonSerializer;

namespace Identity.API.Services;

public interface IEmailService
{
    Task<EmailResponse> SendTemplatelessEmail(string recipient, string subject, string message);
}

public class EmailService : IEmailService
{
    private readonly ILogger<EmailService> _logger;
    private readonly IRequestTrackingService _trackingService;
    //Hold the information about Email Configurations
    private readonly EmailSetting _emailSetting;
    
    public EmailService(
        IRequestTrackingService trackingService,
        ILogger<EmailService> logger,
        IConfiguration configuration
    )
    {
        _trackingService = trackingService;
        _logger = logger;
        _emailSetting = configuration.GetSection("EmailConfigurations").Get<EmailSetting>();
    }
    public async Task<EmailResponse> SendTemplatelessEmail(string recipient, string subject, string message)
    {
        using var client = new HttpClient();
        
        client.DefaultRequestHeaders.Add("Authorization", $"Bearer {_emailSetting.APIKey}");
        
        var jsonPayload = new
        {
            from = _emailSetting.From, 
            to = recipient,      
            subject = subject, 
            html = message  
        };

        // Serialize the JSON payload to a string
        var jsonString = JsonSerializer.Serialize(jsonPayload);

        // Create a StringContent object with the JSON payload and content type
        var content = new StringContent(jsonString, Encoding.UTF8, "application/json");

        var response = await client.PostAsync(_emailSetting.APIUrl, content);

        var statusCode = response.StatusCode.ToString();
        var emailId = "";

        if (!response.IsSuccessStatusCode)
            return new EmailResponse { StatusCode = statusCode, Id = emailId };
            
        // Parse the response content to extract the email id
        var responseContent = await response.Content.ReadAsStringAsync();
        var responseObject = JsonConvert.DeserializeObject<EmailResponse>(responseContent);
        emailId = responseObject?.Id;

        return new EmailResponse { StatusCode = statusCode, Id = emailId };
    }
}