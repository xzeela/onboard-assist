namespace Identity.API.Utility;

public class EmailSetting
{
    public string APIKey { get; set; } = null!;
    public string From { get; set; } = null!;
    public string APIUrl { get; set; } = null!;
}