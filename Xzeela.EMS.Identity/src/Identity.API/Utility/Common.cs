﻿using System.Text;

namespace Identity.API.Utility;

public static class Common
{
    public static string CreatePassword(int length)
    {
        const string valid = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        var res = new StringBuilder();
        var rnd = new Random();
        while (0 < length--)
        {
            res.Append(valid[rnd.Next(valid.Length)]);
        }
        return res.ToString() + "2023!#";
    }

    public static int GetMonths(DateTime startDate, DateTime endDate)
    {
        // Return 0 if the start date is in the future
        if (DateTime.Now.Date < startDate.Date) return 0;

        // Use the end date provided, or the current date if none is provided
        endDate = endDate == DateTime.MinValue ? DateTime.Today : endDate;

        // Calculate the number of months between the start and end dates
        var months = endDate.Month - startDate.Month;

        // Adjust the months if the end date is before the start date
        if (endDate.Day < startDate.Day)
        {
            months--;
        }

        // If the number of months is negative, add 12 months to get the total number of months
        if (months < 0)
        {
            months += 12;
        }
        return months;
    }

    public static int GetYears(DateTime startDate, DateTime endDate)
    {
        // Use the end date provided, or the current date if none is provided
        endDate = endDate == DateTime.MinValue ? DateTime.Today : endDate;

        // Calculate the number of years between the start and end dates
        var years = endDate.Year - startDate.Year;

        // Adjust the years if the end date is before the start date
        if (endDate < startDate)
        {
            years--;
        }
        return years;
    }


    public static int GetMonthsBetweenYears(DateTime startDate, DateTime endDate)
    {
        int months = 0;
        // Calculate the number of years between the two dates
        int years = GetYears(startDate, endDate);

        // Subtract the start year from the end year to get the number of full years between the two dates
        int fullYears = endDate.Year - startDate.Year;

        // Calculate the number of months between the two dates by subtracting the start month from the end month
        months = endDate.Month - startDate.Month;

        // Add the number of months to the number of years to get the total number of months between the two dates
        months += fullYears * 12;

        return months;
    }
}

