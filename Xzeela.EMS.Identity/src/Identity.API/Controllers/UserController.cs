using Identity.API.Models.Requests;
using Identity.Domain.Enums;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Xzeela.EMS.Shared.API.Models;
using Xzeela.EMS.Shared.Services;

namespace Identity.API.Controllers;

[ApiController]
[Authorize(AuthenticationSchemes = "Bearer")]
[ApiVersion("1.0")]
[Route("api/v{version:apiVersion}/xzeela/[controller]")]
public class UserController : ControllerBase
{
    private readonly IRequestTrackingService _trackingService;
    private readonly IMediator _mediator;

    public UserController(
        IRequestTrackingService trackingService,
        IMediator mediator)
    {
        _trackingService = trackingService;
        _mediator = mediator;
    }
    
    
    /// <summary>
    /// Creates a User under the Given Tenant.
    /// </summary>
    /// <param name="createUserRequest">The command to create a User.</param>
    /// <returns>URI path to access the newly created User.</returns>
    /// <response code="201">Returns the newly created User.</response>
    /// <response code="400">Returns a bad request if the operation fails.</response>
    [HttpPost]
    [AllowAnonymous]
    [ProducesResponseType(typeof(BaseResponse), StatusCodes.Status201Created)]
    [ProducesResponseType(typeof(BaseResponse), StatusCodes.Status400BadRequest)]
    public async Task<ActionResult<BaseResponse>> CreateUser(CreateUserRequest createUserRequest)
    {
        try
        {
            // Serialize the updateTenantRequest object to JSON for logging.
            var requestBodyJson = JsonConvert.SerializeObject(createUserRequest);
            _trackingService.TrackRequest($"uri: [POST]/user/ - request: {requestBodyJson}");
                
            var userId = await _mediator.Send(createUserRequest);
            var response = new BaseResponse(userId, ResponseType.Success.ToString());
            var responseBodyJson = JsonConvert.SerializeObject(response);
            _trackingService.TrackRequest($"uri: [POST]/user/ - response: {responseBodyJson}");

            return Created($"api/v1/xzeela/user/{userId}", response);
        }
        catch (Exception ex)
        {
            _trackingService.TrackRequest($"uri: [POST]/user/ - exception: {ex.Message}");
            return BadRequest(new BaseResponse(null!, ResponseType.Failed.ToString()));
        }
    }
}