﻿using Identity.API.Applications.Commands;
using Identity.API.Models.Requests;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Identity.Domain.Enums;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using Xzeela.EMS.Shared.API.Models;
using Xzeela.EMS.Shared.Services;

namespace Identity.API.Controllers
{
    [ApiController]
    [AllowAnonymous]
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/xzeela/[controller]")]
    public class AuthController : ControllerBase
    {
        private readonly IRequestTrackingService _trackingService;
        private readonly IMediator _mediator;

        public AuthController(
            IRequestTrackingService trackingService,
            IMediator mediator)
        {
            _trackingService = trackingService;
            _mediator = mediator;
        }

        /// <summary>
        /// Resets a user's password.
        /// </summary>
        /// <param name="resetPasswordRequest">The request to reset the user's password.</param>
        /// <returns>A response indicating the success or failure of the password reset.</returns>
        /// <response code="200">Returns true if the password reset was successful.</response>
        /// <response code="400">Returns false if the password reset failed.</response>
        [HttpPost("reset-password")]
        [ProducesResponseType(typeof(bool), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(bool), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> ResetPassword([FromBody] ResetPasswordRequest resetPasswordRequest)
        {
            try
            {
                // Serialize the resetPasswordRequest object to JSON for logging.
                var requestBodyJson = JsonConvert.SerializeObject(resetPasswordRequest);
                _trackingService.TrackRequest($"uri: auth/reset-password - request: {requestBodyJson}");
                
                var result = await _mediator.Send(resetPasswordRequest);
                if (!result)
                    return BadRequest(new BaseResponse(_trackingService.GetRequestId() ?? string.Empty, null!,
                        "Unable to reset the password"));
                
                var response = new BaseResponse(_trackingService.GetRequestId() ?? string.Empty, null!,
                    "Password reset successfully");
                var responseBodyJson = JsonConvert.SerializeObject(response);

                _trackingService.TrackRequest($"uri: auth/reset-password - response: {responseBodyJson}");
                return Ok(response);

            }
            catch (Exception ex)
            {
                _trackingService.TrackRequest($"uri: auth/reset-password - exception: {ex.Message}");
                return BadRequest(new BaseResponse(_trackingService.GetRequestId() ?? string.Empty, null!,"Unable to reset the password"));
            }
        }

        /// <summary>
        /// Signs in a user with the provided email and password.
        /// </summary>
        /// <param name="signInRequest">The request to sign in a user.</param>
        /// <returns>A response containing an authentication token if successful.</returns>
        /// <response code="200">Returns the authentication token if sign-in is successful.</response>
        /// <response code="400">Returns a bad request response if sign-in fails. (If any exception thrown)</response>
        ///<response code="401">Returns a bad request response if sign-in fails.</response>
        [HttpPost("signin")]
        [ProducesResponseType(typeof(BaseResponse), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(BaseResponse), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> SignIn([FromBody] SignInRequest signInRequest)
        {
            try
            {
                var traceId = _trackingService.GetRequestId() ?? string.Empty;

                // Serialize the resetPasswordRequest object to JSON for logging.
                var requestBodyJson = JsonConvert.SerializeObject(signInRequest);
                _trackingService.TrackRequest($"uri: auth/signin - request: {requestBodyJson}");
                
                var superAdminSignInResponse = await _mediator.Send(new SuperAdminSignInRequest
                {
                    Email = signInRequest.Email,
                    Password = signInRequest.Password
                });

                superAdminSignInResponse.TraceId = traceId;

                if (superAdminSignInResponse.Message == ResponseType.Success.ToString())
                {
                    var responseBodyJson = JsonConvert.SerializeObject(superAdminSignInResponse);
                    _trackingService.TrackRequest($"uri: auth/signin - request: {responseBodyJson}");
                    return Ok(superAdminSignInResponse);
                }                
                
                var generalUserSignInResponse = await _mediator.Send(new GeneralUserSignInRequest()
                {
                    Email = signInRequest.Email,
                    Password = signInRequest.Password
                });
                
                if (generalUserSignInResponse.Message == ResponseType.Success.ToString())
                {
                    generalUserSignInResponse.TraceId = traceId;
                    var responseBodyJson = JsonConvert.SerializeObject(generalUserSignInResponse);
                    _trackingService.TrackRequest($"uri: auth/signin - request: {responseBodyJson}");
                    return Ok(generalUserSignInResponse);
                }


                var unauthorizedResponse = new BaseResponse(null!, ResponseType.Failed.ToString())
                {
                    TraceId = traceId
                };

                var unauthorizedSerializeResponse= JsonConvert.SerializeObject(generalUserSignInResponse);
                _trackingService.TrackRequest($"uri: auth/signin - request: {unauthorizedSerializeResponse}");
                
                return Unauthorized(unauthorizedResponse);
            }
            catch (Exception ex)
            {
                _trackingService.TrackRequest($"uri: auth/signin - exception: {ex.Message}");
                return BadRequest(new BaseResponse(_trackingService.GetRequestId() ?? string.Empty, null!, "Unable to Authenticate the User"));
            }
        }
         /// <summary>
        /// Sign out the user session
        /// </summary>
        /// <param name="signInRequest">The request to sign out a user.</param>
        /// <returns>Status of sign out request.</returns>
        /// <response code="200">Returns if the sign out success</response>
        /// <response code="400">Returns if there is any error when there is an error happen in sign out. (If any exception thrown)</response>
        [HttpPost("signout")]
        [ProducesResponseType(typeof(BaseResponse), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(BaseResponse), StatusCodes.Status400BadRequest)]
        //public async Task<IActionResult> SignOut([FromBody] SignOutRequest signOutRequest)
        public async Task<IActionResult> SignOut()
        {
            try
            {
                //since there are no external services involve for now return 200; 
                return Ok();
            }
            catch (Exception ex)
            {
                _trackingService.TrackRequest($"uri: auth/signout - exception: {ex.Message}");
                return BadRequest(new BaseResponse(_trackingService.GetRequestId() ?? string.Empty, null!, "Unable to Signout the User"));
            }
        }
        
        /// <summary>
        /// Creates a rest password token.
        /// </summary>
        /// <param name="createResetPasswordTokenRequest">The request to create reset password token.</param>
        /// <returns>A response indicating the success or failure of the password reset.</returns>
        /// <response code="200">Returns true if the rest password token created was successful.</response>
        /// <response code="400">Returns false if the reset password token failed.</response>
        [HttpPost("reset-password-token")]
        [ProducesResponseType(typeof(bool), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(bool), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> CreatePasswordToken([FromBody] CreateResetPasswordTokenRequest createResetPasswordTokenRequest)
        {
            try
            {
                // Serialize the createResetPasswordTokenRequest object to JSON for logging.
                var requestBodyJson = JsonConvert.SerializeObject(createResetPasswordTokenRequest);
                _trackingService.TrackRequest($"uri: auth/reset-password-token - request: {requestBodyJson}");
                
                var result = await _mediator.Send(createResetPasswordTokenRequest);
                
                if (!result)
                    return BadRequest(new BaseResponse(_trackingService.GetRequestId() ?? string.Empty, null!,
                        "Unable to sent password reset token"));
                
                var response = new BaseResponse(_trackingService.GetRequestId() ?? string.Empty, null!,
                    "Password reset token sent successfully");
                
                var responseBodyJson = JsonConvert.SerializeObject(response);
                _trackingService.TrackRequest($"uri: auth/reset-password-token - response: {responseBodyJson}");
                return Ok(response);

            }
            catch (Exception ex)
            {
                _trackingService.TrackRequest($"uri: auth/reset-password-token - exception: {ex.Message}");
                return BadRequest(new BaseResponse(_trackingService.GetRequestId() ?? string.Empty, null!, "Unable to reset the password"));
            }
        }
    }
}
