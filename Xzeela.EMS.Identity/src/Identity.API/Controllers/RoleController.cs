﻿using Identity.API.Applications.Commands;
using Identity.API.Models.Requests;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Identity.Domain.Enums;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using Xzeela.EMS.Shared.API.Models;
using Xzeela.EMS.Shared.Services;

namespace Identity.API.Controllers
{
    [ApiController]
    [Authorize(AuthenticationSchemes = "Bearer")]
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/xzeela/[controller]")]
    public class RoleController : ControllerBase
    {
        private readonly IRequestTrackingService _trackingService;
        private readonly IMediator _mediator;

        public RoleController(
            IRequestTrackingService trackingService,
            IMediator mediator)
        {
            _trackingService = trackingService;
            _mediator = mediator;
        }

        /// <summary>
        /// Creates a role for the Tenant.
        /// </summary>
        /// <param name="createRoleCommand">The command to create a role.</param>
        /// <returns>URI path to access the newly created role.</returns>
        /// <response code="201">Returns the newly created role.</response>
        /// <response code="400">Returns a bad request if the operation fails.</response>
        [HttpPost]
        [ProducesResponseType(typeof(BaseResponse), StatusCodes.Status201Created)]
        [ProducesResponseType(typeof(BaseResponse), StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<BaseResponse>> CreateRole(CreateRoleRequest createRoleRequest)
        {
            try
            {
                // Serialize the updateTenantRequest object to JSON for logging.
                var requestBodyJson = JsonConvert.SerializeObject(createRoleRequest);
                _trackingService.TrackRequest($"uri: [POST]/role/ - request: {requestBodyJson}");
                
                var roleId = await _mediator.Send(createRoleRequest);
                var response = new BaseResponse(roleId, ResponseType.Success.ToString());
                var responseBodyJson = JsonConvert.SerializeObject(response);
                _trackingService.TrackRequest($"uri: [POST]/role/ - response: {responseBodyJson}");

                return Created($"api/v1/xzeela/role/{roleId}", response);
            }
            catch (Exception ex)
            {
                _trackingService.TrackRequest($"uri: [POST]/role/ - exception: {ex.Message}");
                return BadRequest(new BaseResponse(null!, ResponseType.Failed.ToString()));
            }
        }
    }
}
