﻿using Identity.API.Applications.Commands;
using Identity.API.Applications.Queries;
using Identity.API.Models.Requests;
using Identity.Domain.Enums;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Xzeela.EMS.Shared.API.Models;
using Xzeela.EMS.Shared.Services;

namespace Identity.API.Controllers
{
    [ApiController]
    [Authorize(AuthenticationSchemes = "Bearer")]
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/xzeela/[controller]")]
    public class TenantController : ControllerBase
    {
        private readonly IRequestTrackingService _trackingService;
        private readonly IMediator _mediator;

        public TenantController(
            IRequestTrackingService trackingService,
            IMediator mediator)
        {
            _trackingService = trackingService;
            _mediator = mediator;
        }

        private bool IsEmsAdmin()
        {
            var privilege = _trackingService.GetRequestedUserPrivilege();
            return privilege != null && privilege == PrivilegeType.EmsAdmin.ToString();
        }
        
        /// <summary>
        /// Creates a tenant.
        /// </summary>
        /// <param name="createTenantRequest">The request to create a tenant.</param>
        /// <returns>URI path to access the newly created tenant.</returns>
        /// <response code="201">Returns the newly created tenant</response>
        [HttpPost]
        [ProducesResponseType(typeof(BaseResponse), StatusCodes.Status201Created)]
        [ProducesResponseType(typeof(BaseResponse), StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(typeof(BaseResponse), StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<BaseResponse>> CreateTenant([FromForm] CreateTenantRequest createTenantRequest)
        {
            try
            {
                if (!IsEmsAdmin())
                {
                    return Unauthorized(new BaseResponse(null!, ResponseType.UserDontHaveAccess.ToString()));
                }
                
                // Serialize the createTenantRequest object to JSON for logging.
                var requestBodyJson = JsonConvert.SerializeObject(createTenantRequest);
                _trackingService.TrackRequest($"uri: [POST]/tenant/ - request: {requestBodyJson}");
                
                var tenantId = await _mediator.Send(createTenantRequest);
                var response = new BaseResponse(_trackingService.GetRequestId() ?? string.Empty, tenantId,
                    ResponseType.Success.ToString());
                
                var responseBodyJson = JsonConvert.SerializeObject(response);
                _trackingService.TrackRequest($"uri: [POST]/tenant/ - response: {responseBodyJson}");
                return Ok(response);
            }
            catch (Exception ex)
            {
                _trackingService.TrackRequest($"uri: [POST]/tenant/ - exception: {ex.Message}");
                return BadRequest(new BaseResponse(_trackingService.GetRequestId() ?? string.Empty,null!, ResponseType.Failed.ToString()));
            }
        }

        /// <summary>
        /// Retrieves a list of all tenants.
        /// </summary>
        /// <returns>A response containing a list of tenant information if successful.</returns>
        /// <response code="200">Returns a list of tenants if the operation is successful.</response>
        [HttpGet("getalltenants")]
        [ProducesResponseType(typeof(BaseResponse), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(BaseResponse), StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(typeof(BaseResponse), StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<BaseResponse>> GetAllTenants()
        {
            try
            {
                if (!IsEmsAdmin())
                {
                    return Unauthorized(new BaseResponse(null!, ResponseType.UserDontHaveAccess.ToString()));
                }
                
                _trackingService.TrackRequest($"uri: [GET]tenant/getalltenants - requested by: {_trackingService.GetRequestedUserId()}");
                
                var tenants = await _mediator.Send(new GetAllTenantsQuery());
                var response = new BaseResponse(_trackingService.GetRequestId() ?? string.Empty, tenants,
                    ResponseType.Success.ToString());
                
                var responseBodyJson = JsonConvert.SerializeObject(response);
                _trackingService.TrackRequest($"uri: tenant/getalltenants - response: {responseBodyJson}");
                return Ok(response);
            }
            catch (Exception ex)
            {
                _trackingService.TrackRequest($"uri: [GET]tenant/getalltenants - exception: {ex.Message}");
                return BadRequest(new BaseResponse(_trackingService.GetRequestId() ?? string.Empty,null!, ResponseType.Failed.ToString()));
            }
        }

        /// <summary>
        /// Retrieves tenant information by ID.
        /// </summary>
        /// <param name="tenantId">The ID of the tenant to retrieve.</param>
        /// <returns>A response containing tenant information if found.</returns>
        /// <response code="200">Returns tenant information if found.</response>
        /// <response code="404">Returns not found if the tenant is not found.</response>
        [HttpGet("{tenantId}")]
        [ProducesResponseType(typeof(BaseResponse), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(BaseResponse), StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(typeof(BaseResponse), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(BaseResponse), StatusCodes.Status404NotFound)]
        public async Task<ActionResult<BaseResponse>> GetTenantById(int tenantId)
        {
            try
            {
                if (!IsEmsAdmin())
                {
                    return Unauthorized(new BaseResponse(null!, ResponseType.UserDontHaveAccess.ToString()));
                }
                
                _trackingService.TrackRequest($"uri: [GET]tenant/{tenantId} - request: {tenantId}");

                var tenant = await _mediator.Send(new GetTenantByIdRequest { Id = tenantId });
                var response = new BaseResponse(_trackingService.GetRequestId() ?? string.Empty, tenant,
                    ResponseType.Success.ToString());
                
                var responseBodyJson = JsonConvert.SerializeObject(response);
                _trackingService.TrackRequest($"uri: [GET]tenant/{tenantId} - response: {responseBodyJson}");
                return Ok(response);
            }
            catch (Exception ex)
            {
                _trackingService.TrackRequest($"uri: [GET]tenant/{tenantId} - exception: {ex.Message}");
                return BadRequest(new BaseResponse(_trackingService.GetRequestId() ?? string.Empty,null!, ResponseType.Failed.ToString()));
            }
        }

        /// <summary>
        /// Retrieves the display picture of a tenant by ID.
        /// </summary>
        /// <param name="tenantId">The ID of the tenant whose display picture is to be retrieved.</param>
        /// <returns>A response containing the display picture image if found.</returns>
        /// <response code="200">Returns the display picture image if found.</response>
        /// <response code="204">Returns an empty image response if not found.</response>
        [HttpGet("display-picture/{tenantId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> GetTenantDisplayPicture(int tenantId)
        {
            try
            {
                _trackingService.TrackRequest($"uri: [GET]tenant/display-picture/{tenantId} - request: {tenantId}");
                var tenantDisplayPicture = await _mediator.Send(new GetDisplayPictureRequest { Id = tenantId });
                var imageBytes = Convert.FromBase64String(tenantDisplayPicture.Base64Value);
                return File(imageBytes, tenantDisplayPicture.Type);
            }
            catch (Exception ex)
            {
                _trackingService.TrackRequest($"uri: [GET]tenant/display-picture/{tenantId} - exception: {ex.Message}");
                return BadRequest(new BaseResponse(_trackingService.GetRequestId() ?? string.Empty,null!, ResponseType.Failed.ToString()));
            }
        }

        /// <summary>
        /// Deletes a tenant by ID.
        /// </summary>
        /// <param name="tenantId">The ID of the tenant to delete.</param>
        /// <returns>A response indicating the result of the operation.</returns>
        /// <response code="200">Returns success if the operation is successful.</response>
        /// <response code="400">Returns bad request if the operation fails.</response>
        [HttpDelete("delete/{tenantId}")]
        [ProducesResponseType(typeof(BaseResponse), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(BaseResponse), StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<BaseResponse>> DeleteTenant(int tenantId)
        {
            try
            {
                if (!IsEmsAdmin())
                {
                    return Unauthorized(new BaseResponse(null!, ResponseType.UserDontHaveAccess.ToString()));
                }
                
                _trackingService.TrackRequest($"uri: [DETELE]tenant/delete/{tenantId} - request: {tenantId}");

                var isSucceed = await _mediator.Send(new DeleteTenantRequest { Id = tenantId });
                if (!isSucceed)
                    return BadRequest(new BaseResponse(_trackingService.GetRequestId() ?? string.Empty, null!,
                        ResponseType.Failed.ToString()));
                
                var response = new BaseResponse(_trackingService.GetRequestId() ?? string.Empty, null!,
                    ResponseType.Success.ToString());
                var responseBodyJson = JsonConvert.SerializeObject(response);
                
                _trackingService.TrackRequest($"uri: [DELETE]tenant/delete/{tenantId} - response: {responseBodyJson}");
                return Ok(response);
            }
            catch (Exception ex)
            {
                _trackingService.TrackRequest($"uri: [DELETE]tenant/delete/{tenantId} - exception: {ex.Message}");
                return BadRequest(new BaseResponse(_trackingService.GetRequestId() ?? string.Empty,null!, ResponseType.Failed.ToString()));
            }
        }

        /// <summary>
        /// Deactivates a tenant by ID.
        /// </summary>
        /// <param name="tenantId">The ID of the tenant to deactivate.</param>
        /// <returns>A response indicating the result of the operation.</returns>
        /// <response code="200">Returns success if the operation is successful.</response>
        /// <response code="400">Returns bad request if the operation fails.</response>
        [HttpDelete("deactivate/{tenantId}")]
        [ProducesResponseType(typeof(BaseResponse), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(BaseResponse), StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<BaseResponse>> DeactivateTenant(int tenantId)
        {
            try
            {
                if (!IsEmsAdmin())
                {
                    return Unauthorized(new BaseResponse(null!, ResponseType.UserDontHaveAccess.ToString()));
                }
                
                _trackingService.TrackRequest($"uri: [DELETE]tenant/deactivate/{tenantId} - request: {tenantId}");

                var isSucceed = await _mediator.Send(new DeactivateTenantRequest { Id = tenantId });

                if (!isSucceed)
                    return BadRequest(new BaseResponse(_trackingService.GetRequestId() ?? string.Empty, null!,
                        ResponseType.Failed.ToString()));
                
                var response = new BaseResponse(_trackingService.GetRequestId() ?? string.Empty, null!,
                    ResponseType.Success.ToString());
                var responseBodyJson = JsonConvert.SerializeObject(response);

                _trackingService.TrackRequest($"uri: [DELETE]tenant/deactivate/{tenantId} - response: {responseBodyJson}");
                return Ok(response);
            }
            catch (Exception ex)
            {
                _trackingService.TrackRequest($"uri: [DELETE]tenant/deactivate/{tenantId} - exception: {ex.Message}");
                return BadRequest(new BaseResponse(_trackingService.GetRequestId() ?? string.Empty,null!, ResponseType.Failed.ToString()));
            }
        }

        /// <summary>
        /// Update a tenant.
        /// </summary>
        /// <param name="updateTenantRequest">The request to update a tenant.</param>
        /// <returns>Status code of successful completion of the request.</returns>
        /// <response code="200">Returns the base response for updated Tenant</response>
        [HttpPut("update")]
        [ProducesResponseType(typeof(BaseResponse), StatusCodes.Status201Created)]
        [ProducesResponseType(typeof(BaseResponse), StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(typeof(BaseResponse), StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<BaseResponse>> UpdateTenant([FromForm] UpdateTenantRequest updateTenantRequest)
        {
            try
            {
                if (!IsEmsAdmin())
                {
                    return Unauthorized(new BaseResponse(null!, ResponseType.UserDontHaveAccess.ToString()));
                }
                
                // Serialize the updateTenantRequest object to JSON for logging.
                var requestBodyJson = JsonConvert.SerializeObject(updateTenantRequest);
                _trackingService.TrackRequest($"uri: [PUT]tenant/update/ - request: {requestBodyJson}");

                var tenantId = await _mediator.Send(updateTenantRequest);
                var response = new BaseResponse(_trackingService.GetRequestId() ?? string.Empty, tenantId,
                    ResponseType.Success.ToString());
                
                var responseBodyJson = JsonConvert.SerializeObject(response);
                _trackingService.TrackRequest($"uri: [PUT]tenant/update/ - response: {responseBodyJson}");
                return Ok(response);
            }
            catch (Exception ex)
            {
                _trackingService.TrackRequest($"uri: [PUT]tenant/update - exception: {ex.Message}");
                return BadRequest(new BaseResponse(_trackingService.GetRequestId() ?? string.Empty,null!, ResponseType.Failed.ToString()));
            }
        }
    }
}
