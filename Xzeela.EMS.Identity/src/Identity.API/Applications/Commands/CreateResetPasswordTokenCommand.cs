using Identity.API.Models.Requests;
using Identity.API.Services;
using Identity.Domain.Entities;
using Identity.Domain.Exceptions;
using MediatR;
using Microsoft.AspNetCore.Identity;

namespace Identity.API.Applications.Commands;

public class CreateResetPasswordTokenCommand : IRequestHandler<CreateResetPasswordTokenRequest, bool>
{
    private readonly UserManager<AppUser> _userManager;
    private readonly IEmailService _emailService;
    private readonly string? _frontEndURL;

    public CreateResetPasswordTokenCommand(
        UserManager<AppUser> userManager,
        IEmailService emailService)
    {
        _userManager = userManager;
        _emailService = emailService;
        _frontEndURL = Environment.GetEnvironmentVariable("FRONTEND_URL");
    }
    public async Task<bool> Handle(CreateResetPasswordTokenRequest request, CancellationToken cancellationToken)
    {
        var user = await _userManager.FindByEmailAsync(request.Email);

        if (user == null)
            throw new AppUserDomainException("Unable to find the user");
        
        var token = await _userManager.GeneratePasswordResetTokenAsync(user);
         
        var callback = $"{_frontEndURL}/reset-password/{user.Email}/{token}";
        _ = _emailService.
            SendTemplatelessEmail(user.Email, "Xzeela - EMS : Reset Password",  $"Click the  <a href='{callback}'> link </a> to set the password <br><br> Thanks and Regards,<br>Admin.");
        
        return true;
    }
}