using System.ComponentModel.DataAnnotations;
using Identity.API.Models.Requests;
using Identity.Domain.Exceptions;
using Identity.Infrastructure.Interfaces;
using MediatR;
using Xzeela.EMS.Shared.Services;

namespace Identity.API.Applications.Commands;

public class DeactivateTenantCommand : IRequestHandler<DeactivateTenantRequest, bool>
{
    private readonly IRequestTrackingService _trackingService;
    private readonly IUserRepository _userRepository;
    private readonly ITenantRepository _tenantRepository;
    
    public DeactivateTenantCommand(
        IRequestTrackingService trackingService,
        ITenantRepository tenantRepository,
        IUserRepository userRepository
    )
    {
        _trackingService = trackingService;
        _userRepository = userRepository;
        _tenantRepository = tenantRepository;
    }
    public async Task<bool> Handle(DeactivateTenantRequest request, CancellationToken cancellationToken)
    {
        
        var tenant = await _tenantRepository.GetByIdAsync(request.Id);

        if (tenant == null) throw new TenantDomainException("Unable to find the tenant");
        
        var users = await _userRepository.FindUsersByTenant(tenant.Id);

        var requestedUserId = _trackingService.GetRequestedUserId();
      
        foreach (var user in users)
        {
            if (!string.IsNullOrWhiteSpace(requestedUserId))
            {
                user.LastModifiedBy = Convert.ToDouble(requestedUserId);
            }
            user.IsActivated = false;
            _ = _userRepository.UpdateAsync(user);
        }

        if (!string.IsNullOrWhiteSpace(requestedUserId))
        {
            tenant.LastModifiedBy = Convert.ToDouble(requestedUserId);
        }
        
        tenant.IsActivated = false;
        _ = _tenantRepository.UpdateAsync(tenant);

        return true;
    }
}