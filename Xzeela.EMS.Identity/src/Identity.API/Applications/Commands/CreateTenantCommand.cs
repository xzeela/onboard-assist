using System.ComponentModel.DataAnnotations;
using AutoMapper;
using Identity.API.Models.Requests;
using Identity.API.Services;
using Identity.API.Utility;
using Identity.API.Validators;
using Identity.Domain.Entities;
using Identity.Domain.Enums;
using Identity.Domain.Exceptions;
using Identity.Infrastructure.Interfaces;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Xzeela.EMS.Shared.Services;

namespace Identity.API.Applications.Commands;



public class CreateTenantCommand : IRequestHandler<CreateTenantRequest, string>
{
    private readonly IRequestTrackingService _trackingService;
    private readonly ITenantRepository _tenantRepository;
    private readonly IMapper _mapper;
    private readonly RoleManager<Role> _roleManager;
    private readonly UserManager<AppUser> _userManager;
    private readonly IEmailService _emailService;
    private readonly string? _frontEndURL;

    public CreateTenantCommand(
        IRequestTrackingService trackingService,
        ITenantRepository tenantRepository,
        IMapper mapper,
        RoleManager<Role> roleManager,
        UserManager<AppUser> userManager,
        IEmailService emailService)
    {
        _userManager = userManager;
        _roleManager = roleManager;
        _tenantRepository = tenantRepository;
        _trackingService = trackingService;
        _emailService = emailService;
        _mapper = mapper;
        _frontEndURL = Environment.GetEnvironmentVariable("FRONTEND_URL");
    }
    public async Task<string> Handle(CreateTenantRequest request, CancellationToken cancellationToken)
    {
        var requestedUserId =  _trackingService.GetRequestedUserId();

        if (requestedUserId == null)
            throw new UnauthorizedAccessException(ResponseType.UserDontHaveAccess.ToString());
        
        DisplayPicture? displayPicture = null;

        if (await _tenantRepository.CheckNameAvailability(request.Name))
            throw new TenantDomainException("Tenant Name Already Taken");

        if (await _tenantRepository.CheckEmailAvailability(request.Email))
            throw new TenantDomainException("Email Name Already Taken");

        var tenantUser = await _userManager.FindByEmailAsync(request.Email);

        if (tenantUser != null)
            throw new AppUserDomainException("Email is already associate with user");
        
        var tenant = _mapper.Map<Tenant>(request);
        tenant.IsActivated = true;
        tenant.CreatedOn = DateTime.UtcNow;
        tenant.CreatedBy = Convert.ToDouble(requestedUserId);
        tenant.LastModifiedBy = Convert.ToDouble(requestedUserId);
        tenant.LastModifiedOn = DateTime.UtcNow;
            
        if(request.DisplayPictureFile != null)
        {
            displayPicture = new DisplayPicture()
            {
                Name = request.DisplayPictureFile.Name,
                Type = request.DisplayPictureFile.ContentType,
                Size = request.DisplayPictureFile.Length.ToString(),
                CreatedOn = DateTime.UtcNow,
                CreatedBy = Convert.ToDouble(requestedUserId),
                LastModifiedBy = Convert.ToDouble(requestedUserId),
                LastModifiedOn = DateTime.UtcNow,
            };

            using var ms = new MemoryStream();
            await request.DisplayPictureFile.CopyToAsync(ms, cancellationToken);
            displayPicture.Base64Value = Convert.ToBase64String(ms.ToArray(), 0, ms.ToArray().Length);

            tenant.DisplayPicture = displayPicture;
        }

        var tenantResponse = await _tenantRepository.AddAsync(tenant);

        //after creating a tenant create a tenant admin user and role TENANT_ADMIN for that tenant
        tenantUser = new AppUser()
        {
            Name = request.Name,
            UserName = request.Email,
            Email = request.Email,
            UserId = "0", //for tenant admin we keep the record as 0 (Tenant admin is not involve in transactions)
            DisplayPicture = displayPicture,
            Tenant = tenantResponse,
            CreatedOn = DateTime.UtcNow,
            IsActivated = true,
            CreatedBy = Convert.ToDouble(requestedUserId),
            LastModifiedBy = Convert.ToDouble(requestedUserId),
            LastModifiedOn = DateTime.UtcNow
        };
        
        var createUserResult = await _userManager.CreateAsync(tenantUser, Common.CreatePassword(9));

        if (createUserResult.Succeeded)
        {
            var role = new Role()
            {
                Name = $"{tenantUser.Name.ToUpper()}_TENANT_ADMIN",
                CreatedOn = DateTime.UtcNow,
                IsActivated = true,
                IsTenantAdmin = true,
                Tenant = tenantResponse,
                CreatedBy = Convert.ToDouble(requestedUserId),
                LastModifiedBy = Convert.ToDouble(requestedUserId),
                LastModifiedOn = DateTime.UtcNow
            };

            foreach (var module in request.Modules)
            {
                role.Permissions.Add(new Permission
                {
                    Module = module,
                    PermissionType = PermissionType.Edit,
                    IsEnabled = true
                });
            }
            
            var createRoleResult = await _roleManager.CreateAsync(role);

            if (createRoleResult.Succeeded)
            {
                _ = await _userManager.AddToRoleAsync(tenantUser, role.Name);
                
                //Send the email to user to reset the password 
                var token = await _userManager.GeneratePasswordResetTokenAsync(tenantUser);
                         
                var callback = $"{_frontEndURL}/reset-password/{tenantUser.Email}/{token}";
                _ = _emailService.
                    SendTemplatelessEmail(tenantUser.Email, "Xzeela - EMS : Account Created",  $"Click the  <a href='{callback}'> link </a> to set the password <br><br> Thanks and Regards,<br>Admin.");

                return tenantResponse.Id.ToString();

            }

            _ = _userManager.DeleteAsync(tenantUser);
            _ = _tenantRepository.DeleteAsync(tenant);
            throw new RoleDomainException($"Unable to create tenant admin role - {createRoleResult.Errors.First().Description}");
        }

        //If unable to create the user for the tenant role back the tenant that created
        _ = _tenantRepository.DeleteAsync(tenant);
        throw new AppUserDomainException($"Unable to create the user account for tenant - {createUserResult.Errors.First().Description}");
    }
}