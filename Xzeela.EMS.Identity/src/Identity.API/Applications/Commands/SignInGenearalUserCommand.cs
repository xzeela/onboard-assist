using System.ComponentModel.DataAnnotations;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using AutoMapper;
using Identity.API.Models.Requests;
using Identity.API.Models.Responses;
using Identity.API.Utility;
using Identity.Domain.Entities;
using Identity.Domain.Enums;
using Identity.Domain.Exceptions;
using Identity.Infrastructure.Interfaces;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Xzeela.EMS.Shared.API.Models;
using Xzeela.EMS.Shared.Services;

namespace Identity.API.Applications.Commands;

public class SignInGenearalUserCommand : IRequestHandler<GeneralUserSignInRequest, BaseResponse>
{
    private readonly UserManager<AppUser> _userManager;
    private readonly IRequestTrackingService _trackingService;
    private readonly IMapper _mapper;
    private readonly JwtSetting _jwtSettings;
    private readonly IUserRepository _userRepository;
    public SignInGenearalUserCommand(
        IRequestTrackingService trackingService,
        IMapper mapper,
        UserManager<AppUser> userManager,
        IConfiguration configuration,
        IUserRepository userRepository
    )
    {
        _userManager = userManager;
        _trackingService = trackingService;
        _mapper = mapper;
        _userRepository = userRepository;
        _jwtSettings = configuration.GetSection("Jwt").Get<JwtSetting>();
    }

    public async Task<BaseResponse> Handle(GeneralUserSignInRequest request, CancellationToken cancellationToken)
    {
        var signInResponse = new SignInResponse();
        
        var user = await _userManager.Users.FirstOrDefaultAsync(u => u.UserName == request.Email, cancellationToken: cancellationToken);
        if (user is null)
            throw new AppUserDomainException("Unable to find the user to sign in");
            
        var userSigninResult = await _userManager.CheckPasswordAsync(user, request.Password);
        if (!userSigninResult)
            throw new AppUserDomainException("Email or Password entered is incorrect");

        var roles = await _userManager.GetRolesAsync(user);

        var tenant = await _userRepository.GetTenantByUserId(user.Id);      
        var tenantResponse = _mapper.Map<TenantResponse>(tenant);
        
        if(tenantResponse.IsActivated == false) 
            throw new TenantDomainException("Tenant is Inactive");
        
        if(tenantResponse != null)
        {
            tenantResponse.LogoUrl = $"{_trackingService.GetBaseURL()}api/v1/xzeela/Tenant/display-picture/{tenantResponse.Id}";
        }
        signInResponse.Tenant = tenantResponse;
        
        var claims = new List<Claim>
        {
            new("Id", user.Id.ToString()),
            new("Email", user.UserName),
            new("TenantId", user.Tenant.Id.ToString())

        };
        
        if (!roles.Any())
        {
            throw new AppUserDomainException("User does not have any roles assigns");
        }

        var role = roles.FirstOrDefault();
        claims.Add(new Claim("role", role ?? ""));

        claims.Add(role != null && role.ToLower().Contains("tenant_admin")
            ? new Claim("PrivilegeType", PrivilegeType.TenantAdmin.ToString())
            : new Claim("PrivilegeType", PrivilegeType.GeneralUser.ToString()));

        var tokenHandler = new JwtSecurityTokenHandler();
        var tokenKey = Encoding.UTF8.GetBytes(_jwtSettings.Secret);
        var tokenDescriptor = new SecurityTokenDescriptor
         {
             Subject = new ClaimsIdentity(claims),
             Expires = DateTime.UtcNow.AddHours(2),
             SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(tokenKey), SecurityAlgorithms.HmacSha256Signature),
             Issuer = _jwtSettings.Issuer,
             Audience = _jwtSettings.Audience
         };

        var token = tokenHandler.CreateToken(tokenDescriptor);
    
        if(token is null)  throw new AppUserDomainException("Unable to Authenticate User");

        signInResponse.Token = tokenHandler.WriteToken(token);   
        
        return new BaseResponse(signInResponse, ResponseType.Success.ToString());
    }
}