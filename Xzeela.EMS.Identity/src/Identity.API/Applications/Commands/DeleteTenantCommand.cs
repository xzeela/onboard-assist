using Identity.API.Models.Requests;
using Identity.Domain.Exceptions;
using Identity.Infrastructure.Interfaces;
using MediatR;
using Xzeela.EMS.Shared.Services;

namespace Identity.API.Applications.Commands;

public class DeleteTenantCommand : IRequestHandler<DeleteTenantRequest, bool>
{
    private readonly IRequestTrackingService _trackingService;
    private readonly IUserRepository _userRepository;
    private readonly ITenantRepository _tenantRepository;
    
    public DeleteTenantCommand(
        IRequestTrackingService trackingService,
        ITenantRepository tenantRepository,
        IUserRepository userRepository
    )
    {
        _trackingService = trackingService;
        _userRepository = userRepository;
        _tenantRepository = tenantRepository;
    }
    
    public async Task<bool> Handle(DeleteTenantRequest request, CancellationToken cancellationToken)
    {
        var tenant = await _tenantRepository.GetByIdAsync(request.Id);

        if (tenant == null) throw new TenantDomainException("Unable to find the tenant");

        if (tenant.IsActivated)
            throw new ArgumentException("Cannot Delete Tenant while Active - Deactivate and try again");
        
        var users = await _userRepository.FindUsersByTenant(tenant.Id);
        
        var isNotDeactivateUsersAvailable = users.Any(x => x.IsActivated);
        if (isNotDeactivateUsersAvailable)
            throw new ArgumentException(
                "Tenant have already Active Users - Deactivate All users for tenant and try again");
        
        var requestedUserId = _trackingService.GetRequestedUserId();
     
        foreach (var user in users)
        {
            if (!string.IsNullOrWhiteSpace(requestedUserId))
            {
                user.CreatedBy = Convert.ToDouble(requestedUserId);
            }
            user.IsDeleted = true;
            _ = _userRepository.UpdateAsync(user);
        }

        if (!string.IsNullOrWhiteSpace(requestedUserId))
        {
            tenant.CreatedBy = Convert.ToDouble(requestedUserId);
        }
        tenant.IsDeleted = true;
        _ = _tenantRepository.UpdateAsync(tenant);
        
        return true;
    }
}