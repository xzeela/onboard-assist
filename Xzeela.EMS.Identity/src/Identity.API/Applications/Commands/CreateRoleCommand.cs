﻿using Identity.API.Models.Requests;
using Identity.Domain.Entities;
using Identity.Domain.Enums;
using Identity.Domain.Exceptions;
using Identity.Infrastructure.Interfaces;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Xzeela.EMS.Shared.Services;

namespace Identity.API.Applications.Commands;

public class CreateRoleCommand : IRequestHandler<CreateRoleRequest, string>
{
    private readonly RoleManager<Role> _roleManager;
    private readonly IRequestTrackingService _trackingService;
    private readonly ITenantRepository _tenantRepository;
    private readonly IRoleRepository _roleRepository;
    private readonly IUserRepository _userRepository;
    public CreateRoleCommand(
        IRequestTrackingService trackingService,
        RoleManager<Role> roleManager,
        ITenantRepository tenantRepository,
        IRoleRepository roleRepository,
        IUserRepository userRepository
        )
    {
        _roleManager = roleManager;
        _userRepository = userRepository;
        _tenantRepository = tenantRepository;
        _trackingService = trackingService;
        _roleRepository = roleRepository;
    }
    
    public async Task<string> Handle(CreateRoleRequest request, CancellationToken cancellationToken)
    {
        var requestedUserId =  _trackingService.GetRequestedUserId();

        if (requestedUserId == null)
            throw new UnauthorizedAccessException(ResponseType.UserDontHaveAccess.ToString());

        var tenant = await _tenantRepository.GetTenantByTenantKey(request.TenantKey);
        var requestedUserTenant = await _userRepository.GetTenantByUserId(Convert.ToInt32(requestedUserId));
        if (tenant == null)
            throw new TenantDomainException("Invalid tenant key");

        //additional layer of to check ensure tenant id of requested user and tenant is matching
        if(tenant.Id != requestedUserTenant?.Id)
            throw new TenantDomainException("Requested user and Tenant mismatch");

        var role = await _roleRepository.GetRoleByTenantAndName(tenant.Id, request.Name);

        if (role != null) throw new BadHttpRequestException("Role name already exist");
        
        if(request.Permissions.Count <= 0) throw new BadHttpRequestException("To create a role at least one permission is required");

        role = new Role()
        {
            Name = request.Name,
            Tenant = tenant,
            IsActivated = true,
            CreatedBy = Convert.ToDouble(requestedUserId),
            CreatedOn = DateTime.UtcNow,
            LastModifiedBy = Convert.ToDouble(requestedUserId),
            LastModifiedOn = DateTime.UtcNow
        };

        foreach (var permission in request.Permissions.Where(permission => tenant.Modules.Contains(permission.Module)))
        {
            role.Permissions.Add(permission);
        }
        
        var createRoleResponse = await _roleManager.CreateAsync(role);

        if (createRoleResponse.Succeeded)
        {
            return role.Id.ToString();
        }

        throw new RoleDomainException($"Unable to create role - {createRoleResponse.Errors.First().Description}");
    }
}

