﻿using Identity.API.Models.Requests;
using Identity.API.Models.Responses;
using Identity.Domain.Entities;
using Identity.Domain.Exceptions;
using Identity.Infrastructure.Interfaces;
using MediatR;
using Xzeela.EMS.Shared.Services;

namespace Identity.API.Applications.Commands;

public class UpdateTenantCommand : IRequestHandler<UpdateTenantRequest, TenantResponse>
{
    private readonly IRequestTrackingService _trackingService;
    private readonly ITenantRepository _tenantRepository;
    private readonly IRoleRepository _roleRepository;
    public UpdateTenantCommand(
        IRequestTrackingService trackingService,
        ITenantRepository tenantRepository,
        IRoleRepository roleRepository
        )
    {
        _roleRepository = roleRepository;
        _tenantRepository = tenantRepository;
        _trackingService = trackingService;
    }
    public async Task<TenantResponse> Handle(UpdateTenantRequest request, CancellationToken cancellationToken)
    {
        var tenant = await _tenantRepository.GetByIdAsync(request.Id);

        if (tenant == null)
            throw new TenantDomainException("Unable to find the tenant to update");

        //Update the name of the tenant
        if (request.Name != null && await _tenantRepository.CheckNameAvailability(request.Name))
        {
            tenant.Name = request.Name;
        }        
        
        //Update the display picture for the tenant
        if(request.DisplayPictureFile != null)
        {
            tenant.DisplayPicture = new DisplayPicture()
            {
                Name = request.DisplayPictureFile.Name,
                Type = request.DisplayPictureFile.ContentType,
                Size = request.DisplayPictureFile.Length.ToString(),
                CreatedOn = DateTime.UtcNow
            };

            using var ms = new MemoryStream();
            await request.DisplayPictureFile.CopyToAsync(ms, cancellationToken);
            tenant.DisplayPicture.Base64Value = Convert.ToBase64String(ms.ToArray(), 0, ms.ToArray().Length);
        }
        
        //Update the remarks of the tenant
        if (request.Remarks != null)
        {
            tenant.Remarks = request.Remarks;
        }
        
        //Update the Modules of the tenant
        if (!request.Modules.Any())
            return new TenantResponse
            {
                Id = tenant.Id,
                Name = tenant.Name,
                LogoUrl = $"{_trackingService.GetBaseURL()}api/v1/xzeela/Tenant/display-picture/{tenant.Id}"
            };
        
        tenant.Modules.Clear();
        tenant.Modules = request.Modules;

        var roles = await _roleRepository.GetRolesByTenant(request.Id);

        foreach (var role in roles)
        {
            var tempPermissions = role.Permissions.Where(permission => tenant.Modules.Contains(permission.Module)).ToList();
            role.Permissions = tempPermissions;
            await _roleRepository.UpdateAsync(role);
        }
        
        return new TenantResponse
        {
            Id = tenant.Id,
            Name = tenant.Name,
            Key = tenant.Key,
            LogoUrl = $"{_trackingService.GetBaseURL()}api/v1/xzeela/Tenant/display-picture/{tenant.Id}"
        };
    }
}
