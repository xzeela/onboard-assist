using System.ComponentModel.DataAnnotations;
using AutoMapper;
using Identity.API.Models.Requests;
using Identity.API.Services;
using Identity.Domain.Entities;
using Identity.Domain.Exceptions;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Xzeela.EMS.Shared.Services;

namespace Identity.API.Applications.Commands;

public class ResetPasswordCommand : IRequestHandler<ResetPasswordRequest, bool>
{
    private readonly UserManager<AppUser> _userManager;
    public ResetPasswordCommand(
        UserManager<AppUser> userManager
    )
    {
        _userManager = userManager;
    }
    public async Task<bool> Handle(ResetPasswordRequest request, CancellationToken cancellationToken)
    {
        var user = await _userManager.FindByEmailAsync(request.Email);
        if (user == null) throw new AppUserDomainException("Unable to find the user");
        
        if (request.ConfirmPassword != request.Password)
        {
            throw new InvalidDataException("Confirmation password do not match");
        }
        
        if (!request.Password.Any(char.IsUpper) && !request.Password.Any(char.IsSymbol)
                                                      && !request.Password.Any(char.IsLower) && request.Password.Length < 8)
        {
            throw new InvalidDataException("Password should contains at lest one Upper Case " +
                                           "one Lower Case and Special charter with a number with minimum 8 characters");
        }
        if (await _userManager.CheckPasswordAsync(user, request.Password))
        {
            throw new InvalidDataException("Your new password cannot be the same as your old password");
        }
        
        var resetPassResult = await _userManager.ResetPasswordAsync(user, request.Token, request.Password);
        return resetPassResult.Succeeded;
    }
}