using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Identity.API.Models.Requests;
using Identity.API.Models.Responses;
using Identity.API.Utility;
using Identity.Domain.Enums;
using Identity.Infrastructure.Interfaces;
using MediatR;
using Microsoft.IdentityModel.Tokens;
using Xzeela.EMS.Shared.API.Models;

namespace Identity.API.Applications.Commands;

public class SignInSuperAdminCommand : IRequestHandler<SuperAdminSignInRequest, BaseResponse>
{
    private readonly ISuperAdminRepository _superAdminRepository;
    private readonly JwtSetting _jwtSettings;
    
    public SignInSuperAdminCommand(
        ISuperAdminRepository superAdminRepository,
        IConfiguration configuration)
    {
        _superAdminRepository = superAdminRepository;
        _jwtSettings = configuration.GetSection("Jwt").Get<JwtSetting>();
    }
    
    public async Task<BaseResponse> Handle(SuperAdminSignInRequest request, CancellationToken cancellationToken)
    {
        var baseResponse = new BaseResponse(null!, string.Empty);

        var superAdmin = await _superAdminRepository.GetSuperAdminByEmail(request.Email);

        if (superAdmin == null || !string.Equals(superAdmin?.Password, request.Password, StringComparison.CurrentCultureIgnoreCase))
        {
            baseResponse.Message = ResponseType.Failed.ToString();
            return baseResponse;
        }

        var signInResponse = new SignInResponse();

        var claims = new List<Claim>
        {            
            new("Id", superAdmin?.Id.ToString()),
            new("Email", superAdmin?.Email),
            new("PrivilegeType", PrivilegeType.EmsAdmin.ToString())
        };
        
        var tokenHandler = new JwtSecurityTokenHandler();
        var tokenKey = Encoding.UTF8.GetBytes(_jwtSettings.Secret);
        var tokenDescriptor = new SecurityTokenDescriptor
        {
            Subject = new ClaimsIdentity(claims),
            Expires = DateTime.UtcNow.AddHours(6),
            SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(tokenKey), SecurityAlgorithms.HmacSha256Signature),
            Issuer = _jwtSettings.Issuer,
            Audience = _jwtSettings.Audience
        };

        var token = tokenHandler.CreateToken(tokenDescriptor);
        signInResponse.Token = tokenHandler.WriteToken(token);

        baseResponse.Result = signInResponse;
        baseResponse.Message = ResponseType.Success.ToString();
        
        return baseResponse;
    }
}