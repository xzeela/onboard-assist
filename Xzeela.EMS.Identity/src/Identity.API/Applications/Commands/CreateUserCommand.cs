using AutoMapper;
using Identity.API.Models.Requests;
using Identity.API.Services;
using Identity.API.Utility;
using Identity.Domain.Entities;
using Identity.Domain.Exceptions;
using Identity.Infrastructure.Interfaces;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Xzeela.EMS.Shared.API.Models;
using Xzeela.EMS.Shared.Services;

namespace Identity.API.Applications.Commands;

public class CreateUserCommand : IRequestHandler<CreateUserRequest, string>
{
    private readonly ITenantRepository _tenantRepository;
    private readonly RoleManager<Role> _roleManager;
    private readonly UserManager<AppUser> _userManager;
    private readonly IEmailService _emailService;
    private readonly string? _frontEndURL;

    public CreateUserCommand(
        ITenantRepository tenantRepository,
        RoleManager<Role> roleManager,
        UserManager<AppUser> userManager,
        IEmailService emailService)
    {
        _userManager = userManager;
        _roleManager = roleManager;
        _tenantRepository = tenantRepository;
        _emailService = emailService;
        _frontEndURL = Environment.GetEnvironmentVariable("FRONTEND_URL");

    }
    public async Task<string> Handle(CreateUserRequest request, CancellationToken cancellationToken)
    {
        DisplayPicture? displayPicture = null;
        
        var tenant = await _tenantRepository.GetByIdAsync(request.TenantId);
        
        if (tenant is not { IsActivated: true })
            throw new TenantDomainException("Unable to Identify the Tenant");

        var user = await _userManager.FindByEmailAsync(request.Email);
        if (user != null)
            throw new AppUserDomainException("Email is already taken");

        var role = await _roleManager.FindByNameAsync($"{tenant.Name.ToUpper()}_GENERAL_USER");
        
        if (role == null)
        {
            var newRole = new Role()
            {
                Name = $"{tenant.Name.ToUpper()}_GENERAL_USER",
                CreatedOn = DateTime.UtcNow,
                IsActivated = true,
                IsTenantAdmin = true,
                Tenant = tenant,
                CreatedBy = Convert.ToDouble(request.RequestedUserId),
                LastModifiedBy = Convert.ToDouble(request.RequestedUserId),
                LastModifiedOn = DateTime.UtcNow
            };
            
            var createRoleResult = await _roleManager.CreateAsync(newRole);

            if (!createRoleResult.Succeeded)
                throw new RoleDomainException($"Unable to create role - {createRoleResult.Errors.First().Description}");

            role = newRole;
        }
        
        if(request.DisplayPictureFile != null)
        {
            displayPicture = new DisplayPicture()
            {
                Name = request.DisplayPictureFile.Name,
                Type = request.DisplayPictureFile.ContentType,
                Size = request.DisplayPictureFile.Length.ToString(),
                CreatedOn = DateTime.UtcNow,
                CreatedBy = Convert.ToDouble(request.RequestedUserId),
                LastModifiedBy = Convert.ToDouble(request.RequestedUserId),
                LastModifiedOn = DateTime.UtcNow,
            };

            using var ms = new MemoryStream();
            await request.DisplayPictureFile.CopyToAsync(ms, cancellationToken);
            displayPicture.Base64Value = Convert.ToBase64String(ms.ToArray(), 0, ms.ToArray().Length);
        }
        
        var appUser = new AppUser()
        {
            Name = request.Name,
            UserName = request.Email,
            Email = request.Email,
            UserId = request.UserId,
            DisplayPicture = displayPicture,
            Tenant = tenant,
            CreatedOn = DateTime.UtcNow,
            IsActivated = true,
            CreatedBy = Convert.ToDouble(request.RequestedUserId),
            LastModifiedBy = Convert.ToDouble(request.RequestedUserId),
            LastModifiedOn = DateTime.UtcNow
        };
        
        var createUserResult = await _userManager.CreateAsync(appUser, Common.CreatePassword(9));

        if (!createUserResult.Succeeded)
            throw new RoleDomainException($"Unable to create the user account - {createUserResult.Errors.First().Description}");

        var addToRoleResult = await _userManager.AddToRoleAsync(appUser, role.Name);

        if(!addToRoleResult.Succeeded)
            throw new RoleDomainException($"Unable to add the user to role - {addToRoleResult.Errors.First().Description}");

        //Send the email to user to reset the password 
        var token = await _userManager.GeneratePasswordResetTokenAsync(appUser);
        
        var callback = $"{_frontEndURL}/reset-password/{appUser.Email}/{token}";

        _ = _emailService.
            SendTemplatelessEmail(appUser.Email, "Xzeela - EMS : Account Created",  $"Click the  <a href='{callback}'> link </a> to set the password <br><br> Thanks and Regards,<br>Admin.");
        
        return appUser.Id.ToString();   
    }
}