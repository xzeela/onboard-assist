﻿using System;
using AutoMapper;
using Identity.API.Applications.Commands;
using Identity.API.Models.Responses;
using Identity.API.Services;
using Identity.Infrastructure.Interfaces;
using MediatR;
using Xzeela.EMS.Shared.Services;

namespace Identity.API.Applications.Queries;

public class GetAllTenantsQuery : IRequest<List<TenantResponse>>
{

}

public class GetAllTenantsQueryHandler : IRequestHandler<GetAllTenantsQuery, List<TenantResponse>?>
{
    private readonly ILogger<GetAllTenantsQueryHandler> _logger;
    private readonly IRequestTrackingService _trackingService;
    private readonly ITenantRepository _tenantRepository;
    private readonly IMapper _mapper;

    public GetAllTenantsQueryHandler(
       IRequestTrackingService trackingService,
       ILogger<GetAllTenantsQueryHandler> logger,
       ITenantRepository tenantRepository,
       IMapper mapper)
    {
        _tenantRepository = tenantRepository;
        _trackingService = trackingService;
        _logger = logger;
        _mapper = mapper;
    }

    public async Task<List<TenantResponse>?> Handle(GetAllTenantsQuery request, CancellationToken cancellationToken)
    {
        var tenants = await _tenantRepository.ListAllAsync();
        var tenantResponseList = _mapper.Map<List<TenantResponse>>(tenants);

        if (tenantResponseList is not { Count: > 0 }) return tenantResponseList;
        foreach (var tenant in tenantResponseList)
        {
            tenant.LogoUrl = $"{_trackingService.GetBaseURL()}api/v1/xzeela/Tenant/display-picture/{tenant.Id}";
        }

        return tenantResponseList;
    }
}

