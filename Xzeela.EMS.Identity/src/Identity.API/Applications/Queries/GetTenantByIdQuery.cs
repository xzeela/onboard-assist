﻿using System;
using MediatR;
using System.ComponentModel.DataAnnotations;
using Identity.API.Models.Responses;
using AutoMapper;
using Identity.API.Applications.Commands;
using Identity.API.Models.Requests;
using Identity.API.Services;
using Identity.Infrastructure.Interfaces;
using Xzeela.EMS.Shared.Services;

namespace Identity.API.Applications.Queries;

public class GetTenantByIdQuery : IRequestHandler<GetTenantByIdRequest, TenantResponse?>
{
    private readonly IRequestTrackingService _trackingService;
    private readonly ITenantRepository _tenantRepository;
    private readonly IMapper _mapper;

    public GetTenantByIdQuery(
        IRequestTrackingService trackingService,
        ITenantRepository tenantRepository,
        IMapper mapper)
    {
        _tenantRepository = tenantRepository;
        _trackingService = trackingService;
        _mapper = mapper;
    }

    public async Task<TenantResponse?> Handle(GetTenantByIdRequest request, CancellationToken cancellationToken)
    {
        var tenant = await _tenantRepository.GetByIdAsync(request.Id);
        var tenantResponse = _mapper.Map<TenantResponse>(tenant);
        if(tenantResponse != null)
        {
            tenantResponse.LogoUrl = $"{_trackingService.GetBaseURL()}api/v1/xzeela/Tenant/display-picture/{tenantResponse.Id}";
        }
        return tenantResponse;
    }
}