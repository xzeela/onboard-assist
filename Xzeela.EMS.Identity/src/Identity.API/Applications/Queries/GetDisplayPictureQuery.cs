﻿using System;
using AutoMapper;
using Identity.API.Applications.Commands;
using Identity.API.Models.Requests;
using Identity.API.Services;
using Identity.Domain.Entities;
using Identity.Infrastructure.Interfaces;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Xzeela.EMS.Shared.Services;

namespace Identity.API.Applications.Queries;

public class GetDisplayPictureQuery : IRequestHandler<GetDisplayPictureRequest, DisplayPicture?>
{
    private readonly ITenantRepository _tenantRepository;
    public GetDisplayPictureQuery(
        ITenantRepository tenantRepository
        )
    {
        _tenantRepository = tenantRepository;
    }

    public async Task<DisplayPicture?> Handle(GetDisplayPictureRequest request, CancellationToken cancellationToken)
    {
        return await _tenantRepository.GetTenantDisplayPicture(request.Id);
    }
}

