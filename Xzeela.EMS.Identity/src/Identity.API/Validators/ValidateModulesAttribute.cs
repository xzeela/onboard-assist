﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Identity.API.Validators;

public class ValidateModulesAttribute : ValidationAttribute
{
    private readonly Type _enumType;

    public ValidateModulesAttribute(Type enumType)
    {
        _enumType = enumType;
    }

    protected override ValidationResult IsValid(object value, ValidationContext validationContext)
    {
        if (value is null)
        {
            return new ValidationResult("Modules list is required.");
        }

        if (value is not System.Collections.IEnumerable enumerable)
        {
            throw new InvalidOperationException("The property marked with [ValidateModules] must be an IEnumerable.");
        }

        var enumValues = Enum.GetValues(_enumType).Cast<object>();

        foreach (var module in enumerable)
        {
            if (!enumValues.Contains(module))
            {
                return new ValidationResult($"Invalid module value: {module}");
            }
        }

        if (!enumValues.Intersect(enumerable.Cast<object>()).Any())
        {
            return new ValidationResult("At least one valid module is required.");
        }

        return ValidationResult.Success;
    }
}

