﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Identity.API.Validators;

[AttributeUsage(AttributeTargets.Property)]
public class MaxFileSizeAttribute : ValidationAttribute
{
    private readonly long _maxBytes;

    public MaxFileSizeAttribute(long maxBytes)
    {
        _maxBytes = maxBytes;
    }

    protected override ValidationResult IsValid(object value, ValidationContext validationContext)
    {
        if (value is IFormFile file)
        {
            if (file.Length > _maxBytes)
            {
                return new ValidationResult($"The file size should not exceed {_maxBytes / (1024 * 1024)} MB.");
            }
        }

        return ValidationResult.Success;
    }
}

