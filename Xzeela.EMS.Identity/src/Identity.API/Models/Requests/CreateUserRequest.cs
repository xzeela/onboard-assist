using System.ComponentModel.DataAnnotations;
using Identity.API.Validators;
using MediatR;
using Xzeela.EMS.Shared.API.Models;

namespace Identity.API.Models.Requests;

public class CreateUserRequest : IRequest<string>
{
    /// <summary>
    /// Name of the Tenant
    /// </summary>
    [Required(ErrorMessage = "Name is required.")]
    [MaxLength(100, ErrorMessage = "Name cannot exceed 100 characters.")]
    [RegularExpression(@"^(?![0-9]*$)[a-zA-Z0-9\s]+$", ErrorMessage = "Name cannot contain only numbers.")]
    public string Name { get; set; } = null!;
    
    /// <summary>
    /// Email of the User.
    /// </summary>
    [Required(ErrorMessage = "Email is required.")]
    [MaxLength(100, ErrorMessage = "Email cannot exceed 100 characters.")]
    public string Email { get; set; } = null!;
    
    /// <summary>
    /// Display Picture of the User.
    /// Maximum Size is 1MB
    /// </summary>
    [MaxFileSize(1 * 1024 * 1024)] // 1MB in bytes
    public IFormFile? DisplayPictureFile { get; set; }

    public string UserId { get; set; } = null!;
    
    public int TenantId { get; set; }

    public int RequestedUserId { get; set; }
}