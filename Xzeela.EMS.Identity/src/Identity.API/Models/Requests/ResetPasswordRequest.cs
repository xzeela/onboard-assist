using System.ComponentModel.DataAnnotations;
using MediatR;

namespace Identity.API.Models.Requests;


/// <summary>
/// Represents a request to reset a user's password.
/// </summary>
public class ResetPasswordRequest : IRequest<bool>
{
    /// <summary>
    /// Gets or sets the new password.
    /// </summary>
    [Required(ErrorMessage = "Password is Required")]
    [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 8)]
    [RegularExpression(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*\W).+$", ErrorMessage = "Password should contain at least one uppercase letter, one lowercase letter, one special character, and one digit.")]
    public string Password { get; set; } = null!;

    /// <summary>
    /// Gets or sets the confirmation of the new password.
    /// </summary>
    [Required(ErrorMessage = "Password Confirm is Required")]
    [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
    public string ConfirmPassword { get; set; } = null!;

    /// <summary>
    /// Gets or sets the user's email address.
    /// </summary>
    [Required(ErrorMessage = "Email is Required")]
    [EmailAddress(ErrorMessage = "Invalid Email Address")]
    public string Email { get; set; } = null!;

    /// <summary>
    /// Gets or sets the password reset token.
    /// </summary>
    [Required(ErrorMessage = "Token is Required")]
    public string Token { get; set; } = null!;
}