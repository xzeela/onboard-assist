using System.ComponentModel.DataAnnotations;
using Identity.API.Validators;
using Identity.Domain.Enums;
using MediatR;

namespace Identity.API.Models.Requests;

public class CreateTenantRequest : IRequest<string>
{
    /// <summary>
    /// Name of the Tenant
    /// </summary>
    [Required(ErrorMessage = "Name is required.")]
    [MaxLength(100, ErrorMessage = "Name cannot exceed 100 characters.")]
    [RegularExpression(@"^(?![0-9]*$)[a-zA-Z0-9\s]+$", ErrorMessage = "Name cannot contain only numbers.")]
    public string Name { get; set; } = null!;

    /// <summary>
    /// Email of the Tenant. Billing and very transaction report is sent to this email.
    /// </summary>
    [Required(ErrorMessage = "Email is required.")]
    [MaxLength(100, ErrorMessage = "Email cannot exceed 100 characters.")]
    public string Email { get; set; } = null!;

    /// <summary>
    /// Display Picture of the Tenant. Display Picture Will be saved in the DB (Base64 String Format)
    /// Maximum Size is 1MB
    /// </summary>
    [MaxFileSize(1 * 1024 * 1024)] // 1MB in bytes
    public IFormFile? DisplayPictureFile { get; set; }
    
    /// <summary>
    /// Any Special Remarks that Tenant Can Hold. Not mandatory to have 
    /// </summary>
    public string? Remarks { get; set; }

    /// <summary>
    /// List of Modules that Tenant can hold
    /// When Creating a Tenant it is mandatory to have at least one module 
    /// </summary>
    [Required(ErrorMessage = "At least one module is required to create a Tenant.")]
    [ValidateModules(typeof(Module), ErrorMessage = "Invalid modules.")]
    public List<Module> Modules { get; set; } = new();
}