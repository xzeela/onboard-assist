using MediatR;
using Xzeela.EMS.Shared.API.Models;

namespace Identity.API.Models.Requests;

public class SuperAdminSignInRequest : IRequest<BaseResponse>
{
    public string Email { get; set; } = null!;
    public string Password { get; set; } = null!;
}