using Identity.Domain.Entities;
using MediatR;

namespace Identity.API.Models.Requests;

public class GetDisplayPictureRequest : IRequest<DisplayPicture>
{
    public int Id { get; set; }
}