using System.ComponentModel.DataAnnotations;
using Identity.API.Models.Responses;
using Identity.API.Validators;
using Identity.Domain.Enums;
using MediatR;

namespace Identity.API.Models.Requests;

public class UpdateTenantRequest : IRequest<TenantResponse>
{
    /// <summary>
    /// ID of the Tenant to identify whose values will be updating
    /// </summary>
    [Required(ErrorMessage = "Id is required")]
    public int Id { get; set; }
    
    /// <summary>
    /// Name of the Tenant
    /// </summary>
    [MaxLength(100, ErrorMessage = "Name cannot exceed 100 characters.")]
    [RegularExpression(@"^(?![0-9]*$)[a-zA-Z0-9\s]+$", ErrorMessage = "Name cannot contain only numbers.")]
    public string? Name { get; set; }

    /// <summary>
    /// Display Picture of the Tenant. Display Picture Will be saved in the DB (Base64 String Format)
    /// Maximum Size is 1MB
    /// </summary>
    [MaxFileSize(1 * 1024 * 1024)] // 1MB in bytes
    public IFormFile? DisplayPictureFile { get; set; }

    /// <summary>
    /// Any Special Remarks that Tenant Can Hold. Not mandatory to have 
    /// </summary>
    public string? Remarks { get; set; }

    /// <summary>
    /// List of Modules that Tenant can hold
    /// When Creating a Tenant it is mandatory to have at least one module 
    /// </summary>
    [ValidateModules(typeof(Module), ErrorMessage = "Invalid modules.")]
    public List<Module> Modules { get; set; } = new();
}
