using System.ComponentModel.DataAnnotations;
using MediatR;

namespace Identity.API.Models.Requests;

public class CreateResetPasswordTokenRequest : IRequest<bool>
{
    /// <summary>
    /// Email of the Tenant. Billing and very transaction report is sent to this email.
    /// </summary>
    [Required(ErrorMessage = "Email is required.")]
    [MaxLength(100, ErrorMessage = "Email cannot exceed 100 characters.")]
    public string Email { get; set; } = null!;
}
