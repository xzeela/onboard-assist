using MediatR;
using Xzeela.EMS.Shared.API.Models;

namespace Identity.API.Models.Requests;

public class GeneralUserSignInRequest : IRequest<BaseResponse>
{
    public string Email { get; set; } = null!;
    public string Password { get; set; } = null!;
}