using System.ComponentModel.DataAnnotations;
using Identity.API.Models.Responses;
using MediatR;

namespace Identity.API.Models.Requests;

public class GetTenantByIdRequest : IRequest<TenantResponse>
{
    [Required(ErrorMessage = "Tenant Id is required.")]
    public int Id { get; set; }
}