using System.ComponentModel.DataAnnotations;
using MediatR;

namespace Identity.API.Models.Requests;

public class DeleteTenantRequest : IRequest<bool>
{
    /// <summary>
    /// Id of the Tenant
    /// </summary>
    [Required(ErrorMessage = "Tenant Id is required.")]
    public int Id { get; set; }
}