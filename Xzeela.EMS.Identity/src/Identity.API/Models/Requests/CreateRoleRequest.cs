using System.ComponentModel.DataAnnotations;
using Identity.Domain.Entities;
using MediatR;

namespace Identity.API.Models.Requests;

/// <summary>
/// Represents a request to create a new role.
/// </summary>
public class CreateRoleRequest : IRequest<string>
{
    /// <summary>
    /// Gets or sets the name of the role.
    /// </summary>
    [Required(ErrorMessage = "Name is required.")]
    [MaxLength(100, ErrorMessage = "Name cannot exceed 100 characters.")]
    public string Name { get; set; } = null!;

    /// <summary>
    /// Gets or sets the list of permissions associated with the role.
    /// </summary>
    public List<Permission> Permissions { get; set; } = new();

    /// <summary>
    /// Gets or sets the Tenant ID associated with the role.
    /// </summary>
    [Required(ErrorMessage = "Tenant Key is required.")]
    public Guid TenantKey { get; set; }
}