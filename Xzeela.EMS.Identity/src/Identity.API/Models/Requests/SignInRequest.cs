using System.ComponentModel.DataAnnotations;
using MediatR;
using Xzeela.EMS.Shared.API.Models;

namespace Identity.API.Models.Requests;

public class SignInRequest
{
    /// <summary>
    /// Gets or sets the user's email address.
    /// </summary>
    [Required(ErrorMessage = "Email is Required")]
    [EmailAddress(ErrorMessage = "Invalid Email Address")]
    public string Email { get; set; } = null!;

    /// <summary>
    /// Gets or sets the new password.
    /// </summary>
    [Required(ErrorMessage = "Password is Required")]
    [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 8)]
    [RegularExpression(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*\W).+$", ErrorMessage = "Password should contain at least one uppercase letter, one lowercase letter, one special character, and one digit.")]
    public string Password { get; set; } = null!;
}