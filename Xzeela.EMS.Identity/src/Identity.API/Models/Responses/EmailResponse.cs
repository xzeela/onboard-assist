namespace Identity.API.Models.Responses;

public class EmailResponse
{
    public string StatusCode { get; set; } = null!;
    public string? Id { get; set; }
}