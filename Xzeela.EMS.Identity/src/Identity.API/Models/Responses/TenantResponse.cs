namespace Identity.API.Models.Responses;

public class TenantResponse
{
    public int Id { get; set; }
    public string Name { get; set; } = null!;
    public string? LogoUrl { get; set; }
    public Guid Key { get; set; }
    public bool IsActivated { get; set; }
}