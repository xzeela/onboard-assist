namespace Identity.API.Models.Responses;

public class SignInResponse
{
    public string Token { get; set; } = null!;
    public TenantResponse? Tenant { get; set; }
}