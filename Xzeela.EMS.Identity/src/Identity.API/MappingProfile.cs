using AutoMapper;
using Identity.API.Applications.Commands;
using Identity.API.Models.Requests;
using Identity.API.Models.Responses;
using Identity.Domain.Entities;

namespace Identity.API;

public class MappingProfile : Profile
{
    public MappingProfile()
    {

        //since type of DisplayPicture in tenant and create tenant request is different we ignore it 
        CreateMap<CreateTenantRequest, Tenant>().ReverseMap();
        CreateMap<TenantResponse, Tenant>().ReverseMap();
    }
}

