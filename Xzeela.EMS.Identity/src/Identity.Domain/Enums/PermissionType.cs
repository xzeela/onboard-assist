﻿namespace Identity.Domain.Enums
{
    public enum PermissionType
    {
        Edit,
        View
    }
}
