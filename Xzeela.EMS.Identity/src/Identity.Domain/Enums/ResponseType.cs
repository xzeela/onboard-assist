namespace Identity.Domain.Enums;

public enum ResponseType
{
    Success,
    Failed,
    NotFound,
    UserDontHaveAccess
}