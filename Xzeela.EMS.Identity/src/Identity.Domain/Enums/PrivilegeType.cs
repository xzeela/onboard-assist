using System.ComponentModel.DataAnnotations;

namespace Identity.Domain.Enums;

public enum PrivilegeType
{
    [Display(Name = "EMS-ADMIN")]
    EmsAdmin,
    [Display(Name = "TENANT ADMIN")]
    TenantAdmin,
    [Display(Name = "GENERAL USER")]
    GeneralUser,
}