﻿using System.ComponentModel.DataAnnotations;

namespace Identity.Domain.Enums
{
    public enum Module
    {
        [Display(Name = "0. EMPLOYEE MODULE")]
        employee,
        [Display(Name = "1. PROJECT MODULE")]
        project,
        [Display(Name = "2. LEAVE MODULE")]
        leave,
        [Display(Name = "4. TIMESHEET MODULE")]
        timesheet
    }
}
