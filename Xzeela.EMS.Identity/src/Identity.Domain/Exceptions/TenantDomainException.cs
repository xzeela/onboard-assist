﻿using System;
namespace Identity.Domain.Exceptions;

public class TenantDomainException : Exception
{
	public TenantDomainException()
	{
	}

    public TenantDomainException(string message) : base(message)
    {
    }

    public TenantDomainException(string message, Exception innerException) : base(message, innerException)
    {
    }
}

