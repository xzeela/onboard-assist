﻿using System;
namespace Identity.Domain.Exceptions;

public class RoleDomainException : Exception
{
	public RoleDomainException()
	{
	}

    public RoleDomainException(string message) : base(message)
    {
    }

    public RoleDomainException(string message, Exception innerException) : base(message, innerException)
    {
    }
}

