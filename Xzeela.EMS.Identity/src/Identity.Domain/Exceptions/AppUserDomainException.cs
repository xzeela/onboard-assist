﻿using System;
namespace Identity.Domain.Exceptions;

public class AppUserDomainException : Exception
{
	public AppUserDomainException()
	{
	}

    public AppUserDomainException(string message) : base(message)
    {
    }

    public AppUserDomainException(string message, Exception innerException) : base(message, innerException)
    {
    }
}

