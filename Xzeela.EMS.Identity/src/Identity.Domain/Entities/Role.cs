﻿using Microsoft.AspNetCore.Identity;

namespace Identity.Domain.Entities;

public class Role : IdentityRole<int>
{
    public List<Permission> Permissions { get; set; } = new();

    public double CreatedBy { get; set; }
    public DateTime CreatedOn { get; set; }

    public double LastModifiedBy { get; set; }
    public DateTime LastModifiedOn { get; set; } = DateTime.UtcNow;

    public bool IsDeleted { get; set; } = false;
    public bool IsActivated { get; set; }
    
    public Tenant Tenant { get; set; } = null!;

    public bool IsTenantAdmin { get; set; }
}