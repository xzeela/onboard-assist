﻿using Identity.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Identity.Domain.Entities
{
    public class Permission
    {
        public int Id { get; set; }
        public Module Module { get; set; }
        public PermissionType PermissionType { get; set; }
        public bool IsEnabled { get; set; }
    }
}
