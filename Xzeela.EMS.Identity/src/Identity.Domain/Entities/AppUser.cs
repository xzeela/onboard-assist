using Microsoft.AspNetCore.Identity;

namespace Identity.Domain.Entities;

public class AppUser : IdentityUser<int>
{
    public string Name { get; set; } = null!;
    public string UserId { get; set; } = null!;

    public DisplayPicture? DisplayPicture { get; set; }

    public Tenant Tenant { get; set; } = null!;

    public double CreatedBy { get; set; }
    public DateTime CreatedOn { get; set; }

    public double LastModifiedBy { get; set; }
    public DateTime LastModifiedOn { get; set; } = DateTime.UtcNow;

    public bool IsDeleted { get; set; } = false;
    public bool IsActivated { get; set; }

    public DateTime LastLogin { get; set; }
}
