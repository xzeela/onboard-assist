﻿using Identity.Domain.Enums;
using Newtonsoft.Json;

namespace Identity.Domain.Entities;

public class Tenant
{
    public int Id { get; set; }
    public Guid Key { get; set; } = Guid.NewGuid();

    public string Email { get; set; } = null!;
    public string Name { get; set; } = null!;
    public List<Module> Modules { get; set; } = new();
    public DisplayPicture? DisplayPicture { get; set; }

    public string? Remarks { get; set; }

    public double CreatedBy { get; set; }
    public DateTime CreatedOn { get; set; }

    public double LastModifiedBy { get; set; }
    public DateTime LastModifiedOn { get; set; } = DateTime.UtcNow;

    public bool IsDeleted { get; set; } = false;
    public bool IsActivated { get; set; }
}