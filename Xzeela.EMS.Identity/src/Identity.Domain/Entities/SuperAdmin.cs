using System.ComponentModel.DataAnnotations;

namespace Identity.Domain.Entities;

public class SuperAdmin
{
    [Key]
    public double Id { get; set; }
    public string Email { get; set; } = null!;
    public string Password { get; set; } = null!;
    public DateTime CreatedOn { get; set; }
    public DateTime LastLogin { get; set; }
}