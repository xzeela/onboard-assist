﻿using System;
namespace Identity.Domain.Entities;

public class DisplayPicture
{
    public int Id { get; set; }

    public string Base64Value { get; set; } = null!;
    public string Name { get; set; } = null!;

    public string Type { get; set; } = null!;
    public string Size { get; set; } = null!;

    public double CreatedBy { get; set; }
    public DateTime CreatedOn { get; set; }

    public double LastModifiedBy { get; set; }
    public DateTime LastModifiedOn { get; set; } = DateTime.UtcNow;
}