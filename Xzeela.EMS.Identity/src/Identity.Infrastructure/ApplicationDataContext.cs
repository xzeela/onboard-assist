﻿using Identity.Domain.Entities;
using Identity.Domain.Enums;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Newtonsoft.Json;

namespace Identity.Infrastructure;

public class ApplicationDataContext : IdentityDbContext<AppUser, Role, int>
{
    public ApplicationDataContext(
     DbContextOptions<ApplicationDataContext> options
 ) : base(options)
    {
    }
    
    public DbSet<Tenant> Tenants { get; set; }
    public DbSet<Permission> Permissions { get; set; }
    public DbSet<SuperAdmin> SuperAdmins { get; set; }
    public DbSet<DisplayPicture> DisplayPictures { get; set; }

    protected override void OnModelCreating(ModelBuilder builder)
    {
        base.OnModelCreating(builder);
        
        builder.HasDefaultSchema("Identity");
        
        builder.Entity<Tenant>()
            .HasKey(tenant => new { tenant.Id });

        builder.Entity<Permission>()
            .HasKey(permission => new { permission.Id });

        builder.Entity<DisplayPicture>()
            .HasKey(displayPicture => new { displayPicture.Id });
        
        builder.Entity<SuperAdmin>()
            .HasKey(superAdmin => new { superAdmin.Id });
    }
}