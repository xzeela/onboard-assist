using Identity.Domain.Entities;
using Xzeela.EMS.Shared.Persistence.Interfaces;

namespace Identity.Infrastructure.Interfaces;

public interface ITenantRepository : IAsyncRepository<Tenant>
{
    Task<bool> CheckNameAvailability(string name);
    Task<bool> CheckEmailAvailability(string email);
    Task<DisplayPicture?> GetTenantDisplayPicture(int id);
    Task<Tenant?> GetTenantByTenantKey(Guid tenantKey);
}


