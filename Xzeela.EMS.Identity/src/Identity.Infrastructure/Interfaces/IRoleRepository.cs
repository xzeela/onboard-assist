using Identity.Domain.Entities;
using Xzeela.EMS.Shared.Persistence.Interfaces;

namespace Identity.Infrastructure.Interfaces;

public interface IRoleRepository : IAsyncRepository<Role>
{
    Task<Role?> GetRoleByTenantAndName(int tenantId, string name);
    Task<List<Role>> GetRolesByTenant(int tenantId);
}