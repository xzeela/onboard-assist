using Identity.Domain.Entities;
using Xzeela.EMS.Shared.Persistence.Interfaces;

namespace Identity.Infrastructure.Interfaces;

public interface IUserRepository  : IAsyncRepository<AppUser>
{
    Task<List<AppUser>> FindUsersByTenant(int tenantId);
    Task<Tenant?> GetTenantByUserId(int id);
}