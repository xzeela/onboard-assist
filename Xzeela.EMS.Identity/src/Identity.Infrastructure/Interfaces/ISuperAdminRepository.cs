using Identity.Domain.Entities;
using Xzeela.EMS.Shared.Persistence.Interfaces;

namespace Identity.Infrastructure.Interfaces;

public interface ISuperAdminRepository : IAsyncRepository<SuperAdmin>
{
    Task<SuperAdmin?> GetSuperAdminByEmail(string email);
}