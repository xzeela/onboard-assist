using Identity.Domain.Entities;
using Identity.Infrastructure.Interfaces;
using Microsoft.EntityFrameworkCore;
using Xzeela.EMS.Shared.Persistence.Repositories;

namespace Identity.Infrastructure.Repositories;

public class RoleRepository : BaseRepository<Role>, IRoleRepository
{
    private readonly DbSet<Role> _roles;
    public RoleRepository(ApplicationDataContext applicationDbContext) : base(applicationDbContext)
    {
        _applicationDataContext = applicationDbContext;
        _roles = _applicationDataContext.Set<Role>();
    }

    public async Task<Role?> GetRoleByTenantAndName(int tenantId, string name)
    {
        return await _roles.FirstOrDefaultAsync(x => x.NormalizedName == name.ToUpper() && x.Tenant.Id == tenantId);
    }

    public async Task<List<Role>> GetRolesByTenant(int tenantId)
    {
        return await _roles.Include(x => x.Permissions).Where(x => x.Tenant.Id == tenantId).ToListAsync();
    }
}