using Identity.Domain.Entities;
using Identity.Infrastructure.Interfaces;
using Microsoft.EntityFrameworkCore;
using Xzeela.EMS.Shared.Persistence.Repositories;

namespace Identity.Infrastructure.Repositories;

public class TenantRepository : BaseRepository<Tenant>, ITenantRepository
{
    private readonly DbSet<Tenant> _tenants;
    public TenantRepository(ApplicationDataContext applicationDbContext) : base(applicationDbContext)
    {
        _applicationDataContext = applicationDbContext;
        _tenants = _applicationDataContext.Set<Tenant>();
    }

    public Task<bool> CheckNameAvailability(string name)
    {
        return _tenants.AnyAsync(x => x.Name == name.ToLower());
    }

    public Task<bool> CheckEmailAvailability(string email)
    {
        return _tenants.AnyAsync(x => x.Email == email.ToLower());
    }

    public async Task<DisplayPicture?> GetTenantDisplayPicture(int id)
    {
        var tenant = await _tenants.Include(t => t.DisplayPicture).FirstOrDefaultAsync(x => x.Id == id);
        return tenant?.DisplayPicture;
    }

    public async Task<Tenant?> GetTenantByTenantKey(Guid tenantKey)
    {
        return await _tenants.FirstOrDefaultAsync(x => x.Key == tenantKey);
    }
}