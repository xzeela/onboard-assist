using Identity.Domain.Entities;
using Identity.Infrastructure.Interfaces;
using Microsoft.EntityFrameworkCore;
using Xzeela.EMS.Shared.Persistence.Repositories;

namespace Identity.Infrastructure.Repositories;

public class UserRepository : BaseRepository<AppUser>, IUserRepository
{
    private readonly DbSet<AppUser> _users;
    public UserRepository(ApplicationDataContext applicationDbContext) : base(applicationDbContext)
    {
        _applicationDataContext = applicationDbContext;
        _users = _applicationDataContext.Set<AppUser>();
    }

    public async Task<List<AppUser>> FindUsersByTenant(int tenantId)
    {
        return await _users.Where(x => x.Tenant.Id == tenantId).ToListAsync();
    }
    
    public async Task<Tenant?> GetTenantByUserId(int id)
    {
        var user = await _users.Include(x => x.Tenant).FirstOrDefaultAsync(x => x.Id == id);
        return user?.Tenant;
    }
}