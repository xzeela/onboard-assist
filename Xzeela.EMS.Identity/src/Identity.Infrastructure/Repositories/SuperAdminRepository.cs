using Identity.Domain.Entities;
using Identity.Infrastructure.Interfaces;
using Microsoft.EntityFrameworkCore;
using Xzeela.EMS.Shared.Persistence.Repositories;

namespace Identity.Infrastructure.Repositories;

public class SuperAdminRepository : BaseRepository<SuperAdmin>, ISuperAdminRepository
{
    private readonly DbSet<SuperAdmin> _superAdmins;
    public SuperAdminRepository(ApplicationDataContext applicationDbContext) : base(applicationDbContext)
    {
        _applicationDataContext = applicationDbContext;
        _superAdmins = _applicationDataContext.Set<SuperAdmin>();
    }

    public async Task<SuperAdmin?> GetSuperAdminByEmail(string email)
    {
        return await _superAdmins.FirstOrDefaultAsync(x => x.Email == email.ToLower());
    }
    
}