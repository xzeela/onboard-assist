using Identity.API.Controllers;
using Identity.API.Applications.Commands;
using Identity.API.Applications.Queries;
using Identity.API.Models.Requests;
using Identity.API.Models.Responses;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Identity.Domain.Enums;
using Xzeela.EMS.Shared.API.Models;
using Xzeela.EMS.Shared.Services;

namespace Identity.Tests
{
    public class TenantTest
    {
        private readonly Mock<IMediator> _mediatorMock;
        private readonly TenantController _tenantController;

        public TenantTest()
        {
            _mediatorMock = new Mock<IMediator>();
            _tenantController = new TenantController(
                new Mock<IRequestTrackingService>().Object,
                _mediatorMock.Object
            );
        }

        [Fact]
        public async Task CreateTenant_ReturnsCreatedResult()
        {
            // Arrange
            var createTenantRequest = new CreateTenantRequest
            {
                Name = "Sample Tenant Name",
                Email = "sample@example.com",
                DisplayPictureFile = null,
                Remarks = "Optional remarks about the tenant",
                Modules = new List<Module>
                {
                    Module.employee 
                }
            };

            const int expectedTenantId = 1; // You can set your expected result here

            // Mock the behavior of the mediator for CreateTenantRequest
            _mediatorMock.Setup(x => x.Send(It.IsAny<CreateTenantRequest>(), CancellationToken.None))
                        .ReturnsAsync(expectedTenantId.ToString());

            // Act
            var result = await _tenantController.CreateTenant(createTenantRequest);

            // Assert
            var createdResult = Assert.IsType<OkObjectResult>(result.Result);
            var model = Assert.IsAssignableFrom<BaseResponse>(createdResult.Value);

            // You can add more assertions here based on your actual response structure.
            Assert.Equal(expectedTenantId.ToString(), model.Result);
            Assert.Equal(ResponseType.Success.ToString(), model.Message);
            Assert.Equal(StatusCodes.Status200OK, createdResult.StatusCode);
        }
        
        [Fact]
        public async Task GetTenantById_ReturnsOkResult()
        {
            // Arrange
            const int tenantId = 1; // Replace with the desired tenant ID for the test

            var expectedTenantResponse = new TenantResponse
            {
                Id = tenantId,
                Name = "Tenant 1",
                // Add other properties as needed for TenantResponse
            };

            // Mock the behavior of the mediator for GetTenantByIdQuery
            _mediatorMock.Setup(x => x.Send(It.IsAny<GetTenantByIdQuery>(), CancellationToken.None))
                .ReturnsAsync(expectedTenantResponse);

            // Act
            var result = await _tenantController.GetTenantById(tenantId);

            // Assert
            var okResult = Assert.IsType<OkObjectResult>(result.Result);
            var model = Assert.IsAssignableFrom<BaseResponse>(okResult.Value);

            // You can add more assertions here based on your actual response structure.
            Assert.Equal(ResponseType.Success.ToString(), model.Message);
            Assert.Equal(StatusCodes.Status200OK, okResult.StatusCode);
        }

    }
}
